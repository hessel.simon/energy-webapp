ARG APP_NAME
ARG REPORT_FILE

FROM registry.git.rwth-aachen.de/renewable-energy/$APP_NAME:master as pdf

FROM node:16 as builder

WORKDIR /app
COPY decorate-angular-cli.js package.json package-lock.json /app/
RUN npm ci

# COPY files
COPY .eslintrc.json nx.json angular.json tsconfig.base.json  /app/

# COPY folders
COPY apps/$APP_NAME /app/apps/$APP_NAME
COPY assets /app/assets
COPY environments /app/environments
COPY libs /app/libs
COPY sass /app/sass
COPY tools /app/tools

COPY --from=pdf /usr/src/app/technicalreport/$REPORT_FILE ./assets/documents/$REPORT_FILE

RUN npx nx build $APP_NAME --output-path dist

FROM nginx:1.20.2-alpine

WORKDIR /app/

RUN apk add --no-cache --update \
    apache2-utils

COPY resources/nginx.conf /etc/nginx/nginx.conf
COPY resources/site.conf /etc/nginx/sites-enabled/default

## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

## From ‘builder’ stage copy over the artifacts in dist folder to default nginx public folder
COPY --from=builder /app/dist /app/dist

CMD ["nginx"]