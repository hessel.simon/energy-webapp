# Flower Framework

This folder holds the angular-based frontend application of the Computational Renewable Energy WebApp

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
You will need the latest stable Node version installed on you PC.

### Prerequisites (linux need `sudo` permissions, windows without `sudo`)

1. [Node.JS | LTS](https://nodejs.org/en/) (currently 10.x)
2. [Docker](https://docs.docker.com/get-docker/)
3. [Docker compose](https://docs.docker.com/compose/install/) (already in the windows Docker install)
4. Angular CLI
   ```bash
   $ sudo npm install -g @angular/cli
   ```

### Installation

1. Clone the repository. Make sure that you setup your [SSH keys](https://git.rwth-aachen.de/help/ssh/README#locating-an-existing-ssh-key-pair) properly before or clone via HTTPS.

   ```bash
   $ git clone git@git.rwth-aachen.de:renewable-energy/flower-webapp.git
   ```

   or

   ```bash
   $ git clone https://git.rwth-aachen.de/renewable-energy/flower-webapp.git

   ```

2. Change into the backend folder in the cloned repository.
   ```bash
   $ cd flower-webapp/Backend
   ```
3. Checkout the `backend_master` branch

   ```bash
   $ git checkout backend_master
   ```

4. Install packages

   ```bash
   $ npm install
   ```

5. Login to RWTH Registry (GitLab credentials) (You only need to do this once, or whenever your access rights change)

   ```bash
   $ docker login registry.git.rwth-aachen.de
   ```

6. Start local server

   ```bash
   $ cd Backend/docker/Development
   $ docker-compose up
   ```

7. Change into the cloned repository and install packages

   ```bash
   $ cd /path/to/cloned/repository/Windflower_Frontend
   $ npm install
   ```

8. Start local server
   ```bash
   $ npm start
   ```
9. Build for production
   ```bash
   $ ng build --prod
   ```
1. To simulate the Windfarms itself the Flower Simulation Worker needs to be started at https://git.rwth-aachen.de/renewable-energy/flower-simulation-worker
   ```bash
   $ cd ./development/with_backend
   $ docker-compose up
   ```

After that you will have a ready-to-use dev system with http://localhost:8080/ being the endpoint of the backend REST API.
The `src/`, `docs/` and `test/` directories are mounted as volumes, code-changes on your local systems are reflected in the container.

After new changes from the backend have been pulled or made, you need to rebuild the docker image. You can do this by calling either `docker-compose up --build` to force a rebuild of the image before starting up the backend.
After new changes from the backend or Frontend, you may need to reinstall the packages.

### Extra Resources:

1. [Swagger](http://localhost:8080/swagger/): all paths are documented
2. [MongoExpress](http://localhost:8081/): handy tool to change something in db for testing

# Flower Backend

This repository holds the core backend of the Computational Renewable Energy WebApp. It provides a REST API for the frontend.

Technically, it's build on top of [Node.js](https://nodejs.org/) using the [Express framework](https://expressjs.com).

```mermaid
graph LR;

Yggdrasil --- EventBus
Yggdrasil --- database

database --- MongoDB

EventBus --- calculation
EventBus --- mail
EventBus --- ping
EventBus --- project
EventBus --- tasks
EventBus --- user
EventBus --- webserver
EventBus --- news

calculation --- MongoDB
mail --- SMTP
project --- MongoDB
tasks --- RabbitMQ
user --- MongoDB
webserver --- Express
```

## Quick Start & Documentation NX

[Nx Documentation](https://nx.dev/angular)

[10-minute video showing all Nx features](https://nx.dev/getting-started/intro)

[Interactive Tutorial](https://nx.dev/tutorial/01-create-application)

## Development

```sh
$ npm install

$ npm start
```

## Structure

### How to generate a lib in Nx?

1. use @nrwl/angular for a library of angular components
2. set these three attributes:
   - --name=...
   - --directory=...
   - --simpleModuleName

### I want to add a new component/service/pipe/etc where should i put it?

1. Should your class be reusable
   - within in a single app => `libs/<APP_NAME>/**`
   - within in multiple apps => `libs/shared/**`
2. Is it related to an already existing module
   - yes => put into the module and update the `<NAME>-module.ts` file
   - no => Create a new module

### I want to add a new module should i put it?

1. Does your module include a routing module?
   - yes => `libs/<APP_NAME>`
2. Does your module export components/service/pipes that could/should be used in multiple apps
   - yes => `libs/shared`
3. Put where you feel it belongs and can always be moved down the road :)

```
energy-platform
 ┣ apps
 ┃ ┣ windflower
 ┃ ┃ ┣ src
 ┃ ┃ ┃ ┣ app
 ┃ ┃ ┃ ┗ environments
 ┃ ┣ windflower-e2e
 ┃ ┃ ┣ src
 ┃ ┃ ┃ ┣ fixtures
 ┃ ┃ ┃ ┣ integration
 ┃ ┃ ┃ ┗ support
 ┣ libs
 ┃ ┣ shared
 ┃ ┃ ┣ authentication
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┃ ┗ authentication.module.ts
 ┃ ┃ ┣ charts
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ charts.module.ts
 ┃ ┃ ┣ dialog
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ dialog.module.ts
 ┃ ┃ ┣ layout
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┃ ┃ ┣ footer
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ header
 ┃ ┃ ┃ ┃ ┃ ┗ layout.module.ts
 ┃ ┃ ┗ three-visualizations
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ three-visualizations.module.ts
 ┃ ┣ windflower
 ┃ ┃ ┣ admin
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ admin.module.ts
 ┃ ┃ ┣ dashboard
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ dashboard.module.ts
 ┃ ┃ ┣ jobs
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ jobs.module.ts
 ┃ ┃ ┣ landingpage
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┣ components
 ┃ ┃ ┃ ┃ ┃ ┃ ┗ landingpage
 ┃ ┃ ┃ ┃ ┃ ┗ landingpage.module.ts
 ┃ ┃ ┗ project
 ┃ ┃ ┃ ┣ src
 ┃ ┃ ┃ ┃ ┣ lib
 ┃ ┃ ┃ ┃ ┃ ┗ project.module.ts
 ┣ tools
 ┃ ┗ generators
 ┣ angular.json
 ┣ jest.config.js
 ┣ jest.preset.js
 ┣ nx.json
 ┣ package.json
 ┗ tsconfig.base.json
```
