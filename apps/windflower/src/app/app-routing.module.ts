import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { BaseComponent } from '@energy-platform/shared/layout';
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home',
  },
  {
    path: 'home',
    component: BaseComponent,
    loadChildren: () =>
      import('@energy-platform/windflower/landingpage').then(
        (m) => m.LandingpageModule
      ),
  },
  {
    path: 'app',
    component: BaseComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: () =>
          import('@energy-platform/windflower/dashboard').then(
            (m) => m.DashboardModule
          ),
      },
      {
        path: 'admin',
        loadChildren: () =>
          import('@energy-platform/windflower/admin').then(
            (m) => m.AdminModule
          ),
      },

      {
        path: 'project',
        loadChildren: () =>
          import('@energy-platform/windflower/project').then(
            (m) => m.ProjectModule
          ),
      },
      {
        path: 'parameter',
        loadChildren: () =>
          import('@energy-platform/windflower/parameter-page').then(
            (m) => m.ParameterPageModule
          ),
      },
      {
        path: 'jobs',
        loadChildren: () =>
          import('@energy-platform/windflower/jobs').then((m) => m.JobsModule),
      },
      {
        path: '**',
        redirectTo: 'dashboard',
      },
    ],
  },
  {
    path: 'imprint',
    component: BaseComponent,
    loadChildren: () =>
      import('@energy-platform/shared/declaration').then(
        (m) => m.ImprintModule
      ),
  },
  {
    path: 'declaration',
    component: BaseComponent,
    loadChildren: () =>
      import('@energy-platform/shared/declaration').then(
        (m) => m.DeclarationModule
      ),
  },
  {
    path: 'admin',
    component: BaseComponent,
    loadChildren: () =>
      import('@energy-platform/windflower/admin').then((m) => m.AdminModule),
  },
  {
    path: '**',
    redirectTo: 'home',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
