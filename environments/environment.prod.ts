export const environment = {
  production: true,
  API_BASE_URL: '/api',
  MAPBOX_ACCESS_TOKEN:
    'pk.eyJ1Ijoic2ViYXN0aWFuc3VkZXIiLCJhIjoiY2sybWN6ZGk1MGZiMjNvcDBzMzJ2NjY5biJ9.T8Sn_yO_ExhOnpGXiodWeg',
  MAT_ERROR_DEFAULT: 5000,
};
