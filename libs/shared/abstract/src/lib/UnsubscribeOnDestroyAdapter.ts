import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { SubSink } from 'subsink';
import { BaseStructure } from '@energy-platform/shared/utils';
@Component({ template: '', changeDetection: ChangeDetectionStrategy.OnPush }) //Aus dem alten Projekt genommen, passt das?
// eslint-disable-next-line @angular-eslint/component-class-suffix
export abstract class UnsubscribeOnDestroyAdapter
  extends BaseStructure
  implements OnDestroy
{
  /**The subscription sink object that stores all subscriptions */
  subs = new SubSink();
  /**
   * The lifecycle hook that unsubscribes all subscriptions
   * when the component / object gets destroyed
   */
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
