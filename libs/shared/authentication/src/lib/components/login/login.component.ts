import { Component, Inject } from '@angular/core';

import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { LoginDialogData } from '@energy-platform/shared/interfaces';
import { ResetPasswordDialogComponent } from '../reset-password-dialog/reset-password-dialog.component';

@Component({
  selector: 'energy-platform-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  hide: boolean;
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<LoginComponent>,
    @Inject(MAT_DIALOG_DATA) public data: LoginDialogData
  ) {
    this.hide = true;
  }

  public openResetPasswordModal(): void {
    this.dialog.open(ResetPasswordDialogComponent, {
      width: '400px',
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  login(data: unknown): void {
    this.dialogRef.close(data);
  }

  redirectToRegister() {
    this.dialogRef.close('registerUser');
  }
}
