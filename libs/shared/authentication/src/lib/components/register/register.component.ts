import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Inject, OnInit } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatDialog,
} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Organization } from '@energy-platform/shared/interfaces';
import { RegisterDialogData } from '@energy-platform/shared/interfaces';
import { NewOrganizationComponent } from '@energy-platform/shared/dialog';
import { environment } from 'environments/environment';
import { OrganizationService } from '@energy-platform/shared/services';
import { MatCheckboxChange } from '@angular/material/checkbox';

@Component({
  selector: 'energy-platform-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  hide: boolean;
  organizations!: Organization[];
  orgaName!: string;
  orgaDescription!: string;
  isPolicyConsentCheckboxChanged = false;
  isCookieConsentCheckboxChanged = false;
  userAgreesPolicyConsent = false;
  userAgreesCookieConsent = false;
  newOrganizationSelected = false;
  /* Characters that can be used in passwords */
  legalChars: string[] = [
    'A',
    'a',
    'B',
    'b',
    'C',
    'c',
    'D',
    'd',
    'E',
    'e',
    'F',
    'f',
    'G',
    'g',
    'H',
    'h',
    'I',
    'i',
    'J',
    'j',
    'K',
    'k',
    'L',
    'l',
    'M',
    'm',
    'N',
    'n',
    'O',
    'o',
    'P',
    'p',
    'Q',
    'q',
    'R',
    'r',
    'S',
    's',
    'T',
    't',
    'U',
    'u',
    'V',
    'v',
    'W',
    'w',
    'X',
    'x',
    'Y',
    'y',
    'Z',
    'z',
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '_',
    '!',
    '?',
    '@',
    '#',
    '&',
    '(',
    ')',
    '{',
    '}',
    '+',
    '*',
    ',',
    ';',
    ':',
    '-',
    '/',
    '%',
    '=',
    '<',
    '>',
    '~',
    '|',
  ];
  /* The minimum of characters required in a password */
  pwMinLength = 8;

  constructor(
    private organizationService: OrganizationService,
    public dialogRef: MatDialogRef<RegisterComponent>,
    @Inject(MAT_DIALOG_DATA) public data: RegisterDialogData,
    public dialog: MatDialog,
    private router: Router,
    private snackBar: MatSnackBar
  ) {
    super();
    this.hide = true;
  }

  ngOnInit() {
    this.organizationService.getOrganizationListenerPublic().subscribe(
      (res: Organization[]) => {
        this.organizations = res;
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: environment.MAT_ERROR_DEFAULT,
        });
      }
    );
    this.organizationService.fetchOrganizationAllPublic();
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  register(data: any) {
    this.dialogRef.close(data);
  }

  showPrivacyErrorHint(): boolean {
    return this.isPolicyConsentCheckboxChanged && !this.userAgreesPolicyConsent;
  }

  updatePolicyShowErrorHint($event: MatCheckboxChange) {
    this.isPolicyConsentCheckboxChanged = true;
    this.userAgreesPolicyConsent = $event.checked;
  }

  goToDeclarationPage() {
    this.closeDialog();
    this.router.navigate(['/declaration']);
  }

  updateShowCookieErrorHint($event: MatCheckboxChange) {
    this.isCookieConsentCheckboxChanged = true;
    this.userAgreesCookieConsent = $event.checked;
  }

  showCookieErrorHint(): boolean {
    return this.isCookieConsentCheckboxChanged && !this.userAgreesCookieConsent;
  }

  newOrganization() {
    this.data.organizationId = '0';
    const dialogNewOrga = this.dialog.open(NewOrganizationComponent, {
      data: {
        name: this.orgaName,
        description: this.orgaDescription,
      },
    });
    dialogNewOrga.afterClosed().subscribe(
      (orgaData) => {
        if (orgaData !== undefined) {
          this.data.newOrganizationName = orgaData.name;
          this.data.newOrganizationDescription = orgaData.description;
          this.orgaName = orgaData.name;
        }
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: environment.MAT_ERROR_DEFAULT,
        });
      }
    );
    this.newOrganizationSelected = true;
    this.orgaName = this.data.newOrganizationName;
  }

  isInvalid(pwd: string): boolean {
    if (pwd) {
      for (let i = 0; i < pwd.length; i++) {
        let okay = 0;
        for (const s in this.legalChars) {
          if (pwd.charAt(i) == this.legalChars[s]) okay = 1;
        }
        if (okay == 0) return true;
      }
      return false;
    } else {
      return false;
    }
  }

  tooShort(pwd: string): boolean {
    return pwd ? pwd.length < this.pwMinLength : false;
  }
}
