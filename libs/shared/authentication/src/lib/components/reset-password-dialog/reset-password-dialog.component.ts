import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { take } from 'rxjs/operators';
import { environment } from 'environments/environment';
import { AuthenticationService } from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-reset-password-dialog',
  templateUrl: './reset-password-dialog.component.html',
  styleUrls: ['./reset-password-dialog.component.scss'],
})
export class ResetPasswordDialogComponent extends UnsubscribeOnDestroyAdapter {
  email!: string;
  constructor(
    public dialogRef: MatDialogRef<ResetPasswordDialogComponent>,
    private authenticationService: AuthenticationService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  reset(email: string): void {
    if (email) {
      this.authenticationService
        .requestReset(email)
        .pipe(take(1))
        .subscribe(
          (serverResponse: any) => {
            this.logger.info(serverResponse);
            if (serverResponse.status === 200) {
              this.snackBar.open(
                'We have sent you a link to reset your password, please check your email!',
                'Ok',
                {
                  duration: environment.MAT_ERROR_DEFAULT,
                }
              );
            } else {
              this.snackBar.open('Something went wrong!', 'Ok', {
                duration: environment.MAT_ERROR_DEFAULT,
              });
            }
          },
          (error) => {
            this.logger.error(error);
          }
        );
    }
    this.dialogRef.close();
  }
}
