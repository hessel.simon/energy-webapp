import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'energy-platform-declaration-page',
  templateUrl: './declaration-page.component.html',
  styleUrls: ['./declaration-page.component.scss'],
})
export class DeclarationPageComponent implements OnInit {
  constructor(private routeInfo: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    if (this.router.url.endsWith('privacy')) {
      window.location.assign('/declaration#privacy');
    } else if (this.router.url.endsWith('disclaimer')) {
      window.location.assign('/declaration#disclaimer');
    }
  }

  goBackToLandingpage() {
    this.router.navigate(['/home']);
  }
}
