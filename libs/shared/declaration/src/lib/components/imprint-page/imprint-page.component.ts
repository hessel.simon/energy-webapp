import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'energy-platform-imprint-page',
  templateUrl: './imprint-page.component.html',
  styleUrls: ['./imprint-page.component.scss'],
})
export class ImprintPageComponent {
  constructor(private router: Router) {}
  goBackToLandingpage() {
    this.router.navigate(['/login']);
  }
}
