import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImprintPageComponent } from './components/imprint-page/imprint-page.component';
import { RouterModule } from '@angular/router';
// Material Components
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
const components = [ImprintPageComponent];
@NgModule({
  declarations: [components],
  exports: [components],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    RouterModule.forChild([
      { path: '**', pathMatch: 'full', component: ImprintPageComponent },
    ]),
  ],
})
export class ImprintModule {}
