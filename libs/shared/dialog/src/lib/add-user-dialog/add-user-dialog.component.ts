import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Contributor, Project } from '@energy-platform/shared/interfaces';
import { User } from '@energy-platform/shared/interfaces';
import { UserService } from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss'],
})
export class AddUserDialogComponent {
  hide = true;
  usersIn: Contributor[] = [];
  userNotInProject: User[] = [];
  users: User[] = [];
  project: Project | undefined;
  newContributors: string[] = [];

  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    //Data wurde zu Projekt geändert
    @Inject(MAT_DIALOG_DATA) public data: Project,
    private userService: UserService
  ) {
    this.getUsers();
  }

  private getUsers() {
    this.userService.getUserListener(true).subscribe((users) => {
      this.users = users;
      this.project = this.data;
      if (this.project) this.usersIn = this.project.contributors;
      const ids = this.usersIn.map((contributor) => contributor.user.id);
      this.userNotInProject = this.users.filter(
        (user) => !ids.includes(user.id) && !user.deleted
      );
    });
  }

  onSubmit() {
    if (!this.data) {
      return;
    }
    if (!this.newContributors) {
      this.newContributors = [];
    }
    console.log('all users', this.userNotInProject);
    console.log('new contributors', this.newContributors);
    this.dialogRef.close(this.newContributors);
  }
}
