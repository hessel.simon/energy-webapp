import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Project, Right } from '@energy-platform/shared/interfaces';
import { UserService } from '@energy-platform/shared/services';
import { MailService } from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

@Component({
  selector: 'energy-platform-add-user-mail-dialog',
  templateUrl: './add-user-mail-dialog.component.html',
  styleUrls: ['./add-user-mail-dialog.component.scss'],
})
export class AddUserMailDialogComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  email = '';
  project: Project | undefined;
  text = '';
  userId: string | undefined;
  right = Right['READ'];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: { project: Project; userId: string },
    private userService: UserService,
    private mailService: MailService,
    public dialogRef: MatDialogRef<AddUserMailDialogComponent>,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.project = this.data.project;
    this.userId = this.data.userId;
    // TODO change default text
    this.userService.getOwnUserProfile().subscribe((user) => {
      if (this.project && user) {
        this.text =
          'Hello, \nI would like to invite you to my project ' +
          this.project.name +
          '.' +
          ' To join the project please go to https://windflower.energy.rwth-aachen.de/login and create an user account with this email. \n' +
          'Kind regards,\n' +
          user.firstName +
          ' ' +
          user.lastName;
      } else {
        this.logger.error('Project or User undefined');
      }
    });
  }

  onSend() {
    if (this.userId && this.project) {
      this.mailService
        .AddUserToProject(
          this.email,
          this.text,
          this.userId,
          this.project.id,
          this.right
        )
        .subscribe(
          () => {
            this.snackBar.open(
              `Email sent successfuly to ${this.email}`,
              'Ok',
              {
                duration: 5000,
                verticalPosition: 'bottom',
                horizontalPosition: 'right',
              }
            );
            this.dialogRef.close();
          },
          (err) => {
            if (err.error.code.includes('ServiceError')) {
              if (err.error.code.includes('$newEntry')) {
                this.snackBar.open('[INFO]: ' + err.error.messages[0], 'Ok', {
                  duration: 1500,
                  verticalPosition: 'bottom',
                  horizontalPosition: 'right',
                });
                this.dialogRef.close({ updateUser: true });
                return;
              }

              this.snackBar.open('[ERROR]: ' + err.error.messages[0], 'Ok', {
                duration: 1500,
                verticalPosition: 'bottom',
                horizontalPosition: 'right',
              });
            }
          }
        );
    } else {
      this.logger.error('UserId or Project is undefined');
    }
  }
}
