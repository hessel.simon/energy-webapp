import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrganizationService } from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-delete-organization',
  templateUrl: './delete-organization.component.html',
  styleUrls: ['./delete-organization.component.scss'],
})
export class DeleteOrganizationComponent {
  constructor(
    public dialogRef: MatDialogRef<DeleteOrganizationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private organizationService: OrganizationService
  ) {} // change type from any

  close() {
    this.dialogRef.close();
  }
  delete() {
    if (!this.data.organization) {
      return;
    }
    this.organizationService.deleteOrganization(this.data.organization.id);
    this.close();
  }
}
