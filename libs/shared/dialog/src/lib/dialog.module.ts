import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
//Angular Material Imports
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UploadDialogComponent } from './upload-dialog/upload-dialog.component';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { AddUserDialogComponent } from './add-user-dialog/add-user-dialog.component';
import { AddUserMailDialogComponent } from './add-user-mail-dialog/add-user-mail-dialog.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { NewOrganizationComponent } from './new-organization/new-organization.component';
import { NewProjectModalComponent } from './new-project-modal/new-project-modal.component';
import { PublishWarningComponent } from './publish-warning/publish-warning.component';
import { SettingsDialogComponent } from './settings/settings.dialog';
import { DeleteProjectDialogComponent } from './delete-project-dialog/delete-project-dialog.component';
import { ProjectDetailDialogComponent } from './project-detail-dialog/project-detail-dialog.component';
import { DeleteOrganizationComponent } from './delete-organization/delete-organization.component';
import { MergeOrganizationsComponent } from './merge-organizations/merge-organizations.component';
import { NewOrganizationAdminComponent } from './new-organization-admin/new-organization-admin.component';
import { OrganizationDetailDialogComponent } from './organization-detail-dialog/organization-detail-dialog.component';
import { UserDetailDialogComponent } from './user-detail-dialog/user-detail-dialog.component';
import { MatOptionModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';

const components = [
  SettingsDialogComponent,
  DeleteDialogComponent,
  AddUserDialogComponent,
  AddUserMailDialogComponent,
  NewProjectModalComponent,
  PublishWarningComponent,
  NewOrganizationComponent,
  UploadDialogComponent,
  DeleteProjectDialogComponent,
  ProjectDetailDialogComponent,
  DeleteOrganizationComponent,
  MergeOrganizationsComponent,
  NewOrganizationAdminComponent,
  OrganizationDetailDialogComponent,
  UserDetailDialogComponent,
];
@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDialogModule,
    MatSnackBarModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MatOptionModule,
    TranslateModule,
  ],
  declarations: components,
  exports: components,
})
export class DialogModule {}
