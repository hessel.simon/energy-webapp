import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { Organization } from '@energy-platform/shared/interfaces';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { MatSelect } from '@angular/material/select';
import { FormBuilder, FormGroup } from '@angular/forms';
import { OrganizationService } from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-merge-organizations',
  templateUrl: './merge-organizations.component.html',
  styleUrls: ['./merge-organizations.component.scss'],
})
export class MergeOrganizationsComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  organizations!: Organization[];
  organization!: Organization;
  form!: FormGroup;
  organizationChanged = false;

  @ViewChild('organizations', { static: false }) matSelect!: MatSelect;

  ngOnInit() {
    this.getOrganizations();
    this.createForm();
  }

  constructor(
    public dialogRef: MatDialogRef<MergeOrganizationsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private organizationService: OrganizationService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnChanges() {
    this.getOrganizations();
    this.createForm();
  }
  private getOrganizations() {
    this.organizationService.getOrganizationListenerAll().subscribe(
      (organizations) => {
        this.organizations = organizations;
        for (let j = 0; j < this.organizations.length; j++) {
          if (this.data.organization.id === this.organizations[j].id) {
            this.organizations.slice(j, 1);
          }
        }
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.organizationService.fetchOrganizationsAll();
  }

  private createForm() {
    this.form = this.fb.group({
      organizations: [this.organizations],
    });
  }

  onSubmit() {
    if (!this.data.organization) {
      return;
    }
    this.logger.info(this.organization);
    this.organizationService.mergeOrganization(
      this.data.organization.id,
      this.organization.id
    );
    this.dialogRef.close();
  }

  organizationChanges(event: any) {
    this.organizationChanged = true;
  }
}
