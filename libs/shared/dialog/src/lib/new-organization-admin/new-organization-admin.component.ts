import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrganizationService } from '@energy-platform/shared/services';
import { Organization } from '@energy-platform/shared/interfaces';

@Component({
  selector: 'energy-platform-new-organization-admin',
  templateUrl: './new-organization-admin.component.html',
  styleUrls: ['./new-organization-admin.component.scss'],
})
export class NewOrganizationAdminComponent implements OnInit {
  organizations: Organization[] = [];
  form!: FormGroup;
  loading!: boolean;
  // TODO init with mdi-domain logo
  logoURL = '';
  name!: string;
  description!: string;

  constructor(
    public dialogRef: MatDialogRef<NewOrganizationAdminComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private organizationService: OrganizationService,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      organizationName: ['', Validators.required],
      organizationDescription: ['', Validators.required],
      organizationLogo: [this.logoURL, Validators.required],
    });
    this.loading = true;
  }

  onSubmit() {
    this.organizationService.addOrganization(
      this.data.name,
      this.data.description,
      ''
    );
    this.dialogRef.close();
  }

  loadDefaultLogo() {
    this.logoURL = '';
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
