import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OrganizationService } from '@energy-platform/shared/services';
import { Organization } from '@energy-platform/shared/interfaces';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

@Component({
  selector: 'energy-platform-new-organization',
  templateUrl: './new-organization.component.html',
  styleUrls: ['./new-organization.component.scss'],
})
export class NewOrganizationComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  organizations: Organization[] = [];
  form: FormGroup;
  loading = false;
  // TODO init with mdi-domain logo
  logoURL = '';
  name = '';
  description = '';

  constructor(
    public dialogRef: MatDialogRef<NewOrganizationComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: {
      name: string;
      description: string;
    },
    private organizationService: OrganizationService,
    private fb: FormBuilder
  ) {
    super();
    this.form = this.fb.group({
      organizationName: ['', Validators.required],
      organizationDescription: ['', Validators.required],
      organizationLogo: [this.logoURL, Validators.required],
    });
  }

  ngOnInit() {
    this.loading = true;
  }

  onSubmit() {
    this.dialogRef.close(this.data);
    setTimeout(() => {}, 0);
  }

  loadDefaultLogo() {
    this.logoURL = '';
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
