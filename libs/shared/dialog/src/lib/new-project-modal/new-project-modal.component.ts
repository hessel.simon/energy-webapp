import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User, Project } from '@energy-platform/shared/interfaces';
import { UserService, ProjectsService } from '@energy-platform/shared/services';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

interface ProjectToDisplay {
  id: string;
  name: string;
  description: string;
}

@Component({
  selector: 'energy-platform-new-project-modal',
  templateUrl: './new-project-modal.component.html',
  styleUrls: ['./new-project-modal.component.scss'],
})
export class NewProjectModalComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit, OnDestroy
{
  projects: ProjectToDisplay[] = [];
  template = {
    description: '',
    projectId: '',
    releaseId: '',
  };
  emptytemplate = '';
  users: User[] = [];
  private userMe: User | undefined;
  projectName = '';
  projectDescription = '';

  constructor(
    public dialogRef: MatDialogRef<NewProjectModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, //kriegt verschiedenes übergeben: einmal nur projectid = -1, dann projectId = -1, userId, first- and lastname, dann gibt es projectId, projectDescription und userId oder nur project und userid
    private projectsService: ProjectsService,
    private router: Router,
    private usersService: UserService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.getProjects();
    this.getUsers();
  }

  // private getProjects() {
  //   this.projectsService
  //     .getAllProjectsListener()
  //     .pipe(
  //       map<Project[], ProjectToDisplay[]>((response: Project[]) => {
  //         return response.map((el) => {
  //           return {
  //             id: el.id,
  //             name: el.name,
  //             description: el.description,
  //           };
  //         });
  //       })
  //     )
  //     .subscribe(
  //       (data: ProjectToDisplay[]) => {
  //         this.projects[0] = { id: '0', name: 'default', description: '' };
  //         for (const proj of data) {
  //           let duplicate = false;
  //           for (const p of this.projects) {
  //             if (proj.id === p.id) {
  //               duplicate = true;
  //             }
  //           }
  //           if (!duplicate) this.projects.push(proj);
  //         }
  //         this.getProjectToLoad();
  //       },
  //       (error: { status: string; statusText: string }) => {
  //         this.snackBar.open(
  //           `Error ${error.status}: ${error.statusText}`,
  //           'Ok',
  //           {
  //             duration: 1500,
  //           }
  //         );
  //         this.logger.error('Error when loading existing projects: ' + error);
  //       }
  //     );
  //   this.projectsService.fetchAllAccessibleProjects();
  // }
  getProjects() {
    this.subs.sink = this.projectsService
      .fetchAllAccessibleProjects(true, this.data.userId)
      .subscribe(
        () => {},
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
    this.projectsService.getOpenProjectsListener().subscribe(
      (ProjectToDisplay: Project[]) => {
        this.projects[0] = { id: '0', name: 'default', description: '' };
        for (const proj of ProjectToDisplay) {
          let duplicate = false;
          for (const p of this.projects) {
            if (proj.id === p.id) {
              duplicate = true;
            }
          }
          if (!duplicate) this.projects.push(proj);
        }
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );

    // this.subs.add(this.privateProjectsSub);
  }

  private getUsers() {
    this.usersService.getUserListener().subscribe(
      (data) => {
        this.users = data;
        // remove own user profile
        this.usersService.getOwnUserProfile().subscribe(
          (user) => {
            let index = -1;
            if (user) {
              this.userMe = user;

              for (let i = 0; i < this.users.length; i++) {
                if (this.users[i].id == this.userMe.id) {
                  index = i;
                }
              }
            } else this.logger.error('User not found');
            if (index > -1) {
              this.users.splice(index, 1);
            }
          },
          (error) => {
            this.snackBar.open(
              `Error ${error.status}: ${error.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
          }
        );
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.usersService.fetchAllUsers();
  }

  private getProjectToLoad() {
    if (this.data.projectID !== -1) {
      this.template.projectId = this.data.projectID;
      this.template.description = this.data.projectDescription;
    }
  }

  onSubmit() {
    if (this.template.description === '') {
      this.projectsService
        .addNewProject(
          this.projectName,
          'wind',
          this.projectDescription,
          this.emptytemplate
        )
        .subscribe(() => this.dialogRef.close());
    } else {
      this.projectsService
        .duplicateProject(
          this.projectName,
          'wind',
          this.projectDescription,
          this.template
        )
        .subscribe(() => this.dialogRef.close());
    }
    this.router.navigate(['/app/dashboard']);
  }

  redirectTo(uri: string) {
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then(() => this.router.navigate([uri]));
  }

  closeDialog() {
    this.dialogRef.close();
  }

  changeProjectId(projectId: string) {
    this.template.projectId = projectId;
    for (const val of this.projects) {
      if (val.id === projectId) {
        this.template.description = val.description;
      }
    }
  }
}
