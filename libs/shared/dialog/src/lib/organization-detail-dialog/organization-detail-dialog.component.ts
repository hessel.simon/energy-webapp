import { Component, ElementRef, Inject, OnInit } from '@angular/core';
import { Organization } from '@energy-platform/shared/interfaces';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { OrganizationService } from '@energy-platform/shared/services';
import { ViewChild } from '@angular/core';

@Component({
  selector: 'energy-platform-organization-detail-dialog',
  templateUrl: './organization-detail-dialog.component.html',
  styleUrls: ['./organization-detail-dialog.component.scss'],
})
export class OrganizationDetailDialogComponent implements OnInit {
  organization!: Organization;
  loading!: boolean;
  form!: FormGroup;
  logoURL!: string;
  defaultLogo = true;

  constructor(
    public dialogRef: MatDialogRef<OrganizationDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private organizationService: OrganizationService
  ) {}

  @ViewChild('FileSelectInputDialog', { static: false })
  FileSelectInputDialog!: ElementRef;

  public OpenAddFilesDialog() {
    const e: HTMLElement = this.FileSelectInputDialog.nativeElement;
    e.click();
  }

  ngOnInit() {
    this.createForm();
  }

  loadDefaultLogo() {}

  onSubmit() {
    this.organizationService.updateOrganization(
      this.data.organization,
      this.form.value.organizationName,
      this.form.value.organizationDescription,
      ''
    );
    // return the form values to update the register form
    this.dialogRef.close(this.form.value);
  }

  private createForm() {
    this.form = this.fb.group({
      organizationName: [
        this.data.organization.organizationName,
        Validators.required,
      ],
      organizationDescription: [
        this.data.organization.organizationDescription,
        Validators.required,
      ],
      organizationLogo: [this.data.organization.logoURL, Validators.required],
    });
    this.loading = true;
  }

  closeDialog() {
    this.dialogRef.close(this.form.value);
  }
}
