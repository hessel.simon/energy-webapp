import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contributor, Project, User } from '@energy-platform/shared/interfaces';
import { UserService, ProjectsService } from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform--project-detail-dialog',
  templateUrl: './project-detail-dialog.component.html',
  styleUrls: ['./project-detail-dialog.component.less'],
})
export class ProjectDetailDialogComponent implements OnInit {
  project!: Project;
  loading!: boolean;
  users: User[] = [];
  form!: FormGroup;
  contributors: Contributor[] = [];
  usersNotInProject: User[] = [];

  constructor(
    public dialogRef: MatDialogRef<ProjectDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private usersService: UserService,
    private projectService: ProjectsService,
    private snackBar: MatSnackBar
  ) {} // change type from any

  ngOnInit() {
    this.getUsers();
  }

  private getUsers() {
    this.usersService.getUserListener().subscribe(
      (allUsers) => {
        this.users = allUsers;
        this.project = this.data.project;
        this.contributors = this.project.contributors;
        // eslint-disable-next-line prefer-const
        let ids = [];
        for (let j = 0; j < this.contributors.length; j++) {
          ids.push(this.contributors[j]['user'].id);
        }
        for (let i = 0; i < this.users.length; i++) {
          if (!(ids.indexOf(this.users[i].id) > -1)) {
            if (!this.users[i].deleted) {
              this.usersNotInProject.push(this.users[i]);
            }
          }
        }
        this.createForm();
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.usersService.fetchAllUsers();
  }

  private createForm() {
    this.form = this.fb.group({
      projectName: [this.data.project.name, Validators.required],
      projectDescription: [this.data.project.description, Validators.required],
      collaboratiors: [this.data.project.contributors],
    });
    this.loading = true;
  }

  getUserToDisplay(id: string) {
    const userToDisplay = this.users.find((user) => user.id === id);
    if (userToDisplay)
      return userToDisplay.firstName + ' ' + userToDisplay.lastName;
    return '';
  }

  onSubmit() {
    this.data.project.name = this.form.value.projectName;
    this.data.project.contributors = this.form.value.collaboratiors;
    this.data.project.description = this.form.value.projectDescription;
    this.projectService.editProject(this.data.project);
    let newContributors = this.form.value.collaboratiors;
    if (!newContributors) {
      newContributors = [];
    }
    for (let i = 0; i < newContributors.length; i++) {
      if (!newContributors[i]) continue;
      this.projectService.addContributor(
        this.data.project.id,
        newContributors[i]
      );
    }
    this.dialogRef.close();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
