import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'energy-platform-publish-warning',
  templateUrl: './publish-warning.component.html',
  styleUrls: ['./publish-warning.component.scss'],
})
export class PublishWarningComponent {
  constructor(
    public dialogRef: MatDialogRef<PublishWarningComponent>,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public data: { projectId: string }
  ) {}

  publishToGit() {
    this.dialogRef.close(true);
  }

  abortPublish() {
    this.dialogRef.close(false);
  }

  goToResultPage() {
    this.abortPublish();
    this.router.navigate([`app/result/${this.data.projectId}`]);
  }
}
