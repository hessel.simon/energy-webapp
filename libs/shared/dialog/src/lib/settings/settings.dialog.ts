import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { SettingsDialogData } from '@energy-platform/shared/interfaces';
import { Organization } from '@energy-platform/shared/interfaces';
import { NewOrganizationComponent } from '../new-organization/new-organization.component';
import { OrganizationService } from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from '@energy-platform/shared/services';
import { legalChars } from './legalChars';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
//import {RegisterDialogComponent} from "../register/register.dialog";

@Component({
  selector: 'energy-platform-settings-dialog',
  templateUrl: './settings.dialog.html',
  styleUrls: ['./settings.dialog.scss'],
})
export class SettingsDialogComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  hide: boolean;
  organizations: Organization[] | undefined;
  orgaName = '';
  orgaDescription = '';
  newOrganizationSelected = false;
  hideOld = true;
  hideNew = true;
  oldPasswordWasWrong = false;
  newPasswordIsSameAsOldPassword = false;
  oldPassword: string | undefined;
  newPassword: string | undefined;

  /* Characters that can be used in passwords */

  /* The minimum of characters required in a password */
  pwMinLength = 8;

  constructor(
    public dialogRef: MatDialogRef<SettingsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SettingsDialogData,
    private organizationService: OrganizationService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private usersService: UserService //private registerSerice: RegisterDialogComponent,
  ) {
    super();
    this.hide = true;
    //this.minimalPasswordLength = registerSerice.pwMinLength;
  }

  ngOnInit() {
    this.subs.sink = this.organizationService
      .getOrganizationListenerPublic()
      .subscribe(
        (res) => {
          this.organizations = res;
        },
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
    this.organizationService.fetchOrganizationAllPublic();
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  update(data: SettingsDialogData) {
    this.dialogRef.close(data);
  }

  newOrganization() {
    this.data.organizationId = '0';
    const dialogNewOrga = this.dialog.open(NewOrganizationComponent, {
      data: {
        name: this.orgaName,
        description: this.orgaDescription,
      },
    });
    this.subs.sink = dialogNewOrga.afterClosed().subscribe(
      (orgaData) => {
        if (orgaData !== undefined) {
          this.data.newOrganizationName = orgaData.name;
          this.data.newOrganizationDescription = orgaData.description;
        }
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.newOrganizationSelected = true;
  }

  editPassword(data: SettingsDialogData) {
    if (data.oldPassword === data.newPassword) {
      this.newPasswordIsSameAsOldPassword = true;
      return;
    }
    this.subs.sink = this.usersService
      .editPassword(data.oldPassword, data.newPassword)
      .subscribe(
        (response) => {
          this.logger.info('Response after correct password: ', response);
          this.closeDialog();
          if (response) {
            this.openSnackBar('Password was changed successfully.', 'Ok');
          }
        },
        (error) => {
          this.logger.error('Response after wrong password: ', error);
          this.oldPasswordWasWrong = true;
          this.snackBar.open(
            //`Error ${error.status}: ${error.statusText}`,
            `Old password incorrect`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
    //this.oldPassword = "";
    //this.newPassword = "";
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }

  isInvalid(pwd: string): boolean {
    if (pwd) {
      for (let i = 0; i < pwd.length; i++) {
        let okay = 0;
        for (const s in legalChars) {
          if (pwd.charAt(i) == legalChars[s]) okay = 1;
        }
        if (okay == 0) return true;
      }
      return false;
    } else {
      return false;
    }
  }

  tooShort(pwd: string): boolean {
    return pwd ? pwd.length < this.pwMinLength : false;
  }

  onOldPasswordChange(pwd: string) {
    if (pwd === 'oldPassword') {
      this.oldPasswordWasWrong = false;
    } else if (pwd === 'newPassword') {
      this.newPasswordIsSameAsOldPassword = false;
    }
  }
}
