import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-user-detail-dialog',
  templateUrl: './user-detail-dialog.component.html',
  styleUrls: ['./user-detail-dialog.component.scss'],
})
export class UserDetailDialogComponent implements OnInit {
  form!: FormGroup;
  loading!: boolean;

  constructor(
    public dialogRef: MatDialogRef<UserDetailDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private usersService: UserService
  ) {} // change type from any

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    // if we will have feature "create new user" the that`s to change

    this.form = this.fb.group({
      firstName: [this.data.user.firstName, Validators.required],
      lastName: [this.data.user.lastName, Validators.required],
      email: [this.data.user.email, Validators.required],
    });
    this.loading = true;
  }

  onSubmit() {
    this.data.user.firstName = this.form.value.firstName;
    this.data.user.lastName = this.form.value.lastName;
    this.data.user.email = this.form.value.email;
    this.usersService.editUser(this.data.user);
    this.dialogRef.close();
  }

  closeDialog() {
    this.dialogRef.close();
  }
}
