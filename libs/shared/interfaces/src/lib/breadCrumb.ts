export interface BreadCrumb {
  id: number;
  label: string;
  url: string;
  parameter?: unknown;
}
