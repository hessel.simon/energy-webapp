export interface ChartItem {
  name: string;
  value: unknown;
}

export interface BarVerticalChart {
  items: ChartItem[];
}

export interface LineChartItem {
  name: string;
  series: ChartItem[];
}

export interface LineChart {
  items: LineChartItem[];
}
