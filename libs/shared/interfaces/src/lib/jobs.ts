import { Task } from './task';

export enum JobStatus {
  INPROGRESS = 'in progress',
  DONE = 'done',
  ABORTED = 'aborted',
}

export enum JobType {
  OPTIMIZATION = 'optimization',
  SIMULATION = 'simulation',
  CABELING = 'cabeling',
}

export interface Job {
  id: number;
  // projectName: string;
  projectId: number;
  jobType: JobType;
  startingTime: Date;
  expectedEndTime: Date;
  // userCreator: string;
  userCreator: number;
  status: JobStatus;
  expectedDuration: string; // ??? '5 days' or '20 hours'
  isRunning: boolean; // if job is currently running true if not false
}

export interface JobsWithStatus {
  status: string;
  jobs: Job[];
}

// seems like jobs are deprecated
export interface TasksWithStatus {
  status: string;
  tasks: Task[];
}
