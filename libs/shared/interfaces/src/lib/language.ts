export interface Language {
  abbreviation: string;
  fullName: string;
}
