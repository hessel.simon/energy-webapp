export interface AddUserMailDialogInterface {
  email: string;
  text: string;
}
