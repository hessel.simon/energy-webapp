export interface MenuEntry {
  title: string;
  path: string;
  icon: string;
}
