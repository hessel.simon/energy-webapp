export interface WindBins {
  wind_measurement_height: number;
  directions: number;
  data: number[][][];
  direction_probability: number[][];
}

export interface WindWeibullItem {
  direction: number;
  direction_probability: number;
  scale: number;
  shape: number;
  timeslot: number;
}

export interface WindWeibull {
  comment?: string;
  comment_wind_measurement_height?: string;
  wind_measurement_height: number;
  weibull: WindWeibullItem[];
}
