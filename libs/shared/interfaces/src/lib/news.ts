export interface News {
  id: string;
  headline: string;
  text: string;
  draft: boolean;
  date: Date;
}
