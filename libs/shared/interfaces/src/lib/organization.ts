import { User } from './user';

export interface Organization {
  id: string;
  organizationName: string;
  organizationDescription: string;
  users: User[];
  logo: string;
}

export type OrganizationDTO = Omit<
  Organization,
  'logo' | 'organizationDescription' | 'organizationName'
> & {
  logoURL: Organization['logo'];
  description: Organization['organizationDescription'];
  name: Organization['organizationName'];
};
