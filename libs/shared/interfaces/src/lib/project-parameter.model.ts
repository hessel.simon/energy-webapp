export interface Site {
  surface_roughness: number;
  field_boundaries_long_lat: Array<Array<number>>;
  restricted_areas_long_lat: Array<Array<Array<number>>>;
  substations_long_lat: Array<Array<number>>;
}

export interface Turbine {
  name: string;
  cut_out_speed: number;
  cut_in_speed: number;
  hub_height: number;
  rotor_diameter: number;
  ct_table: Array<number>;
  power_table: Array<number>;
}

export interface SimulationModel {
  wake_model: string;
  wind_data_type: string;
  interpolation_method: string;
  turbine_height_wind_speed_adjustment: boolean;
  num_wind_directions: number;
  num_wind_speeds: number;
  loss_plant_performance_percentage: number;
  loss_power_curve_percentage: number;
  loss_wake_effect_percentage: number;
  loss_wind_speed_percentage: number;
}

export interface Economics {
  cost_per_turbine: number;
  cost_per_turbine_foundation: number;
  compute_foundation_cost: boolean;
  cost_per_substation: number;
  cable_laying_cost_per_cable_length: number;
  project_management_cost: number;
  cost_per_operation: number;
  interest_rate: number;
  project_lifetime: number;
  energy_price_tariff: Array<number>;
}

export interface CableType {
  id: number;
  material_cost: number;
  current_rating: number;
  cable_capacitance: number;
  ac_resistance: number;
  insulation_loss_factor: number;
}

export interface Cabling {
  cable_types: Array<CableType>;
  power_loss_percentage: number;
  cable_voltage: number;
  structure: string;
  site_main_frequency: number;
  max_cable_types: number;
  cost_cable_connect: number;
  cost_generator: number;
}

export interface Uq {
  uq_type_comment: string;
  pdf_type_comment: string;
  uncertainties_name_comment: string;
  method: string;
  sample_size: number;
  uq_windspeed: {
    consider: boolean;
    pdf_type: string;
    p90_percentage: number;
  };
  uq_wakeeffect: {
    consider: boolean;
    pdf_type: string;
    p90_percentage: number;
  };
  uq_ctcurve: { consider: boolean; pdf_type: string; p90_percentage: number };
  uq_surfaceroughness: {
    consider: boolean;
    pdf_type: string;
    p90_percentage: number;
  };
  uq_powercurve: {
    consider: boolean;
    pdf_type: string;
    p90_percentage: number;
  };
  uq_plantperformance: {
    consider: boolean;
    pdf_type: string;
    p90_percentage: number;
  };
  sparse_grid_level: number;
}

export interface Optimization {
  multi_step: {
    optimization_steps: Array<string>;
    objective_function: string;
    num_turbines: number;
    pattern_search_method: string;
    pattern_combinations_per_parameter: number;
    openmp_threads: number;
  };
  optimization_mode: string;
  close_packing: {
    matrix_radius_factor: number;
    matrix_simulations_per_side: number;
    interpolated_positions_per_side: number;
    number_threshold_steps: number;
    threshold_range: Array<number>;
  };
  hexagonal_grid: {
    rotation_angle_interval: Array<number>;
    hexagon_side_interval: Array<number>;
    vertical_stretch_interval: Array<number>;
  };
  honeycomb_grid: {
    expansion_interval: Array<number>;
  };
  slanted_grid: {
    shearing_angle_interval: Array<number>;
    rows_distance_interval: Array<number>;
    cols_distance_interval: Array<number>;
  };
  spiral_grid: {
    density_interval: Array<number>;
    relative_zoom_interval: Array<number>;
    vertical_stretch_interval: Array<number>;
  };
  local_search: {
    num_circles: number;
    num_circle_positions: Array<number>;
    radial_distance: number;
    iteration_range: Array<number>;
    termination_relative_convergence: number;
    frequency_output_solution: number;
  };
  simulated_annealing: {
    temperature_add: number;
    temperature_move: number;
    temperature_remove: number;
    alpha_add: number;
    alpha_move: number;
    alpha_remove: number;
    convergence_criteria: number;
    move_steps: number;
    num_distances: number;
    move_directions: number;
    useFirstFit: boolean;
  };
  multi_objective: {
    pareto_functions: Array<string>;
    weighting_number: number;
    weighting: Array<string>;
  };
}

export interface Meteo {
  wind_measurement_height: number;
  number_direction_slots: number;
  meteo_filetype: string;
}

export interface resultsSimulation {
  final_result: {
    economics: {
      annual_efficiency: number;
      annual_o_and_m_costs: number;
      cabling_costs: number;
      foundation_costs: number;
      internal_rate_of_return: number;
      investment_costs: number;
      levelized_cost_of_energy: number;
      levelized_cost_of_energy_cable_efficiency: number;
      net_annual_energy_production: number;
      net_present_value: number;
      payback_period: number;
      substation_costs: number;
      turbine_costs: number;
    };
  };
}

export interface resultsOptimization {
  final_result: {
    economics: {
      annual_efficiency: number;
      annual_o_and_m_costs: number;
      cable_length_efficiency: number;
      cabling_length: number;
      cabling_costs: number;
      foundation_costs: number;
      internal_rate_of_return: number;
      investment_costs: number;
      levelized_cost_of_energy: number;
      levelized_cost_of_energy_cable_efficiency: number;
      net_annual_energy_production: number;
      net_present_value: number;
      payback_period: number;
      substation_costs: number;
      turbine_costs: number;
    };
  };
}

export interface sensitivityAnalysisQuantils {
  bound_max: number;
  bound_min: number;
  kurtosis: number;
  mean: number;
  p2: number;
  p25: number;
  p50: number;
  p75: number;
  p98: number;
  raw_values: number[];
  skewness: number;
  standard_deviation: number;
  variance: number;
}
export interface resultsSensitivityAnalysis {
  annual_efficiency: sensitivityAnalysisQuantils;
  internal_rate_of_return: sensitivityAnalysisQuantils;
  levelized_cost_of_energy: sensitivityAnalysisQuantils;
  net_annual_energy_production: sensitivityAnalysisQuantils;
  net_present_value: sensitivityAnalysisQuantils;
  payback_period: sensitivityAnalysisQuantils;
}

export interface Printer3d {
  main_wind_direction: number;
  print_restricted_areas: boolean;
  print_topography: boolean;
  version: string;
}

export interface ProjectParameter {
  site: Site;
  turbine: Turbine;
  simulationModel: SimulationModel;
  economics: Economics;
  cabling: Cabling;
  uq: Uq;
  optimization: Optimization;
  meteo: Meteo;
  locations: string;
  topography: string;
  positions: string;
  simulation_result: resultsSimulation;
  optimization_result: resultsOptimization;
  sensitivityAnalysis: resultsSensitivityAnalysis;
  printer3d: Printer3d;
  printer3d_result: string;
}

export type Config = {
  unit: string;
  minimum: number;
  maximum: number;
  step: number;
};
