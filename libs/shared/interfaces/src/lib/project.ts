import { ProjectParameter } from './project-parameter.model';
import { Location } from './location';
import { Task } from './task';

export enum Right {
  READ = 'read',
  WRITE = 'write',
  MANAGE = 'manage',
  OWN = 'own',
}

export enum ProjectStatus {
  HIDDEN = 'hidden',
  PUBLIC = 'public',
  UNDER_REVIEW = 'on_review',
}

export interface Creator {
  id: string;
  firstName: string;
  lastName: string;
  organization: {
    name: string;
    _id: string;
  };
}

export interface Contributor {
  id: string;
  user: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    organization: string;
  };
  project: string;
  rights: Right;
}

export interface Project {
  id: string;
  name: string;
  repository_name: string;
  description: string;
  type: string;
  creationDate: Date;
  published_web_app: boolean;
  creator: Creator;
  location: Location;
  isPublic: boolean;
  projectStatus: ProjectStatus;
  parameters?: ProjectParameter;
  contributors: Array<Contributor>;
  tasks: Array<Task>;
  deprecatedTasks: Array<string>;
  releases: Release[];
}

export interface Release {
  id: string;
  version: string;
  status: 'hidden' | 'public' | 'on_review';
  timestamp: Date;
}

export interface ProjectProgress {
  progress: number;
  status: string;
}

export type ProjectPermissionPipeInput = [Project, string];

// item type in the localstorage caracterising a project  and the his selected release
export interface StoreItem {
  projectId: string;
  activeReleaseId: string | undefined;
}

export type ProjectDTO = Omit<
  Project,
  'releases' | 'creationDate' | 'location' | 'projectStatus' | 'contributors'
> & {
  contributors?: Project['contributors'];
  published_versions: Project['releases'];
  createdAt: Project['creationDate'];
  location: [Project['location']['lat'], Project['location']['lng']] | never[];
  status: Project['projectStatus'];
};

export type PublishRealeaseDTO = { id: string; project: { id: string } };

export type ContributorDTO = Omit<Contributor, 'id' | 'user'> & {
  _id: Contributor['id'];
  user: Omit<Contributor['user'], 'id'> & { _id: Contributor['user']['id'] };
};
