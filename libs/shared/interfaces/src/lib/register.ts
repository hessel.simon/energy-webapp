export interface RegisterDialogData {
  firstName: string;
  lastName: string;
  mail: string;
  password: string;
  organizationId: string;
  newOrganizationName: string;
  newOrganizationDescription: string;
}
