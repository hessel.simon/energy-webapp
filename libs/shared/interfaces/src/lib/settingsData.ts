export interface SettingsDialogData {
  firstName: string;
  lastName: string;
  mail: string;
  organizationId: string;
  newOrganizationName: string;
  newOrganizationDescription: string;
  oldPassword: string;
  newPassword: string;
}
