export interface Task {
  id: string;
  projectId: string;
  projectType: string;
  taskType: TaskTypes;
  deprecated: boolean;
  requestor?: string; // User ID
  status: {
    running: boolean;
    finished: boolean;
    aborted: boolean;
    error: string;
  };
  times: {
    request: Date;
    start: Date;
    finish: Date;
  };
}

export enum TaskTypes {
  simulation = 'simulation',
  sensitivityAnalysis = 'sensitivityAnalysis',
  generate3dPrinterFile = 'generate3dPrinterFile',
  optimization = 'optimization',
  generateTopography = 'generateTopography',
  generateWindData = 'generateWindData',
}
