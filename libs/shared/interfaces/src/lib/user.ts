import { Organization, OrganizationDTO } from './organization';
import { Task } from './task';

export interface User {
  id: string;
  acceptedLegalTermsVersion: number;
  active: boolean;
  adminUnlocked: boolean;
  email: string;
  emailVerified: boolean;
  firstName: string;
  lastLogin: Date;
  deleted: boolean;
  isAdmin: boolean;
  lastName: string;
  organization: Organization;
  registrationDate: Date;
  tasks: Task[];
  numberCurrentlyRunningJobs: number;
  numberFinishedJobs: number;
}

export type UserDTO = Omit<User, 'active' | 'organization'> & {
  deactivated: unknown;
  organization: OrganizationDTO;
};
