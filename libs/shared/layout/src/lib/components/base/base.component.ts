import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';
import {
  AuthenticationService,
  UserService,
} from '@energy-platform/shared/services';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
@Component({
  selector: 'energy-platform-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.scss'],
})
export class BaseComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  isLoggedIn;

  constructor(
    private userService: UserService,
    private authenticationService: AuthenticationService,
    private translate: TranslateService
  ) {
    super();
    this.isLoggedIn = this.authenticationService.getIsLoggedIn;
  }

  private checkIfLoggedIn() {
    this.userService.getOwnUserProfile().subscribe(
      (user) => {
        this.logger.info('logged in: ', user);
      },
      (error) => {
        this.logger.error('not logged in: ', error);
      }
    );
  }

  ngOnInit(): void {
    this.checkIfLoggedIn();
  }
}
