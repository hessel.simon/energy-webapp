import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'energy-platform-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent {
  @Input()
  isLoggedIn = false;
  constructor(private router: Router) {}
  navigateToImprint() {
    this.router.navigate(['/imprint']);
  }
  navigateToPrivacy() {
    this.router.navigate(['/declaration', { id: 'privacy' }]);
  }

  navigateToDisclaimer() {
    this.router.navigate(['/declaration', { id: 'disclaimer' }]);
  }
  reportBug() {
    window.location.href =
      'mailto:sunflower@rwth-aachen.de?subject=Windflower Report Bug';
  }
  requestFeature() {
    window.location.href =
      'mailto:sunflower@rwth-aachen.de?subject=Windflower Request Feature';
  }
}
