import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Input } from '@angular/core';

//Login imports:
import {
  LoginDialogData,
  RegisterDialogData,
} from '@energy-platform/shared/interfaces';

import { environment } from 'environments/environment';
import {
  AuthenticationService,
  UserService,
} from '@energy-platform/shared/services';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { catchError, switchMap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { LoginComponent } from '@energy-platform/shared/authentication';
import { RegisterComponent } from '@energy-platform/shared/authentication';

@Component({
  selector: 'energy-platform-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends UnsubscribeOnDestroyAdapter {
  @Input()
  isLoggedIn = false;
  mail!: string;
  password!: string;
  firstName!: string;
  lastName!: string;
  organizationId!: string;
  newOrganizationName: any;
  newOrganizationDescription: any;
  organization: any;

  constructor(
    private authenticationService: AuthenticationService,
    private readonly userService: UserService,
    private router: Router,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    super();
    //this.logger.info('Test');
    //this.logger.warn('Test');
    //this.logger.error('Test');
  }

  resetFormData(): void {
    this.mail = '';
    this.firstName = '';
    this.lastName = '';
    this.password = '';
  }

  public openRegisterModal(): void {
    const dialogRef = this.dialog.open(RegisterComponent, {
      width: '400px',
      data: {
        mail: this.mail,
        password: this.password,
        firstName: this.firstName,
        lastName: this.lastName,
        organization: this.organization,
      },
    });

    dialogRef
      .afterClosed()
      .subscribe((registerFormData: RegisterDialogData) => {
        if (registerFormData !== undefined) {
          this.mail = registerFormData.mail;
          this.password = registerFormData.password;
          this.firstName = registerFormData.firstName;
          this.lastName = registerFormData.lastName;
          this.organizationId = registerFormData.organizationId;
          this.newOrganizationName = registerFormData.newOrganizationName;
          this.newOrganizationDescription =
            registerFormData.newOrganizationDescription;
          if (this.mail !== undefined && this.password !== undefined) {
            if (
              this.newOrganizationName !== undefined &&
              this.newOrganizationDescription !== undefined
            ) {
              this.authenticationService
                .registerWithNewOrganization(
                  this.firstName,
                  this.lastName,
                  this.mail,
                  this.password,
                  this.newOrganizationName,
                  this.newOrganizationDescription,
                  ''
                )
                .subscribe(
                  (success) => {
                    if (success) {
                      this.snackBar.open(
                        'User was registered successfully',
                        'Ok',
                        {
                          duration: environment.MAT_ERROR_DEFAULT,
                        }
                      );

                      this.loginAfterRegistered();

                      this.resetFormData();
                    }
                  },
                  (error) => {
                    this.snackBar.open(
                      `Error ${error.status}: ${error.statusText}`,
                      'Ok',
                      {
                        duration: environment.MAT_ERROR_DEFAULT,
                      }
                    );
                  }
                );
            } else {
              this.authenticationService
                .register(
                  this.firstName,
                  this.lastName,
                  this.mail,
                  this.password,
                  this.organizationId
                )
                .subscribe(
                  (success) => {
                    if (success) {
                      this.snackBar.open(
                        'User was registered successfully',
                        'Ok',
                        {
                          duration: environment.MAT_ERROR_DEFAULT,
                        }
                      );

                      this.loginAfterRegistered();

                      this.resetFormData();
                    }
                  },
                  (error) => {
                    //email already in use
                    this.snackBar.open(
                      `Error ${error.status}: Email already in use`,
                      'Ok',
                      {
                        duration: environment.MAT_ERROR_DEFAULT,
                      }
                    );
                    this.logger.error(error);
                  }
                );
            }
          }
        }
      });
  }

  public openLoginModal(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      data: { mail: this.mail, password: this.password },
    });

    dialogRef.afterClosed().subscribe((loginFormData: LoginDialogData) => {
      if (String(loginFormData) === 'registerUser') {
        this.openRegisterModal();
      }

      if (loginFormData !== undefined) {
        this.mail = loginFormData.mail;
        this.password = loginFormData.password;
        if (this.mail !== undefined && this.password !== undefined) {
          this.authenticationService
            .login(this.mail, this.password)
            .pipe(
              catchError((error) => {
                this.snackBar.open('Something went wrong!', 'Ok', {
                  duration: environment.MAT_ERROR_DEFAULT,
                });
                return throwError(error);
              }),
              switchMap(() => {
                this.resetFormData();

                // Get user information
                return this.userService.getOwnUserProfile();
              }),
              catchError((error) => {
                this.snackBar.open(
                  `Error ${error.status}: ${error.statusText}`,
                  'Ok',
                  {
                    duration: environment.MAT_ERROR_DEFAULT,
                  }
                );
                this.logger.error(
                  'Error when getting detailed user information',
                  error
                );
                return throwError(error);
              })
            )
            .subscribe(
              (user) => {
                const isAdmin: boolean = user.isAdmin;
                isAdmin
                  ? this.router.navigate(['/admin/dashboard'])
                  : this.router.navigate(['/app/dashboard']);
                if (this.router.url.endsWith('/app/dashboard'))
                  window.location.reload();
              },
              (error) => {
                this.snackBar.open('Wrong email or password!', 'Ok', {
                  duration: environment.MAT_ERROR_DEFAULT,
                });
                this.logger.error(error);
              }
            );
        }
      }
    });
  }

  loginAfterRegistered(): void {
    this.logger.info('user was registered successfully');
    this.logger.info('trying login');

    this.authenticationService
      .login(this.mail, this.password)
      .pipe(
        catchError((error) => {
          this.snackBar.open('Something went wrong!', 'Ok', {
            duration: environment.MAT_ERROR_DEFAULT,
          });
          return throwError(error);
        }),
        switchMap(() => {
          this.resetFormData();

          // Get user information
          return this.userService.getOwnUserProfile();
        }),
        catchError((error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: environment.MAT_ERROR_DEFAULT,
            }
          );
          this.logger.error(
            'Error when getting detailed user information',
            error
          );
          return throwError(error);
        })
      )
      .subscribe(
        (user) => {
          const isAdmin: boolean = user.isAdmin;
          isAdmin
            ? this.router.navigate(['/admin/dashboard'])
            : this.router.navigate(['/app/dashboard']);
          if (this.router.url.endsWith('/app/dashboard'))
            window.location.reload();
        },
        (error) => {
          this.snackBar.open('Wrong email or password!', 'Ok', {
            duration: environment.MAT_ERROR_DEFAULT,
          });
          this.logger.error(error);
        }
      );
  }
}
