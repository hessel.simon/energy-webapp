import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import SphericalMercator from '@mapbox/sphericalmercator';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Project } from '@energy-platform/shared/interfaces';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-icon-generator',
  templateUrl: './icon-generator.component.html',
  styleUrls: ['./icon-generator.component.scss'],
})
export class IconGeneratorComponent implements OnInit {
  @Input() project!: Project;
  @Input() isPublic = false;
  coordinates$!: Observable<number[][]>;
  @ViewChild('canvas', { static: true }) canvas!: ElementRef<HTMLCanvasElement>;
  constructor(
    private readonly projectService: ProjectsService,
    private parameterService: ParameterService
  ) {}

  ngOnInit(): void {
    console.log(this.isPublic);

    this.coordinates$ = this.parameterService
      // .getProjectParameters(this.project.id)
      .getParameter(this.project.id, 'positions')
      .pipe(
        map(({ positions, ...rest }) => {
          console.log(rest, positions);
          return positions
            .trim()
            .split('\n')
            .map((value: string) =>
              value.split(';').map((test: string) => Number.parseFloat(test))
            );
        })
      );

    this.initIcon();
  }
  initIcon() {
    const canvas = this.canvas.nativeElement;
    const canvasW = canvas.getBoundingClientRect().width;
    const canvasH = canvas.getBoundingClientRect().height;
    const ctx = canvas.getContext('2d');
    if (ctx === null) return;
    ctx.fillStyle = '#005566';
    ctx.fillRect(0, 0, canvasW, canvasH);
    const sm = new SphericalMercator({});
    this.coordinates$.subscribe((coordinates: number[][]) => {
      // convert coordinates to mercator meters
      const meters = coordinates.map((lonlat) =>
        sm.forward([lonlat[0], lonlat[1]])
      );
      // find minimum/maximum x/y
      const minX = meters.reduce((val, xy) => {
        return Math.min(xy[0], val);
      }, meters[0][0]);
      const minY = meters.reduce((val, xy) => {
        return Math.min(xy[1], val);
      }, meters[0][1]);
      const maxX = meters.reduce((val, xy) => {
        return Math.max(xy[0], val);
      }, meters[0][0]);
      const maxY = meters.reduce((val, xy) => {
        return Math.max(xy[1], val);
      }, meters[0][1]);
      const scale = (Math.max(maxX - minX, maxY - minY) * 1.2) / canvasW;
      const offsetX = (canvasW - (maxX - minX) / scale) / 2;
      const offsetY = (canvasH - (maxY - minY) / scale) / 2;
      ctx.fillStyle = '#ffffff';
      meters.forEach((xy) => {
        const x = (xy[0] - minX) / scale;
        const y = (xy[1] - minY) / scale;
        ctx.fillRect(offsetX + x, offsetY + y, 2, 2);
      });
    });
  }
}
