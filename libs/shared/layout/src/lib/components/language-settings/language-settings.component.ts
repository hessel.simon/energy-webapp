import { Language } from '@energy-platform/shared/interfaces';
import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'energy-platform-language-settings',
  templateUrl: './language-settings.component.html',
  styleUrls: ['./language-settings.component.scss'],
})
export class LanguageSettingsComponent {
  listOfSupportedLanguages: Language[];
  selectedLanguage: Language;

  constructor(private translateService: TranslateService) {
    this.listOfSupportedLanguages = [
      { fullName: 'English', abbreviation: 'en' },
      { fullName: 'Español', abbreviation: 'es' },
      { fullName: 'Deutsch', abbreviation: 'de' },
      { fullName: 'Polski', abbreviation: 'pl' },
    ];

    this.selectedLanguage =
      this.getLanguageByAbbreviation(
        this.translateService.getDefaultLang() || 'English'
      ) || this.listOfSupportedLanguages[0];
    this.translateService.setDefaultLang(this.selectedLanguage.abbreviation);
  }

  getLanguageByAbbreviation(languageAbbreviation: string): Language {
    return this.listOfSupportedLanguages.filter(
      (language) => language.abbreviation === languageAbbreviation
    )[0];
  }

  changeLanguage(languageAbbreviation: string): void {
    if (
      languageAbbreviation !== undefined &&
      this.listOfSupportedLanguages.filter(
        (language) => language.abbreviation === languageAbbreviation
      ).length === 1
    ) {
      this.selectedLanguage =
        this.getLanguageByAbbreviation(languageAbbreviation);
      this.translateService.setDefaultLang(this.selectedLanguage.abbreviation);
      localStorage.setItem('language', this.selectedLanguage.abbreviation);
    }
  }
}
