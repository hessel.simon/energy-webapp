import { Component, Input, OnInit } from '@angular/core';
import { News } from '@energy-platform/shared/interfaces';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import {
  DateFormatterService,
  NewsService,
} from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-newsfeed',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.scss'],
})
export class NewsfeedComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @Input() showDrafts = false;

  newsList: News[] = [];

  constructor(
    private newsService: NewsService,
    private snackBar: MatSnackBar,
    public dateFormatter: DateFormatterService
  ) {
    super();
  }

  ngOnInit() {
    this.loadNews();
    // Destroy is handled by UnsubscribeOnDestroyAdapter
  }

  getNews() {
    return this.newsList;
  }

  loadNews() {
    this.newsService.getNewsListener().subscribe(
      (data: News[]) => (this.newsList = [...data]),
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.newsService.fetchNews();
  }

  downloadTechnicalReport(report: string) {
    const a = document.createElement('a');
    a.href = 'assets/documents/' + report;
    a.download = report;
    document.body.appendChild(a);
    a.click();
  }
}
