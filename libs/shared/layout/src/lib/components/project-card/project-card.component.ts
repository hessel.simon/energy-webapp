import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
// TEMP import { DateFormatterService } from '../services/date-formatter.service';

@Component({
  selector: 'energy-platform-project-card',
  templateUrl: './project-card.component.html',
  styleUrls: ['./project-card.component.scss'],
})
export class ProjectCardComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @Input()
  project: any;

  @Input()
  userId = '';

  @Input()
  type!: 'public' | 'private' | 'on_review';

  publicReleases!: any[];
  defaultRelease!: any;

  permission = {
    1: 'Read',
    2: 'Write',
    3: 'Manage',
    4: 'Own',
  };
  permissionIconAssoc = {
    1: 'description',
    2: 'edit',
    3: 'build',
    4: 'Own',
  };

  /**
   * Local variables
   */
  selectedReleaseId!: string; // just used for public projects
  activeReleaseVersion!: string;

  constructor(
    // TEMP private dateFormatter: DateFormatterService,
    private router: Router
  ) {
    super();
  }

  ngOnInit(): void {
    if (this.project) {
      this.publicReleases = this.project.releases.filter(this.isPublic);
      if (this.publicReleases.length > 0) {
        this.defaultRelease = this.publicReleases[0];
        this.selectedReleaseId = this.defaultRelease.id;
        this.activeReleaseVersion = this.defaultRelease.version;
      }
    }
  }

  isPublic(release: any) {
    return release.status === 'public';
  }

  onCardClick(projectId: string, releaseId: string) {
    if (releaseId !== undefined) {
      this.router.navigateByUrl(`/app/project/${projectId}/${releaseId}`);
      return;
    } else {
      this.router.navigateByUrl(`/app/project/${projectId}`);
    }
  }

  goToParameterPage(projectId: string) {
    if (this.type === 'public') {
      this.router.navigateByUrl(
        `/app/parameter/${projectId}/${this.selectedReleaseId}`
      );
      return;
    }
    this.router.navigateByUrl(`/app/parameter/${projectId}`);
  }

  goTo3DPage(projectId: string) {
    if (this.type === 'public') {
      this.router.navigateByUrl(
        `/app/parameter/visualization/${projectId}/${this.selectedReleaseId}`
      );
      return;
    }
    console.log('HIER', projectId);
    this.router.navigateByUrl(`/app/parameter/visualization/${projectId}`);
  }

  goToResultsPage(projectId: string) {
    if (this.type === 'public') {
      this.router.navigateByUrl(
        `/app/parameter/result/${projectId}/${this.selectedReleaseId}`
      );
      /* const route = `app/parameter/result/`;
      this.router.navigate([route, projectId, this.selectedReleaseId], {
        queryParams: {
          name: this.project.name,
          version: this.activeReleaseVersion,
        },
      }); */
      return;
    }
    this.router.navigateByUrl(`/app/parameter/result/${projectId}`);
  }

  updateRelease(releaseId: string) {
    const releaseName = this.project.releases.filter(
      (el: any) => el.id === releaseId
    )[0].version;
    this.selectedReleaseId = releaseId;
    this.activeReleaseVersion = releaseName;
  }

  preventPropagation(e: Event) {
    e.stopPropagation();
  }
}
