import { Component, OnInit, Input } from '@angular/core';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { ActivatedRoute, Router } from '@angular/router';
import {
  FeatureGroup,
  FitBoundsOptions,
  icon,
  latLng,
  LatLng,
  LatLngBounds,
  marker,
  Marker,
  popup,
  tileLayer,
} from 'leaflet';
import {
  Project,
  ProjectStatus,
  // TEMP Release,
  //Right,
  //User,
} from '@energy-platform/shared/interfaces';
export interface MapBorderBox {
  northEast: LatLng;
  southEast: LatLng;
  southWest: LatLng;
  northWest: LatLng;
}
@Component({
  selector: 'energy-platform-project-map',
  templateUrl: './project-map.component.html',
  styleUrls: ['./project-map.component.scss'],
})
export class ProjectMapComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  // Map
  mainLayer: any;
  markerLayer: Marker[];
  mapFitToBounds!: LatLngBounds;
  mapFitToBoundsOptions: FitBoundsOptions;
  mapBorderBox!: MapBorderBox;
  options: any;

  // Projects
  @Input()
  privateProjects: Project[] = [];
  @Input()
  publicProjects: Project[] = [];
  privateProjectsFiltered: Project[] = [];
  publicProjectsFiltered: Project[] = [];
  allProjects: Project[] = [];

  filters = {
    displayVisible: true, // refreshes project in current map selection
    displayPrivate: true, // shows private projects
    displayPublic: true, // shows public projects
  };

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    super();

    this.markerLayer = [];
    this.mapFitToBoundsOptions = { maxZoom: 15, animate: true };
    this.mainLayer = tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    );

    // used in HTML file
    this.options = {
      layers: [this.mainLayer],
      center: latLng([49.77882445, 6.05932344]),
      zoom: 10,
    };
  }

  ngOnInit(): void {
    this.updateMap();
  }
  updateMap() {
    this.markerLayer = [];

    // Check if projects available. If not return
    if (!this.privateProjects && !this.publicProjects) {
      return;
    }

    // Concat private and public projects if not filtered
    if (this.filters.displayPublic) {
      this.allProjects = this.filters.displayPrivate
        ? [...this.privateProjects, ...this.publicProjects]
        : [...this.publicProjects];
    } else if (this.filters.displayPrivate) {
      this.allProjects = [...this.privateProjects];
    } else {
      return;
    }
    //const allProjects = [...this.privateProjects, ...this.publicProjects];

    this.publicProjectsFiltered = this.publicProjects;
    this.privateProjectsFiltered = this.privateProjects;

    for (const project of this.allProjects) {
      const name = `<p><b>Project Name: </b>${project.name}</p>`,
        creator = `<p><b>Creator: </b>${project.creator.firstName} ${project.creator.lastName}</p>`,
        participants = `<p><b>Participants: </b>${project.contributors.length}</p>`,
        agency = project.creator.organization
          ? `<p><b>Agency: </b>${project.creator.organization.name}</p>`
          : '<p><b>No Agency</b></p>';

      const htmlPopupContent =
        project.projectStatus === ProjectStatus.PUBLIC && project.isPublic
          ? `${name}\n${creator}`
          : project.creator.organization
          ? `${name}\n${participants}\n${creator}\n${agency}`
          : `${name}\n${participants}\n${creator}`;

      const popupContent = popup()
        .setLatLng([project.location.lat, project.location.lng])
        .setContent(htmlPopupContent)
        .openOn(this.mainLayer);

      const newMarker = this.createMarker(project).bindPopup(popupContent);
      newMarker.on('click', () => newMarker.openPopup());

      this.markerLayer.push(newMarker);
    }
    if (this.markerLayer && this.markerLayer.length > 0) {
      const featureGroup = new FeatureGroup(this.markerLayer);
      this.mapFitToBounds = featureGroup.getBounds().pad(0.5);

      this.mapBorderBox = {
        northEast: featureGroup.getBounds().getNorthEast(),
        southEast: featureGroup.getBounds().getSouthEast(),
        southWest: featureGroup.getBounds().getSouthWest(),
        northWest: featureGroup.getBounds().getNorthWest(),
      };
    }
  }
  onMoveend(event: any) {
    this.mapBorderBox = {
      northEast: event.target.getBounds().getNorthEast(),
      southEast: event.target.getBounds().getSouthEast(),
      southWest: event.target.getBounds().getSouthWest(),
      northWest: event.target.getBounds().getNorthWest(),
    };
    if (this.filters.displayVisible) {
      this.filterProjects();
    }
  }
  private filterProjects() {
    this.privateProjects = this.privateProjectsFiltered.filter(
      (value: Project) => {
        if (
          (this.mapBorderBox.southWest.lat <= value.location.lat &&
            value.location.lat <= this.mapBorderBox.northEast.lat) ||
          (this.mapBorderBox.northEast.lat <= value.location.lat &&
            value.location.lat <= this.mapBorderBox.southWest.lat)
        ) {
          if (
            this.mapBorderBox.southWest.lng <= value.location.lng &&
            value.location.lng <= this.mapBorderBox.northEast.lng
          ) {
            return true;
          }
        }
        return false;
      }
    );
    this.publicProjects = this.publicProjectsFiltered.filter(
      (value: Project) => {
        if (
          (this.mapBorderBox.southWest.lat <= value.location.lat &&
            value.location.lat <= this.mapBorderBox.northEast.lat) ||
          (this.mapBorderBox.northEast.lat <= value.location.lat &&
            value.location.lat <= this.mapBorderBox.southWest.lat)
        ) {
          if (
            this.mapBorderBox.southWest.lng <= value.location.lng &&
            value.location.lng <= this.mapBorderBox.northEast.lng
          ) {
            return true;
          }
        }
        return false;
      }
    );
  }
  createMarker(projectProperties: Project): any {
    const markerIcon: string = projectProperties.isPublic
      ? 'assets/images/map/marker-icon.png'
      : 'assets/images/map/marker-icon_own.png';
    return marker(
      [projectProperties.location.lat, projectProperties.location.lng],
      {
        icon: icon({
          iconSize: [44, 61],
          iconAnchor: [13, 41],
          iconUrl: markerIcon,
          shadowUrl: 'assets/images/map/marker-shadow.png',
        }),
        title: projectProperties.name,
      }
    );
  }
  setPublicProjects(publicProjects: Project[]) {
    this.publicProjects = publicProjects;
    this.updateMap();
    this.logger.info('setPublicProjects');
  }
  setPrivateProjects(privateProjects: Project[]) {
    this.privateProjects = privateProjects;
    this.updateMap();
    this.logger.info('setPrivateProjects');
  }
  setFilter(filters: {
    displayVisible: boolean;
    displayPrivate: boolean;
    displayPublic: boolean;
  }) {
    this.filters = filters;
    this.updateMap();
  }
  getPrivateProjectsFiltered(): Project[] {
    return this.privateProjectsFiltered;
  }
  getPublicProjectsFiltered(): Project[] {
    return this.publicProjectsFiltered;
  }
}
