import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  AuthenticationService,
  UserService,
} from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'environments/environment';
import { MatDialog } from '@angular/material/dialog';
import { SettingsDialogComponent } from '@energy-platform/shared/dialog';
import { RegisterDialogData } from '@energy-platform/shared/interfaces';
@Component({
  selector: 'energy-platform-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.scss'],
})
export class UserSettingsComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  isLoggedIn = true;
  username: string | undefined;
  mail: string | undefined;
  firstName: string | undefined;
  lastName: string | undefined;
  organizationId: string | undefined;
  isAdmin = false;
  isInAdminMode = false;
  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private userService: UserService,
    private activatedRoute: ActivatedRoute,
    private location: Location
  ) {
    super();
  }
  ngOnInit(): void {
    this.getIsInAdminMode();

    this.activatedRoute.data.subscribe((resolverData) => {
      this.logger.info('resolver Data: ', resolverData);
      this.userService.getOwnUserProfile().subscribe(
        (serverData: any) => {
          this.logger.info('user Data: ', serverData);
          if (serverData) {
            const userData = serverData;
            if (userData !== null) {
              this.username = `${userData.firstName} ${userData.lastName}`;
              this.mail = userData.email;
              this.firstName = userData.firstName;
              this.lastName = userData.lastName;
              this.organizationId = userData.organization.id;
              this.isAdmin = userData.isAdmin;
              // this.logger.info('this.isAdmin', this.isAdmin);
            } else {
              this.logger.error('UserData === null');
            }
          } else {
            this.snackBar.open(
              `Error ${serverData.status}: ${serverData.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
            this.router.navigate(['/login']);
          }
        },
        (error: any) => {
          this.logger.error('user Data error: ', error);
        }
      );
    });
  }
  /*
  public openSettingsModal(): void {
    const dialogRef = this.dialog.open(SettingsComponent, {
      width: '500px',
      data: {
        mail: this.mail,
        firstName: this.firstName,
        lastName: this.lastName,
        organizationId: this.organizationId,
      },
    });

    dialogRef
      .afterClosed()
      .subscribe((registerFormData: RegisterDialogData) => {
        if (registerFormData !== undefined) {
          this.mail = registerFormData.mail;
          this.firstName = registerFormData.firstName;
          this.lastName = registerFormData.lastName;

          if (registerFormData.newOrganizationName !== undefined) {
            this.userService
              .updateAccountWithNewOrganization(
                registerFormData.firstName,
                registerFormData.lastName,
                registerFormData.mail,
                registerFormData.newOrganizationName,
                registerFormData.newOrganizationDescription,
                ''
              )
              .subscribe((serverResponse) => {
                if (serverResponse.status === 200) {
                  this.snackBar.open(
                    'User account was updated successfully',
                    'Ok',
                    {
                      duration: 1500,
                    }
                  );
                  window.setTimeout(function () {
                    location.reload();
                  }, 1500);
                } else {
                  this.snackBar.open(
                    `Error ${serverResponse.status}: ${serverResponse.statusText}`,
                    'Ok',
                    {
                      duration: 1500,
                    }
                  );
                }
              });
          } else {
            this.userService
              .update(
                registerFormData.firstName,
                registerFormData.lastName,
                registerFormData.mail,
                registerFormData.organizationId
              )
              .subscribe(
                (serverResponse: { status: number; statusText: any }) => {
                  if (serverResponse.status === 200) {
                    this.snackBar.open(
                      'User account was updated successfully',
                      'Ok',
                      {
                        duration: 1500,
                      }
                    );
                    window.setTimeout(function () {
                      location.reload();
                    }, 1500);
                  } else {
                    this.snackBar.open(
                      `Error ${serverResponse.status}: ${serverResponse.statusText}`,
                      'Ok',
                      {
                        duration: 1500,
                      }
                    );
                  }
                },
                (error: { status: any; statusText: string }) => {
                  this.snackBar.open(
                    `Error ${error.status}: ${error.statusText}`,
                    'Ok',
                    {
                      duration: 1500,
                    }
                  );
                  this.logger.error(error);
                }
              );
          }
        }
      });
  }
*/
  logout(): void {
    this.authService.logout().subscribe(
      () => {
        this.router.navigate(['/home']);
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: environment.MAT_ERROR_DEFAULT,
        });
        this.logger.error('Error on logout: ', error);
      }
    );
  }

  public openSettingsModal(): void {
    const dialogRef = this.dialog.open(SettingsDialogComponent, {
      width: '400px',
      data: {
        mail: this.mail,
        firstName: this.firstName,
        lastName: this.lastName,
        organizationId: this.organizationId,
      },
    });

    dialogRef
      .afterClosed()
      .subscribe((registerFormData: RegisterDialogData) => {
        if (registerFormData !== undefined) {
          this.mail = registerFormData.mail;
          this.firstName = registerFormData.firstName;
          this.lastName = registerFormData.lastName;

          if (registerFormData.newOrganizationName !== undefined) {
            this.userService
              .updateAccountWithNewOrganization(
                registerFormData.firstName,
                registerFormData.lastName,
                registerFormData.mail,
                registerFormData.newOrganizationName,
                registerFormData.newOrganizationDescription,
                ''
              )
              .subscribe(
                (success) => {
                  if (success) {
                    this.snackBar.open(
                      'User account was updated successfully',
                      'Ok',
                      {
                        duration: environment.MAT_ERROR_DEFAULT,
                      }
                    );
                    window.setTimeout(function () {
                      location.reload();
                    }, 1500);
                  }
                },
                (error) => {
                  this.snackBar.open(
                    `Error ${error.status}: ${error.statusText}`,
                    'Ok',
                    {
                      duration: environment.MAT_ERROR_DEFAULT,
                    }
                  );
                }
              );
          } else {
            this.userService
              .update(
                registerFormData.firstName,
                registerFormData.lastName,
                registerFormData.mail,
                registerFormData.organizationId
              )
              .subscribe(
                (success) => {
                  if (success) {
                    this.snackBar.open(
                      'User account was updated successfully',
                      'Ok',
                      {
                        duration: environment.MAT_ERROR_DEFAULT,
                      }
                    );
                    window.setTimeout(function () {
                      location.reload();
                    }, 1500);
                  }
                },
                (error) => {
                  //email already in use
                  this.snackBar.open(
                    `Error ${error.status}: Email already in use`,
                    'Ok',
                    {
                      duration: environment.MAT_ERROR_DEFAULT,
                    }
                  );
                  this.logger.error(error);
                }
              );
          }
        }
      });
  }
  private getIsInAdminMode() {
    this.isInAdminMode = this.location.path().startsWith('/admin')
      ? true
      : false;
  }
}
