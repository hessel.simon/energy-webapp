import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { PipesModule } from '@energy-platform/shared/pipes';
import { BaseComponent } from './components/base/base.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { LanguageSettingsComponent } from './components/language-settings/language-settings.component';
import { UserSettingsComponent } from './components/user-settings/user-settings.component';
import { ProjectMapComponent } from './components/project-map/project-map.component';
import { ProjectCardComponent } from './components/project-card/project-card.component';
import { NewsfeedComponent } from './components/newsfeed/newsfeed.component';
// Material Components
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatListModule } from '@angular/material/list';

const components = [
  HeaderComponent,
  FooterComponent,
  BaseComponent,
  LanguageSettingsComponent,
  UserSettingsComponent,
  ProjectMapComponent,
  ProjectCardComponent,
  NewsfeedComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([]),
    MatToolbarModule,
    MatIconModule,
    MatProgressBarModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    TranslateModule.forChild(),
    LeafletModule,
    PipesModule,
    MatFormFieldModule,
    MatSelectModule,
    MatListModule,
  ],
  declarations: components,
  exports: components,
})
export class LayoutModule {}
