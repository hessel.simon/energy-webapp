import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectProgressValuePipe } from './project-progress-value.pipe';
import { ProjectPermissionPipe } from './project-permission.pipe';
import { UserRightByProjectPipe } from './user-right-by-project.pipe';
import { ProjectNameByIdPipe } from './project-name-by-id.pipe';
import { UserNameByIdPipe } from './user-name-by-id.pipe';

const pipes = [
  ProjectProgressValuePipe,
  ProjectPermissionPipe,
  UserRightByProjectPipe,
  ProjectNameByIdPipe,
  UserNameByIdPipe,
];

@NgModule({
  imports: [CommonModule],
  declarations: pipes,
  exports: pipes,
})
export class PipesModule {}
