import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ProjectsService } from '@energy-platform/shared/services';
/*
 * Returns Observable of Project Name of given Project ID.
 *  Attention: You have to subscribe in Template!
 */
@Pipe({ name: 'projectName' })
export class ProjectNameByIdPipe implements PipeTransform {
  constructor(private projectSvc: ProjectsService) {}

  transform(value: string): Observable<string> {
    return this.projectSvc
      .getProject(value)
      .pipe(map((project) => project.name));
  }
}
