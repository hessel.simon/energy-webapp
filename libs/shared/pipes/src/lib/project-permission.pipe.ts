import { Pipe, PipeTransform } from '@angular/core';
import {
  Project,
  ProjectPermissionPipeInput,
  ProjectStatus,
  Right,
} from '@energy-platform/shared/interfaces';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

@Pipe({
  name: 'projectPermissionPipe',
})
export class ProjectPermissionPipe
  extends UnsubscribeOnDestroyAdapter
  implements PipeTransform
{
  transform(value: ProjectPermissionPipeInput, ...args: unknown[]): number {
    return this.getProjectPermission(value[0], value[1]);
  }

  /**
   * Return permission number from user right for this project
   */
  getProjectPermission(project: Project, userId: string): number {
    if (project.projectStatus === ProjectStatus.PUBLIC) {
      return 1;
    } else {
      for (let i = 0; i < project.contributors.length; i++) {
        if (project.contributors[i].user.id === userId) {
          const userRight: Right = project.contributors[i].rights;
          switch (userRight) {
            case Right.READ:
              return 1;
            case Right.WRITE:
              return 2;
            case Right.MANAGE:
              return 3;
            case Right.OWN:
              return 3;
          }
        }
      }
    }
    return 0; // should never happen
  }
}
