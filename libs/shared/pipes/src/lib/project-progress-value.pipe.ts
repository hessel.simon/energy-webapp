import { Pipe, PipeTransform } from '@angular/core';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Project, ProjectProgress } from '@energy-platform/shared/interfaces';

@Pipe({
  name: 'projectProgressValuePipe',
})
export class ProjectProgressValuePipe
  extends UnsubscribeOnDestroyAdapter
  implements PipeTransform
{
  transform(project: Project, ...args: unknown[]): ProjectProgress {
    return this.getProjectProgressBarValues(project);
  }

  getProjectProgressBarValues(project: Project): {
    progress: number;
    status: string;
  } {
    if (project.tasks.length > 0) {
      const result = { progress: 0, status: 'projectOverview.statusAborted' }; //Adapted Values to fit into the translation pipe in projectOverview I did not encounter errors like this
      for (let i = 0; i < project.tasks.length; i++) {
        if (project.tasks[i].status.finished) {
          result.progress = 1;
          //result.status = "Finished";
          result.status = 'projectOverview.statusFinished';
        } else if (project.tasks[i].status.running) {
          return { progress: 0.5, status: 'projectOverview.statusRunning' };
        }
      }
      return result;
    } else {
      return { progress: 0, status: 'projectOverview.statusNotStartetYet' };
    }
  }
}
