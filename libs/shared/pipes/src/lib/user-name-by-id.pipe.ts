import { Pipe, PipeTransform } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserService } from '@energy-platform/shared/services';
/*
 * Returns Observable of Project Name of given Project ID.
 *  Attention: You have to subscribe in Template!
 */
@Pipe({ name: 'userName' })
export class UserNameByIdPipe implements PipeTransform {
  constructor(private userSvc: UserService) {}

  transform(value: string): Observable<string> {
    return this.userSvc
      .getUser(value)
      .pipe(map((user) => `${user?.firstName} ${user?.lastName}`));
  }
}
