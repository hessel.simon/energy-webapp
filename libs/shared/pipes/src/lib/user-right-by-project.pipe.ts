import { Pipe, PipeTransform } from '@angular/core';
import { Project, Right, User } from '@energy-platform/shared/interfaces';
import { compareRights } from '@energy-platform/shared/utils';

@Pipe({
  name: 'userhasRightForProject',
})
export class UserRightByProjectPipe implements PipeTransform {
  transform(
    {
      project,
      user,
      minimumRight,
    }: { project: Project; user?: User; minimumRight: Right },
    ..._args: unknown[]
  ): boolean {
    if (!project.contributors || !user) return false;

    const right = project.contributors.find(
      (contributor) => contributor.user.id === user.id
    )?.rights;
    if (!right) return false;

    return compareRights(right, minimumRight) > 0;
  }
}
