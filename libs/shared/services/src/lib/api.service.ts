import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { BaseStructure } from '@energy-platform/shared/utils';
@Injectable({
  providedIn: 'root',
})
export class APIService extends BaseStructure {
  protected baseURL: string;
  public serviceName: string;
  constructor() {
    super();
    this.baseURL = environment.API_BASE_URL;
    this.serviceName = 'API Service';
  }

  throwError(errorMessage: string): never {
    throw new Error(`${this.serviceName}: ${errorMessage}`);
  }
}
