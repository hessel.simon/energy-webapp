import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APIService } from './api.service';
import {
  CheckTokenResponse,
  LoginResponse,
} from '@energy-platform/shared/interfaces';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { BehaviorSubject, of, throwError } from 'rxjs';

export const TOKEN_NAME = 'authToken';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService extends APIService {
  private isLoggedIn = new BehaviorSubject(false);
  private authToken: string | undefined;
  constructor(private httpInstance: HttpClient) {
    super();
    this.serviceName = 'Authentication Service';
    this.authToken = this.getToken();
    this.isLoggedIn.next(Boolean(this.authToken));
  }

  private clearStorage() {
    this.authToken = undefined;
    localStorage.clear();
  }
  get getIsLoggedIn() {
    return this.isLoggedIn;
  }

  setToken(token: string): void {
    if (!token) {
      this.throwError('Auth Token is undefined and cannot be set');
    }
    localStorage.setItem(TOKEN_NAME, token);
  }

  getToken() {
    if (this.authToken) return this.authToken;

    return localStorage.getItem(TOKEN_NAME) || undefined;
  }

  isTokenValid() {
    const isToken = this.getToken();
    if (!isToken) return of(false);
    return this.httpInstance
      .get<CheckTokenResponse>(`${this.baseURL}/user/check/token`)
      .pipe(
        map((checkToken) => Boolean(checkToken.userId)),
        catchError((err) => {
          this.clearStorage();
          this.isLoggedIn.next(false);
          this.logger.error('Is token valid error: ', err);
          return of(false);
        })
      );
  }

  login(mail: string, password: string) {
    const route = this.baseURL + '/user/login';
    return this.httpInstance
      .post<LoginResponse>(
        route,
        {
          email: mail,
          password,
        },
        { observe: 'response' }
      )
      .pipe(
        map((response) => {
          if (response.status == 200 && response.body?.authToken) {
            this.isLoggedIn.next(true);
            this.setToken(response.body.authToken);
            return response.body;
          }
          return undefined;
        })
      );
  }

  logout() {
    return this.httpInstance.post(`${this.baseURL}/user/logout`, {}).pipe(
      tap(() => {
        this.isLoggedIn.next(false);
        this.clearStorage();
      })
    );
  }

  register(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    organizationId: string
  ) {
    return this.httpInstance
      .post(
        `${this.baseURL}/user/register`,
        {
          firstName,
          lastName,
          email,
          password,
          organizationId,
        },
        { observe: 'response' }
      )
      .pipe(
        switchMap((response) =>
          response.status === 201 ? of(true) : throwError(response)
        )
      );
  }

  registerWithNewOrganization(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    organizationName: string,
    organizationDescription: string,
    organizationUrl: string
  ) {
    const newOrganization = {
      name: organizationName,
      description: organizationDescription,
      logoUrl: organizationUrl,
    };
    return this.httpInstance
      .post(
        `${this.baseURL}/user/register`,
        {
          firstName,
          lastName,
          email,
          password,
          newOrganization,
        },
        { observe: 'response' }
      )
      .pipe(
        switchMap((response) =>
          response.status === 201 ? of(true) : throwError(response)
        )
      );
  }

  requestReset(email: string) {
    return this.httpInstance.post(
      `${this.baseURL}/user/password/requestreset`,
      {
        email: email,
      }
    );
  }

  reset(password: string, resettoken: string) {
    return this.httpInstance.post(`${this.baseURL}/user/password/reset`, {
      password: password,
      resetToken: resettoken,
    });
  }
}
