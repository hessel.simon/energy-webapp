/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Task } from '@energy-platform/shared/interfaces';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { APIService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class CalculationService extends APIService {
  constructor(private http: HttpClient) {
    super();
  }

  startCalculation(projectID: string, taskType: string): Observable<string> {
    const route = this.baseURL + '/calculation/task';
    this.logger.info(' start calculation :');
    return this.http
      .post<Task>(route, {
        projectId: projectID,
        taskType: taskType,
      })
      .pipe(map((task) => task.id));
  }

  stopCalculation(taskID: string) {
    const route = this.baseURL + `/calculation/task/${taskID}/abort`;
    return this.http.post(route, {
      taskId: taskID,
    });
  }

  getCalculationResult(taskID: string) {
    const route = this.baseURL + `/calculation/task/${taskID}`;
    return this.http.get(route).pipe();
  }

  getCalculationStatus(taskID: string) {
    return this.http
      .get<Task>(`${this.baseURL}/calculation/task/${taskID}/status`)
      .pipe(map((task) => task.status));
  }

  getCalculationsOfProject(projectID: string): Observable<Task[]> {
    return this.http.post<Task[]>(
      `${this.baseURL}/calculation/project/${projectID}`,
      {}
    );
  }

  getAllCalculations(): Observable<Task[]> {
    return this.http.get<Task[]>(`${this.baseURL}/calculation/task`);
  }
}
