import { DatePipe } from '@angular/common';
import { Injectable } from '@angular/core';

@Injectable()
export class DateFormatterService {
  constructor(public datePipe: DatePipe) {}

  public getDateInRightFormat(date: Date) {
    return this.datePipe.transform(date, 'dd/MMM/yyyy');
  }
}
