import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { APIService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class MailService extends APIService {
  constructor(private httpInstance: HttpClient) {
    super();
    this.serviceName = 'Mail Service';
  }

  AddUserToProject(
    email: string,
    text: string,
    userId: string,
    projectId: string,
    rights: string
  ) {
    const route = this.baseURL + '/mail/invitation';
    return this.httpInstance.post(
      route,
      {
        email: email,
        text: text,
        userId: userId,
        projectId: projectId,
        rights: rights,
      },
      { observe: 'response' }
    );
  }
}
