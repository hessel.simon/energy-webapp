/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { APIService } from './api.service';
import { News } from '@energy-platform/shared/interfaces';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NewsService extends APIService {
  private newsListener = new Subject<News[]>();

  private news: News[] = [
    {
      id: '1',
      headline: '500 Projects!',
      text: 'We have reached 500 projects today! To celebrate this we are going to throw a party.',
      date: new Date('2019-07-12'),
      draft: false,
    },
    {
      id: '2',
      headline: '100 Projects',
      text: 'We have reached 100 projects today! This is the first milestone of Windflower',
      date: new Date('2019-07-12'),
      draft: false,
    },
    {
      id: '3',
      headline: 'Windflower is released',
      text: 'Today we are happy to annouce that Windflower is relased. You can visit the page under here',
      date: new Date('2019-07-11'),
      draft: false,
    },
  ];

  constructor(private http: HttpClient) {
    super();
  }

  getNewsListener(): Subject<News[]> {
    return this.newsListener;
  }

  fetchNews() {
    const route = this.baseURL + '/news';
    this.http
      .get<Array<any>>(route, { observe: 'response' })
      .pipe(
        map((response) => {
          const body = response.body;
          // eslint-disable-next-line prefer-const
          let news: Array<News> = [];
          if (body)
            for (let i = 0; i < body.length; i++) {
              news.push(<News>{
                id: body[i]._id,
                headline: body[i].headline,
                text: body[i].text,
                draft: body[i].draft,
                date: new Date(body[i].createdAt),
              });
            }
          return news;
        })
      )
      .subscribe((news: Array<News>) => {
        this.news = news;
        this.newsListener.next(this.news);
      });
  }

  addPost(headline: string, text: string, draft: boolean) {
    const route = this.baseURL + '/news';
    this.http
      .post(
        route,
        {
          headline: headline,
          text: text,
          draft: draft,
        },
        { observe: 'response' }
      )
      .subscribe(
        (_) => {
          this.fetchNews();
        },
        (error) => {
          this.logger.error('Error when adding new post: ', error);
        }
      );
  }

  updatePost(postId: string, headline: string, text: string, draft: boolean) {
    const route = this.baseURL + '/news/' + postId;
    this.http
      .patch(
        route,
        {
          headline: headline,
          text: text,
          draft: draft,
        },
        { observe: 'response' }
      )
      .subscribe(
        (_) => {
          this.fetchNews();
        },
        (error) => {
          this.logger.error('Error when updating post' + postId + ': ', error);
        }
      );
  }

  deletePost(postId: string) {
    const route = this.baseURL + '/news/' + postId;
    this.http.delete(route, { observe: 'response' }).subscribe(
      (_) => {
        this.fetchNews();
      },
      (error) => {
        this.logger.error('Error when deleting post' + postId + ': ', error);
      }
    );
  }
}
