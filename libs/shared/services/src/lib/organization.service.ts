import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError } from 'rxjs';
import { catchError, map, switchMapTo, tap } from 'rxjs/operators';
import {
  Organization,
  OrganizationDTO,
} from '@energy-platform/shared/interfaces';
import { APIService } from './api.service';
import { UserService } from './user.service';
@Injectable({
  providedIn: 'root',
})
export class OrganizationService extends APIService {
  private organizationListenerAll = new BehaviorSubject<Organization[]>([]);
  private organizationListenerPublic = new BehaviorSubject<Organization[]>([]);

  private initialFetch = false;
  constructor(private http: HttpClient, private userService: UserService) {
    super();
  }

  private parseOrganizationDTO({
    logoURL,
    description,
    name,
    ...organization
  }: OrganizationDTO) {
    return {
      logo: logoURL,
      organizationDescription: description,
      organizationName: name,
      ...organization,
    };
  }

  getOrganizationListenerAll() {
    if (!this.initialFetch) {
      this.initialFetch = true;
      return this.fetchOrganizationsAll().pipe(
        switchMapTo(this.organizationListenerAll)
      );
    }
    return this.organizationListenerAll;
  }

  getOrganizationListenerPublic() {
    if (!this.initialFetch) {
      this.initialFetch = true;
      return this.fetchOrganizationAllPublic().pipe(
        switchMapTo(this.organizationListenerPublic)
      );
    }
    return this.organizationListenerPublic;
  }

  fetchOrganizationAllPublic() {
    return this.http
      .get<OrganizationDTO[]>(`${this.baseURL}/organization/public/all`)
      .pipe(
        map((response) => response.map(this.parseOrganizationDTO)),
        tap((organizations) =>
          this.organizationListenerPublic.next(organizations)
        )
      );
  }

  fetchOrganizationsAll() {
    return this.http
      .get<OrganizationDTO[]>(`${this.baseURL}/organization/all`)
      .pipe(
        map((response) => response.map(this.parseOrganizationDTO)),
        tap((organizations) => this.organizationListenerAll.next(organizations))
      );
  }

  addOrganization(name: string, text: string, logoURL: string) {
    return this.http
      .post(`${this.baseURL}/organization`, {
        name: name,
        description: text,
        logoURL: logoURL,
      })
      .pipe(
        tap(() => this.fetchOrganizationsAll()),
        catchError((error) => {
          this.logger.error('Error when adding new organization: ', error);
          return throwError(error);
        })
      );
  }

  updateOrganization(
    organization: Organization,
    name: string,
    description: string,
    logoURL: string
  ) {
    return this.http
      .patch(`${this.baseURL}/organization/${organization.id}`, {
        name: name,
        description: description,
        logoUrl: logoURL,
      })
      .pipe(
        tap(() => this.fetchOrganizationsAll()),
        catchError((error) => {
          this.logger.error(
            'Error when updating organization' + organization.id + ': ',
            error
          );
          return throwError(error);
        })
      );
  }

  deleteOrganization(organizationId: string) {
    this.http
      .delete(`${this.baseURL}/organization/${organizationId}`, {
        observe: 'response',
      })
      .subscribe(
        (_) => {
          this.fetchOrganizationsAll();
        },
        (error) => {
          this.logger.error(
            ' Error when deleting organization ',
            organizationId,
            error
          );
        }
      );
  }

  mergeOrganization(sourceID: string, destinationID: string) {
    this.http
      .post(
        `${this.baseURL}/organization/merge`,
        {
          srcOrgaId: sourceID,
          destOrgaId: destinationID,
        },
        { observe: 'response' }
      )
      .subscribe(
        (_) => {
          this.fetchOrganizationsAll();
        },
        (error) => {
          this.logger.error(
            error +
              ' Error when merging organization ' +
              sourceID +
              ' and ' +
              destinationID
          );
        }
      );
  }
}
