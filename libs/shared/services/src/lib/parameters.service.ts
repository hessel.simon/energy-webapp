import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APIService } from './api.service';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ParameterService extends APIService {
  constructor(private http: HttpClient) {
    super();
  }

  getSchemas(): Observable<any> {
    const route = this.baseURL + '/project/parameterschemas';
    return this.http.get<any>(route, { observe: 'response' }).pipe(
      map((response) => {
        return response.body.wind;
      })
    );
  }

  getParameter(projectId: string, filter: string) {
    this.logger.info(' :  was called ');
    const route = `${this.baseURL}/project/${projectId}/parameters`;
    const params = new HttpParams().set('filter', `property:${filter}`);

    return this.http
      .get<any>(route, { observe: 'response', params })
      .pipe(map((response) => response.body));
  }

  getParameterOfRelease(
    projectId: string,
    releaseId: string,
    filter: string = '',
    visibility = ''
  ) {
    const isVisibilityEmpty = visibility === '';

    const route = `${this.baseURL}/public/projects/${projectId}/releases/${releaseId}/parameters`;
    const params = new HttpParams().set('filter', `property:${filter}`);

    return this.http
      .get<any>(route, { observe: 'response', params })
      .pipe(map((response) => response.body));
  }
}
