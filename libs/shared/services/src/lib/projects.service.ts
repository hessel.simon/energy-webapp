import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  ContributorDTO,
  Project,
  ProjectDTO,
  ProjectParameter,
  ProjectStatus,
  PublishRealeaseDTO,
  Release,
  Right,
} from '@energy-platform/shared/interfaces';
import 'rxjs';
import { BehaviorSubject, forkJoin, of, throwError } from 'rxjs';
//import { TemplateService } from "./template.service";
import {
  catchError,
  concatMap,
  map,
  switchMap,
  switchMapTo,
  tap,
} from 'rxjs/operators';
import { APIService } from './api.service';
import { ReleasesService } from './releases.service';

@Injectable({
  providedIn: 'root',
})
export class ProjectsService extends APIService {
  // Components can subscribe to the current list of projects and are notified once the list is updated
  private privateProjectsListener = new BehaviorSubject<Project[]>([]);
  private publicProjectsListener = new BehaviorSubject<Project[]>([]);
  private deletedProjectsListener = new BehaviorSubject<Project[]>([]);
  private allProjectsListener = new BehaviorSubject<Project[]>([]);
  private openProjectsListener = new BehaviorSubject<Project[]>([]);

  private initialFetch = false;

  constructor(
    private http: HttpClient,
    private releaseService: ReleasesService
  ) {
    // Invoke constructor of ApiService class to get access to api base url
    super();
  }

  /************************************************
   * Listeners that components can subscribe to
   ************************************************/
  getPrivateProjectsListener() {
    return this.getProjectsListener(this.privateProjectsListener);
  }

  getPublicProjectsListener() {
    return this.getProjectsListener(this.publicProjectsListener);
  }

  getDeletedProjectsListener() {
    return this.getProjectsListener(this.deletedProjectsListener);
  }

  getAllProjectsListener() {
    return this.getProjectsListener(this.allProjectsListener);
  }

  getOpenProjectsListener() {
    return this.getProjectsListener(this.openProjectsListener);
  }
  private getProjectsListener(listener: BehaviorSubject<Project[]>) {
    if (!this.initialFetch) {
      this.initialFetch = true;
      return this.fetchAllOpenProjects().pipe(switchMapTo(listener));
    }
    return listener;
  }

  private parseProjectDTO({
    createdAt,
    location,
    status,
    published_versions,
    ...projectDto
  }: ProjectDTO): Project {
    return {
      contributors: [],
      ...projectDto,
      creationDate: new Date(createdAt),
      location:
        location.length > 1
          ? { lat: location[1], lng: location[0] }
          : { lat: 50.775119, lng: 7.04413271 },
      isPublic: status === 'public' ? true : false,
      projectStatus: status,
      releases: this.sortReleasesInDescendingOrder(published_versions),
    };
  }

  /************************************************
   * Http fetch methods
   ************************************************/

  /**
   * Fetch all projects that a user can access.
   */
  fetchAllAccessibleProjects(showMy: boolean = false, userId: string = '') {
    return this.http.get<ProjectDTO[]>(`${this.baseURL}/project`).pipe(
      map((publicProjects) => {
        const projects: Project[] = publicProjects.map((project) =>
          this.parseProjectDTO(project)
        );
        return showMy
          ? projects.filter((project) =>
              this.isProjectOwnByUserWithId(project, userId)
            )
          : projects;
      }),
      tap((projects) => {
        const { newPrivateProjects, newPublicProjects } = projects.reduce(
          ({ newPrivateProjects, newPublicProjects }, project) => {
            if (
              project.releases.some((release) => release.status === 'public')
            ) {
              newPublicProjects.push(project);
              newPrivateProjects.push({
                ...project,
                isPublic: false,
                projectStatus: ProjectStatus.HIDDEN,
                releases: project.releases.map((release) => ({
                  id: release.id,
                  version: release.version,
                  status: 'hidden',
                  timestamp: release.timestamp,
                })),
              });
            } else {
              newPrivateProjects.push(project);
            }
            return { newPrivateProjects, newPublicProjects };
          },
          // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
          { newPrivateProjects: [], newPublicProjects: [] } as {
            newPrivateProjects: Project[];
            newPublicProjects: Project[];
          }
        );

        const allProjects: Project[] = [];
        allProjects.concat(newPrivateProjects, newPublicProjects);
        this.publicProjectsListener.next(newPublicProjects);
        this.privateProjectsListener.next(newPrivateProjects);
        this.allProjectsListener.next(allProjects);
      })
    );
  }

  private isProjectOwnByUserWithId(project: Project, userId: string) {
    this.logger.info(`userID = ${userId}`, project.creator);
    return project.creator.id === userId;
  }

  fetchAllOpenProjects() {
    return this.http.get<ProjectDTO[]>(`${this.baseURL}/public/projects`).pipe(
      map((publicProjects) => {
        const projects: Project[] = publicProjects.map((project) =>
          this.parseProjectDTO(project)
        );
        this.logger.info('projects before returned : ', projects);
        return projects;
      }),
      tap((projects) => {
        // store project release informations
        this.updateProjectsReleaseInfo(projects);
        this.openProjectsListener.next(projects);
      })
    );
  }

  /**
   * Fetch all deleted projects the current user can restore
   */
  fetchDeletedProjects() {
    return this.http.get<ProjectDTO[]>(`${this.baseURL}/project/deleted`).pipe(
      map((deletedProjects) => {
        this.logger.info(' fetched deleted projects ');
        return deletedProjects.map((project) => this.parseProjectDTO(project));
      }),
      tap((projects) => this.deletedProjectsListener.next(projects))
    );
  }

  /**
   * Return single project by id
   */
  getProject(id: string) {
    return this.allProjectsListener.pipe(
      map((projects) => projects.find((project) => project.id === id)),
      tap((project) => project && this.updateProjectsReleaseInfo(project)),
      switchMap((project) =>
        project
          ? of(project)
          : this.http.get<ProjectDTO>(`${this.baseURL}/project/${id}`).pipe(
              map((project) => {
                const sortedReleases = this.sortReleasesInDescendingOrder(
                  project.published_versions
                );
                const proj: Project = {
                  id: project.id,
                  name: project.name,
                  repository_name: project.repository_name,
                  description: project.description,
                  type: project.type,
                  creationDate: new Date(project.createdAt),
                  published_web_app: project.published_web_app,
                  creator: project.creator,
                  location:
                    project.location.length > 1
                      ? { lat: project.location[1], lng: project.location[0] }
                      : { lat: 50.775119, lng: 7.04413271 },
                  isPublic: project.status === 'public' ? true : false,
                  projectStatus: project.status,
                  contributors: project.contributors || [],
                  releases: sortedReleases,
                  tasks: project.tasks,
                  deprecatedTasks: project.deprecatedTasks,
                };

                return proj;
              }),
              tap((proj) => this.updateProjectsReleaseInfo(proj))
            )
      )
    );
  }

  /**
   * Creates new project.
   */
  addNewProject(
    name: string,
    type: string,
    description: string,
    template: string
  ) {
    return this.http
      .post(`${this.baseURL}/project`, {
        name: name,
        type: type,
        description: description,
        template: template,
      })
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        catchError((error) => {
          this.logger.error('Error when adding new project: ', error);
          return throwError(error);
        })
      );
  }
  /**
   * Duplicates project.
   */
  duplicateProject(
    name: string,
    type: string,
    description: string,
    template: {
      description: string;
      projectId: string;
      releaseId: string;
    }
  ) {
    return this.http
      .post(`${this.baseURL}/project`, {
        name: name,
        type: type,
        description: description,
        template: template,
      })
      .pipe(
        catchError((error) => {
          this.logger.error('Error when adding new project: ', error);
          return throwError(error);
        })
      );
  }

  editProject(project: Project) {
    return this.http
      .patch(`${this.baseURL}/project/${project.id}`, {
        name: project.name,
        description: project.description,
        status: project.projectStatus,
      })
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        catchError((error) => {
          this.logger.error(' Error when editing project: ', project.id, error);
          return throwError(error);
        })
      );
  }

  deleteProject(projectId: string) {
    return this.http.delete(`${this.baseURL}/project/${projectId}`).pipe(
      concatMap(() => this.fetchAllAccessibleProjects()),
      concatMap(() => this.fetchDeletedProjects()),
      catchError((error) => {
        this.logger.error('Error when deleting project: ', projectId, error);
        return throwError(error);
      })
    );
  }

  recycleProject(projectId: string) {
    return this.http
      .post(`${this.baseURL}/project/${projectId}/recycle`, null)
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        concatMap(() => this.fetchDeletedProjects()),
        catchError((error) => {
          this.logger.error('Error when recycle project: ', projectId, error);
          return throwError(error);
        })
      );
  }

  /**
   * This route publishes a project that is in status under review at least to the git. If published_web_app is set to true, then this project is also set to the list of published projects in webapp.
   */
  publishProjectRelease(release: PublishRealeaseDTO) {
    const { id: projectId } = release.project;
    const { id: releaseId } = release;

    return this.http
      .post(
        `${this.baseURL}/project/${projectId}/releases/${releaseId}/review/confirm`,
        {}
      )
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        catchError((error) => {
          this.logger.error('Error when publishing project to git: ', error);
          return throwError(error);
        })
      );
  }

  /**
   * This route discards a project from the list of projects with status under review. This project will neither be published to git, nor to the webapp. It's status will remain hidden.
   */
  discardProjectPublishRequest(release: PublishRealeaseDTO) {
    const { id: projectId } = release.project;
    const { id: releaseId } = release;

    return this.http
      .post(
        `${this.baseURL}/project/${projectId}/releases/${releaseId}/review/discard`,
        {}
      )
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        catchError((error) => {
          this.logger.error('Error when publishing project to git: ', error);
          return throwError(error);
        })
      );
  }

  /**
   * This route pushes a project to the list of projects that are in status under_review. An admin can decide whether to publish this project or discard the request.
   * @param project
   */
  pushProjectToPublishReview(project: Project) {
    const { releases } = project;
    const version = this.getNextVersionFromRelease(releases);

    this.logger.info('Releases: ', releases);

    return this.http
      .post(`${this.baseURL}/project/${project.id}/publish`, { version })
      .pipe(
        concatMap(() => this.fetchAllAccessibleProjects()),
        catchError((error) => {
          this.logger.error(
            'Error when publishing project to review process: ',
            project.id,
            error
          );
          return throwError(error);
        })
      );
  }

  private getNextVersionFromRelease(releases: Release[]) {
    const sortedArray = releases
      .map((release) => parseInt(release.version.replace('v', ''), 10))
      .sort((a, b) => b - a);

    if (sortedArray.length === 0) {
      return `v${1}`;
    }
    this.logger.info(' : Sorted Releases ', sortedArray);
    const currentLatest = sortedArray[0];

    return `v${currentLatest + 1}`;
  }

  private sortReleasesInDescendingOrder(releases: Release[]): Release[] {
    return releases.sort((a, b) => {
      const versionA = parseInt(a.version.replace('v', ''));
      const versionB = parseInt(b.version.replace('v', ''));
      return versionB - versionA;
    });
  }

  getProjectParameters(projectId: string) {
    return this.http.get<ProjectParameter>(
      `${this.baseURL}/project/${projectId}/parameters`
    );
  }

  // used on public visualization page
  getReleaseParameters(projectId: string, releaseId: string) {
    return this.http.get<ProjectParameter>(
      `${this.baseURL}/public/projects/${projectId}/releases/${releaseId}/parameters`
    );
  }

  getPublicReleasedProjects() {
    return this.http.get<ProjectDTO[]>(`${this.baseURL}/public/projects`).pipe(
      map((publicProjects) =>
        publicProjects.map((project) =>
          this.parseProjectDTO({
            ...project,
            tasks: [],
          })
        )
      )
    );
  }

  getPublicReleasedProjectsById(projectId: string) {
    return this.http.get<ProjectDTO[]>(`${this.baseURL}/public/projects`).pipe(
      map((publicProjects) => {
        this.logger.info(': ', publicProjects);
        const filteredProjects = publicProjects.filter(
          (project) => project.id === projectId
        );

        if (filteredProjects.length > 0)
          return this.parseProjectDTO(filteredProjects[0]);
        else throw 'Project not found.';
      })
    );
  }

  setProjectParameters(projectId: string, parameters: Record<string, unknown>) {
    return this.http
      .patch(`${this.baseURL}/project/${projectId}/parameters`, parameters)
      .pipe(
        concatMap(() => this.getProject(projectId)),
        catchError((error) => {
          this.logger.error(
            ' Error when updating project parameters: ',
            projectId,
            error
          );
          return throwError(error);
        })
      );
  }

  /*****************
   * Helper methods
   *****************/
  private updateProjectsReleaseInfo(arg: Project[] | Project) {
    (Array.isArray(arg) ? arg : [arg]).forEach((project) => {
      if (!this.releaseService.getReleaseForProjectWithId(project.id)) {
        return {
          projectId: project.id,
          activeReleaseId: undefined,
        };
      }
      return undefined;
    });
  }

  private fetchContributorIdsForSingleProject(route: string) {
    return this.http.get<ContributorDTO[]>(route).pipe(
      map((contributers) =>
        contributers.map((contributer) => ({
          ...contributer,
          id: contributer._id,
          user: {
            ...contributer.user,
            id: contributer.user._id,
          },
        }))
      )
    );
  }

  /**
   * For each project return an array of contributors with rights.
   * @param projectIds
   */
  fetchProjectContributorIds(projectIds: string[]) {
    return forkJoin(
      projectIds.map((projectId) =>
        this.fetchContributorIdsForSingleProject(
          `${this.baseURL}/project/${projectId}/contributors`
        )
      )
    );
  }

  /**
   * For each project return an array of contributors with rights.
   */
  fetchProjectContributorId(projectId: string) {
    return this.fetchContributorIdsForSingleProject(
      `${this.baseURL}/project/${projectId}/contributors`
    );
  }

  addContributor(
    projectId: string,
    contributorId: string,
    rights: Right = Right.READ
  ) {
    return this.http.put(
      `${
        this.baseURL
      }/project/${projectId}/contributors/${contributorId}?rights=${rights.toString()}`,
      {}
    );
  }

  deleteContributors(projectId: string, contributorId: string) {
    return this.http.delete(
      `${this.baseURL}/project/${projectId}/contributors/${contributorId}`
    );
  }

  downloadParamsAndResults(projectId: string) {
    /*if(!releaseId){
      const route = `/project/${projectId}/download`;
      return this.baseURL + route;
    }else{
      const route = `/public/projects/${projectId}/releases/${releaseId}/download`;
      return this.baseURL + route;
    }*/

    window.open(`${this.baseURL}/project/download/${projectId}`);
  }
  downloadParamsAndResultsAsZip(projectId: string, releaseId?: string) {
    return !releaseId
      ? `${this.baseURL}/project/${projectId}/download`
      : `${this.baseURL}/public/projects/${projectId}/releases/${releaseId}/download`;
  }
}
