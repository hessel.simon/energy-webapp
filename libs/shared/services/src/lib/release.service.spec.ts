import { TestBed } from '@angular/core/testing';

import { ReleasesService } from './releases.service';

describe('ReleasesService', () => {
  let service: ReleasesService;
  const LocalStorageKey = 'WindflowerProjectReleases';

  beforeEach(() => {
    localStorage.setItem(LocalStorageKey, JSON.stringify({ '1': '5' }));
    TestBed.configureTestingModule({});
    service = TestBed.inject(ReleasesService);
    expect(service).toBeTruthy();
  });

  it('ID 1 is present', () => {
    expect(service.getReleaseForProjectWithId('1')).toBe('5');
  });
  it('ID 2 is not present', () => {
    expect(service.getReleaseForProjectWithId('2')).toBe(undefined);
  });
  it('ID 2 should be 4 after updateActiveReleaseForProject', () => {
    expect(service.getReleaseForProjectWithId('1')).toBe('5');
    expect(service.getReleaseForProjectWithId('2')).toBe(undefined);

    service.updateActiveReleaseForProject({
      projectId: '2',
      activeReleaseId: '4',
    });

    expect(service.getReleaseForProjectWithId('2')).toBe('4');

    expect(localStorage.getItem(LocalStorageKey)).toBe(
      JSON.stringify({ '1': '5', '2': '4' })
    );
  });
  it('ID 1 should be undefined after setLatestReleaseForProject', () => {
    expect(service.getReleaseForProjectWithId('1')).toBe('5');

    service.setLatestReleaseForProject({
      projectId: '2',
      activeReleaseId: '4',
    });

    expect(service.getReleaseForProjectWithId('2')).toBe(undefined);

    expect(localStorage.getItem(LocalStorageKey)).toBe(
      JSON.stringify({ '1': '5' })
    );
  });
});
