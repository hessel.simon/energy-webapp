import { Injectable } from '@angular/core';
import { StoreItem } from '@energy-platform/shared/interfaces';

@Injectable({
  providedIn: 'root',
})
export class ReleasesService {
  private LocalStorageKey = 'WindflowerProjectReleases';
  private projects: Record<string, string | undefined>;

  constructor() {
    this.projects = this.getDataFromLocalStorage();
  }

  public updateActiveReleaseForProject(projectIds: StoreItem[] | StoreItem) {
    this.releaseforProject(projectIds, true);
  }

  public setLatestReleaseForProject(projectIds: StoreItem[] | StoreItem) {
    this.releaseforProject(projectIds, false);
  }
  private releaseforProject(
    projectIds: StoreItem[] | StoreItem,
    UpdatereleaseId: boolean
  ) {
    for (const item of this.createQueryArray(projectIds)) {
      if (item?.projectId) {
        const ReleaseId = UpdatereleaseId ? item.activeReleaseId : undefined;
        this.projects[item.projectId] = ReleaseId;
      }
    }

    // update data in localstorage
    this.updateLocalStorage(this.projects);
  }

  public getReleaseForProjectWithId(projectId: string): string | undefined {
    return this.projects[projectId];
  }

  private getDataFromLocalStorage(): Record<string, string | undefined> {
    const jsonData = localStorage.getItem(this.LocalStorageKey);
    if (!jsonData) {
      return {};
    }
    return JSON.parse(jsonData);
  }
  private updateLocalStorage(data: Record<string, string | undefined>) {
    localStorage.setItem(this.LocalStorageKey, JSON.stringify(data));
  }

  private createQueryArray(projectIds: StoreItem[] | StoreItem) {
    return Array.isArray(projectIds) ? projectIds : [projectIds];
  }
}
