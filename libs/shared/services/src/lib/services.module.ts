import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from './authentication.service';
import { UserService } from './user.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { SetTokenInterceptor } from './set.token.interceptor';
import { DateFormatterService } from './date-formatter.service';
import { DatePipe } from '@angular/common';

@NgModule({
  imports: [CommonModule],
  providers: [
    AuthenticationService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: SetTokenInterceptor, multi: true },
    DateFormatterService,
    DatePipe,
  ],
})
export class ServicesModule {}
