/* eslint-disable @typescript-eslint/consistent-type-assertions */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Organization,
  User,
  UserDTO,
} from '@energy-platform/shared/interfaces';
import { BehaviorSubject, of } from 'rxjs';
import {
  concatMap,
  map,
  switchMap,
  switchMapTo,
  tap,
  zip,
} from 'rxjs/operators';
import { APIService } from './api.service';

//export const TOKEN_NAME = 'jwt_token';

@Injectable({ providedIn: 'root' })
export class UserService extends APIService {
  success = true;
  private usersListener = new BehaviorSubject<User[]>([]);
  private initialFetch = false;

  constructor(private httpInstance: HttpClient) {
    super();
  }

  getUserListener(refetch = false) {
    if (!this.initialFetch || refetch) {
      this.initialFetch = true;
      return this.getOwnUserProfile().pipe(
        zip(this.fetchAllUsers()),
        tap(([user, users]) => {
          if (user !== null && !users.some((u) => u.id === user.id)) {
            this.usersListener.next([...users, user]);
          }
        }),
        switchMapTo(this.usersListener)
      );
    }
    return this.usersListener;
  }

  getOwnUserProfile() {
    return this.httpInstance.get<User>(`${this.baseURL}/user/me`);
  }

  editOwnUserProfile(body: unknown) {
    return this.httpInstance.patch<User>(`${this.baseURL}/user/me`, body, {
      observe: 'response',
    });
  }

  fetchAllUsers() {
    return this.httpInstance.get<UserDTO[]>(`${this.baseURL}/user`, {}).pipe(
      map((body) =>
        body.map(({ deactivated, organization, ...userDto }) => ({
          ...userDto,
          active: !deactivated,
          organization: {
            id: organization?.id,
            organizationDescription: organization?.description,
            organizationName: organization?.name,
            users: organization?.users,
            logo: organization?.logoURL,
          },
          numberCurrentlyRunningJobs: 0, // TODO userDto.numberCurrentlyRunningJobs,
          numberFinishedJobs: 0, // TODO userDto.numberFinishedJobs,
        }))
      ),
      tap((users) => this.usersListener.next(users))
    );
  }

  deleteUser(userID: string) {
    return this.httpInstance
      .delete<User>(`${this.baseURL}/user/${userID}`, { observe: 'response' })
      .pipe(concatMap(() => this.fetchAllUsers()));
  }

  getUsersInOrganization(organizationId: Organization['id']) {
    return this.fetchAllUsers().pipe(
      map((users) =>
        users.filter((user) => user.organization.id === organizationId)
      )
    );
  }

  editUser(user: User) {
    return this.httpInstance
      .patch(`${this.baseURL}/user/${user.id}`, {
        email: user.email,
        firstName: user.firstName,
        lastName: user.lastName,
        active: user.active,
      })
      .pipe(concatMap(() => this.fetchAllUsers()));
  }

  update(
    firstName: string,
    lastName: string,
    email: string,
    organizationId: string
  ) {
    return this.httpInstance.patch(`${this.baseURL}/user/me`, {
      firstName,
      lastName,
      email,
      organizationId,
    });
  }

  updateAccountWithNewOrganization(
    firstName: string,
    lastName: string,
    email: string,
    organizationName: string,
    organizationDescription: string,
    organizationUrl: string
  ) {
    return this.httpInstance.patch(`${this.baseURL}/user/me`, {
      firstName,
      lastName,
      email,
      newOrganization: {
        name: organizationName,
        description: organizationDescription,
        logoUrl: organizationUrl,
      },
    });
  }

  editPassword(oldPassword: string, newPassword: string) {
    return this.httpInstance
      .put(`${this.baseURL}/user/me/password`, { oldPassword, newPassword })
      .pipe(map(() => true));
  }

  /**
   * Return single user by id
   */
  getUser(id: string) {
    return this.usersListener.pipe(
      map((users) => users.find((user) => user.id === id)),
      switchMap((user) => (user ? of(user) : this.fetchAllUsers())),
      map((users) =>
        Array.isArray(users) ? users.find((user) => user.id === id) : users
      )
    );
  }
}
