import {
  Component,
  ElementRef,
  Input,
  OnInit,
  OnChanges,
  ViewChild,
  SimpleChanges,
} from '@angular/core';
import { WEBGL } from 'three/examples/jsm/WebGL';
import { ThreeCubeService } from './three-cube.service';

@Component({
  selector: 'energy-platform-three-cube',
  templateUrl: './three-cube.component.html',
})
export class ThreeCubeComponent implements OnInit, OnChanges {
  @Input() rotationX: number | undefined;
  @Input() rotationY: number | undefined;
  @Input() rotationZ: number | undefined;
  @Input() positionX: number | undefined;
  @Input() positionY: number | undefined;
  @Input() positionZ: number | undefined;
  @Input() scaleX: number | undefined;
  @Input() scaleY: number | undefined;
  @Input() scaleZ: number | undefined;
  @Input() alpha: number | undefined;
  @Input() wireframe: boolean | undefined;

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement> | undefined;

  constructor(private threeCubeServ: ThreeCubeService) {}

  ngOnInit() {
    if (WEBGL.isWebGLAvailable() && this.rendererCanvas) {
      this.threeCubeServ.createScene(this.rendererCanvas);
      this.threeCubeServ.animate();
    } else {
      const warning = WEBGL.getWebGLErrorMessage();
      warning.style.width = '80%';
      const rendererWrapper = document.getElementById('rendererWrapper');
      if (rendererWrapper) {
        rendererWrapper.appendChild(warning);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['rotationX'])
      this.threeCubeServ.rotationX(changes['rotationX'].currentValue);

    if (changes['rotationY'])
      this.threeCubeServ.rotationY(changes['rotationY'].currentValue);

    if (changes['rotationZ'])
      this.threeCubeServ.rotationZ(changes['rotationZ'].currentValue);

    if (changes['positionX'])
      this.threeCubeServ.positionX(changes['positionX'].currentValue);

    if (changes['positionY'])
      this.threeCubeServ.positionY(changes['positionY'].currentValue);

    if (changes['positionZ'])
      this.threeCubeServ.positionZ(changes['positionZ'].currentValue);

    if (changes['scaleX'])
      this.threeCubeServ.scaleX(changes['scaleX'].currentValue);

    if (changes['scaleY'])
      this.threeCubeServ.scaleY(changes['scaleY'].currentValue);

    if (changes['scaleZ'])
      this.threeCubeServ.scaleZ(changes['scaleZ'].currentValue);

    if (changes['alpha'])
      this.threeCubeServ.alpha(changes['alpha'].currentValue);

    if (changes['wireframe'])
      this.threeCubeServ.wireframe(changes['wireframe'].currentValue);
  }

  onResize() {
    this.threeCubeServ.resize();
  }
}
