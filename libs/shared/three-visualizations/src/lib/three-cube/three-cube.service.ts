import { Injectable, ElementRef, OnDestroy, NgZone } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

@Injectable({
  providedIn: 'root',
})
export class ThreeCubeService implements OnDestroy {
  private canvas: HTMLCanvasElement | undefined;

  private renderer: THREE.WebGLRenderer | undefined;
  private scene: THREE.Scene | undefined;
  private camera: THREE.PerspectiveCamera | undefined;
  private cameraControls: OrbitControls | undefined;
  private light: THREE.AmbientLight | undefined;
  private cube: THREE.Mesh | undefined;
  private cubeMaterial: THREE.MeshBasicMaterial | undefined;

  private frameId: number | undefined;

  constructor(private ngZone: NgZone) {}

  ngOnDestroy() {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
  }

  createScene(canvas: ElementRef<HTMLCanvasElement>): void {
    this.canvas = canvas.nativeElement;

    // Create renderer
    this.renderer = new THREE.WebGLRenderer({
      canvas: this.canvas,
      antialias: true,
    });
    this.renderer.setSize(this.canvas.width, this.canvas.height);

    // Create scene
    this.scene = new THREE.Scene();

    // Add camera
    this.camera = new THREE.PerspectiveCamera(
      75.0,
      this.canvas.width / this.canvas.height,
      0.1,
      1000.0
    );
    this.camera.position.z = 5;
    this.scene.add(this.camera);

    // Add camera controls
    this.cameraControls = new OrbitControls(this.camera, this.canvas);
    this.cameraControls.enableKeys = false;

    // Add light
    this.light = new THREE.AmbientLight(0x404040);
    this.light.position.z = 10;
    this.scene.add(this.light);

    // Add cube
    this.cubeMaterial = new THREE.MeshBasicMaterial({ color: 0x0000ff });
    this.cube = new THREE.Mesh(
      new THREE.BoxGeometry(1, 1, 1),
      this.cubeMaterial
    );
    this.scene.add(this.cube);

    // Add skybox
    this.scene.background = new THREE.CubeTextureLoader().load([
      './assets/textures/skybox_xpos.png',
      './assets/textures/skybox_xneg.png',
      './assets/textures/skybox_ypos.png',
      './assets/textures/skybox_yneg.png',
      './assets/textures/skybox_zpos.png',
      './assets/textures/skybox_zneg.png',
    ]);
  }

  animate() {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState !== 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }

  render() {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    if (this.renderer && this.scene && this.camera) {
      this.renderer.render(this.scene, this.camera);
    }
  }

  resize() {
    this.fitCanvasToContainer();

    if (this.canvas && this.camera && this.renderer) {
      const width = this.canvas.width;
      const height = this.canvas.height;

      this.camera.aspect = width / height;
      this.camera.updateProjectionMatrix();

      this.renderer.setSize(width, height);
    }
  }

  fitCanvasToContainer() {
    if (this.canvas) {
      this.canvas.style.width = '100%';
      this.canvas.style.height = '100%';

      this.canvas.width = this.canvas.offsetWidth;
      this.canvas.height = this.canvas.offsetHeight;
    }
  }

  rotationX(value: number) {
    if (this.cube !== undefined) this.cube.rotation.x = value;
  }

  rotationY(value: number): void {
    if (this.cube !== undefined) this.cube.rotation.y = value;
  }

  rotationZ(value: number): void {
    if (this.cube !== undefined) this.cube.rotation.z = value;
  }

  positionX(value: number): void {
    if (this.cube !== undefined) this.cube.position.x = value;
  }

  positionY(value: number): void {
    if (this.cube !== undefined) this.cube.position.y = value;
  }

  positionZ(value: number): void {
    if (this.cube !== undefined) this.cube.position.z = value;
  }

  scaleX(value: number) {
    if (this.cube !== undefined) {
      this.cube.scale.x = value;
    }
  }

  scaleY(value: number) {
    if (this.cube !== undefined) {
      this.cube.scale.y = value;
    }
  }

  scaleZ(value: number) {
    if (this.cube !== undefined) {
      this.cube.scale.z = value;
    }
  }

  alpha(value: number) {
    if (this.cube !== undefined)
      if (this.cubeMaterial) {
        this.cubeMaterial.setValues({
          opacity: value,
          transparent: true,
        });
      }
  }

  wireframe(value: boolean) {
    if (this.cube !== undefined)
      if (this.cubeMaterial) {
        this.cubeMaterial.setValues({
          wireframe: value,
        });
      }
  }
}
