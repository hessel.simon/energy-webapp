import { TopographyLoader, WaitForHeightLoader } from './TopographyLoader';
import * as THREE from 'three';
import { mPerPixel } from './util';
import { Polygon } from '@turf/helpers';

export class CSVTopographyLoader
  extends TopographyLoader
  implements WaitForHeightLoader
{
  protected mesh: THREE.Mesh | undefined;
  protected metadata: Record<string, any> = {};
  protected callbacks: Array<(mesh: THREE.Mesh) => void> = [];
  protected fetching = false;

  waitForHeight(geodeticCoords: number[], func: (mesh: THREE.Mesh) => void) {
    this.callbacks.push(func);
  }

  updateTerrain(box: Polygon | undefined, zoom: number | undefined) {
    if (this.fetching) return;
    this.fetching = true;
    fetch('./assets/topography/top1.csv')
      .then((response) => response.text())
      .then((data) => {
        const rows = data.split('\n');
        // save metadata
        // create simple
        let datastart;
        for (let i = 0; i < rows.length; i++) {
          if (rows[i].trim().match(/^\d/)) break;
          const kv = rows[i].trim().split(' ', 2);
          this.metadata[kv[0]] = kv[1];
          datastart = i;
        }
        this.metadata['ncols'] = parseInt(this.metadata['ncols']);
        this.metadata['nrows'] = parseInt(this.metadata['nrows']);
        const llcorner: [number, number] = [
          parseFloat(this.metadata['llcorner_lon']),
          parseFloat(this.metadata['llcorner_lat']),
        ];
        const geometry = new THREE.PlaneBufferGeometry(
          1,
          1,
          this.metadata['ncols'] + 1,
          this.metadata['nrows'] + 1
        );
        const pos = geometry.attributes.position;

        if (datastart) {
          const pa = pos.array;
          rows.slice(datastart + 1).forEach((row, row_index) => {
            if (!row) return;
            row
              .trim()
              .split(' ')
              .forEach((val, col_index) => {
                // TODO Code should redone dot use setXYZ() instead of updateing postions arrayLike object manually
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                pa[3 * (row_index * this.metadata['ncols'] + col_index) + 2] =
                  parseFloat(val) /
                  mPerPixel(llcorner[1], this.BASE_PLANE_DIMENSION);
              });
          });
        }
        pos.needsUpdate = true;
        geometry.computeVertexNormals();
        // make mesh
        // TODO make texture configurable
        const texture = new THREE.TextureLoader().load(
          './assets/textures/concrete.jpg',
          (_tex) => {}
        );
        const material = new THREE.MeshBasicMaterial({ map: texture });
        this.mesh = new THREE.Mesh(geometry, material);
        this.mesh.receiveShadow = true;
        // position and scale mesh

        const box = this.geodeticToLocal(llcorner);
        box.push(
          ...this.geodeticToLocal([
            llcorner[0] +
              parseFloat(this.metadata['cellsize']) * this.metadata['ncols'],
            llcorner[1] +
              parseFloat(this.metadata['cellsize']) * this.metadata['nrows'],
          ])
        );

        this.mesh.scale.setScalar(Math.abs(box[0] - box[3]));
        this.mesh.position.set(box[0], 0, box[2]);
        this.mesh.rotation.set(-Math.PI / 2, 0, 0);

        this.scene.add(this.mesh);
        // call waiting callbacks for height
        setTimeout(() => {
          this.callbacks.forEach((func) => {
            if (this.mesh) func(this.mesh);
          });
        }, 0);
      });
  }
}
