import SphericalMercator from '@mapbox/sphericalmercator';
import { Polygon } from '@turf/helpers';
import { Scene } from 'three';
import { mPerPixel } from './util';

export interface WaitForHeightLoader {
  waitForHeight(
    geodeticCoords: number[],
    func: (mesh: THREE.Mesh) => void
  ): void;
}
export class TopographyLoader {
  readonly BASE_PLANE_DIMENSION = 65024;
  sm: SphericalMercator = new SphericalMercator({
    size: this.BASE_PLANE_DIMENSION,
  });
  constructor(protected scene: Scene) {}

  geodeticToLocal(
    lnglat: ReturnType<TopographyLoader['localToGeodetic']>,
    elevation = 0
  ) {
    const [cx, cy] = this.sm.px(lnglat, 0);
    let y = 0;
    if (elevation && elevation != 0) {
      const metersPerPixel = mPerPixel(lnglat[1], this.BASE_PLANE_DIMENSION); //real-world distance this pixel represents
      y = elevation / metersPerPixel;
    }

    return [
      cx - this.BASE_PLANE_DIMENSION / 2,
      y,
      cy - this.BASE_PLANE_DIMENSION / 2,
    ];
  }

  localToGeodetic(x: number, z: number) {
    return this.sm.ll(
      [x + this.BASE_PLANE_DIMENSION / 2, z + this.BASE_PLANE_DIMENSION / 2],
      0
    );
  }

  heightToPixels(lat: number) {
    return mPerPixel(lat, this.BASE_PLANE_DIMENSION);
  }

  updateTerrain(box: Polygon | undefined, zoom: number | null | undefined) {}
}
