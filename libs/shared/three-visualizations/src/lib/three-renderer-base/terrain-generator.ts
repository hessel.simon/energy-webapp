import * as cover from '@mapbox/tile-cover';
import { Polygon } from '@turf/helpers';
import { environment } from 'environments/environment';
import getPixels from 'get-pixels';

import * as THREE from 'three';
import { Mesh, Scene } from 'three';
import { MeshWithCoordsAndZoom } from '../utils/MeshWithCoordsAndZoom';
import { TopographyLoader, WaitForHeightLoader } from './TopographyLoader';
import { slashify } from './util';

const NUM_WORKERS = 4; //window.navigator.hardwareConcurrency/2;

export default class TerrainGenerator
  extends TopographyLoader
  implements WaitForHeightLoader
{
  neighborTiles = [
    [0, 0, -1],
    [0, -1, 0],
    [0, 0, 1],
    [0, 1, 0],
  ] as const;
  row: number[][] = [[], [], [], []];
  totalCount = 49152;
  rowCount = 384;
  workerPool: Worker[] = [];
  zoom = 12;
  callbacks: Record<string, [(mesh: MeshWithCoordsAndZoom) => void]> = {};

  constructor(protected scene: Scene) {
    super(scene);
    //get specific pixel indices of each edge
    for (let c = 0; c < this.rowCount; c += 3) {
      //top, left, bottom, right
      this.row[0].push(c + 1);
      this.row[1].push((c / 3) * this.rowCount + 1);
      this.row[2].push(c + 1 + this.totalCount - this.rowCount);
      this.row[3].push((c / 3 + 1) * this.rowCount - 2);
    }
    for (let i = 0; i < NUM_WORKERS; i++) {
      const elevationWorker = new Worker('./tile-service.worker', {
        type: 'module',
      });
      elevationWorker.addEventListener('message', this.makeMesh.bind(this));
      this.workerPool.push(elevationWorker);
    }
  }

  //fill seam between elevation tiles by adopting the edge of neighboring tiles
  resolveSeams(
    data: number[] | Float32Array,
    [z, x, y]: number[] | Float32Array
  ) {
    //iterate through neighbors
    this.neighborTiles.forEach((tile, index) => {
      //figure out neighbor tile coordinate
      const targetTile = tile.map((coord, idx) => {
        return coord + [z, x, y][idx];
      });

      //if neighbor exists,
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const neighbor = this.scene.getObjectByProperty(
        'coords',
        slashify(targetTile)
      ) as Mesh;
      if (neighbor) {
        // indices that need to be overwritten
        const indicesToChange = this.row[index];
        //indices of neighbor vertices to copy
        const neighborIndices = this.row[(index + 2) % 4];
        const neighborVertices =
          neighbor.geometry.getAttribute('position').array;

        for (let a = 0; a < 128; a++) {
          data[indicesToChange[a]] = neighborVertices[neighborIndices[a]];
        }
      }
    });
    return data;
  }

  callTileReady(plane: MeshWithCoordsAndZoom) {
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const { coords } = plane as MeshWithCoordsAndZoom;
    if (this.callbacks[coords]) {
      this.callbacks[coords].forEach((func) => {
        func(plane);
      });
      delete this.callbacks[coords];
    }
  }

  makeMesh(e: MessageEvent) {
    const [arrayBuffer, tile] = e.data.makeMesh;
    let array = new Float32Array(arrayBuffer);

    const texture = new THREE.TextureLoader().load(
      this.assembleUrl(true, tile),
      (_tex) => {
        this.scene.remove(placeholder);
        plane.visible = true;
        this.callTileReady(plane);
      }
    );
    const material = new THREE.MeshBasicMaterial({
      map: texture,
      polygonOffset: true,
      polygonOffsetFactor: 0,
      polygonOffsetUnits: -0.8,
    });

    const tileSize = this.BASE_PLANE_DIMENSION / Math.pow(2, this.zoom);
    const segments = 127;
    const geometry = new THREE.PlaneBufferGeometry(
      tileSize,
      tileSize,
      segments,
      segments
    );

    array = new Float32Array(this.resolveSeams(array, tile));
    if (geometry.attributes.position instanceof THREE.BufferAttribute) {
      geometry.attributes.position.set(array);
    }

    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const plane = new THREE.Mesh(
      geometry,
      material
    ) as unknown as MeshWithCoordsAndZoom;

    plane.coords = slashify(tile);
    plane.zoom = tile[0];
    plane.receiveShadow = true;

    const placeholder = new THREE.Mesh(
      geometry,
      new THREE.MeshBasicMaterial({ wireframe: true, color: 0x999999 })
    );
    plane.visible = false;
    this.scene.add(placeholder);
    this.scene.add(plane);
  }

  updateTerrain(box: Polygon, zoom: number) {
    super.updateTerrain(box, zoom);
    if (zoom != this.zoom) {
      this.scene.children.forEach((obj) => {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        const temp = obj as MeshWithCoordsAndZoom;
        obj.visible = temp.zoom === undefined || temp.zoom == zoom;
      });
    }
    if (zoom) {
      this.zoom = zoom;
    }
    // reorder to zxy
    let imageTiles = cover
      .tiles(box, { min_zoom: zoom, max_zoom: zoom })
      .map(([x, y, z]) => {
        return [z, x, y];
      });
    // filter out existing tiles
    imageTiles = imageTiles.filter((tile: any[]) => {
      return !this.scene.getObjectByProperty('coords', slashify(tile));
    });

    const elevations = new Set<string>();
    //assemble list of elevation tiles, as grandparent tiles of imagery
    imageTiles.forEach((deslashed: number[]) => {
      const grandparent = [
        deslashed[0] - 2,
        Math.floor(deslashed[1] / 4),
        Math.floor(deslashed[2] / 4),
      ];
      elevations.add(slashify(grandparent));
    });
    const slashImageTiles = imageTiles.map(slashify);

    elevations.forEach((coords) => {
      const tile = coords.split('/').map((num) => {
        return parseFloat(num);
      });

      getPixels(this.assembleUrl(false, tile), (err, pixels) => {
        // if a tile is not available, it is probably an ocean tile
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        if (err) pixels = null;
        this.workerPool[2 * (tile[1] % 2) + (tile[2] % 2)].postMessage([
          pixels,
          tile,
          slashImageTiles,
          1,
        ]);
      });
    });
  }

  waitForHeight(geodeticCoords: number[], func: (mesh: THREE.Mesh) => void) {
    const coords = slashify(
      this.coordsToTile(geodeticCoords[0], geodeticCoords[1])
    );
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const mesh = this.scene.getObjectByProperty(
      'coords',
      coords
    ) as MeshWithCoordsAndZoom;
    if (mesh) {
      func(mesh);
      return;
    }
    if (this.callbacks[coords]) this.callbacks[coords].push(func);
    else this.callbacks[coords] = [func];
  }

  coordsToTile(x: number, y: number, z = this.zoom) {
    const tileSize = this.BASE_PLANE_DIMENSION / Math.pow(2, z);
    return [
      z,
      Math.floor((x + this.BASE_PLANE_DIMENSION / 2) / tileSize),
      Math.floor((y + this.BASE_PLANE_DIMENSION / 2) / tileSize),
    ];
  }

  private assembleUrl(img: boolean, coords: any[]) {
    const tileset = img ? 'mapbox.satellite' : 'mapbox.terrain-rgb'; //
    const res = img ? '@2x.png' : '@2x.pngraw';

    //domain sharding
    const serverIndex = 2 * (coords[1] % 2) + (coords[2] % 2);
    const server = ['a', 'b', 'c', 'd'][serverIndex];
    return (
      'https://' +
      server +
      '.tiles.mapbox.com/v4/' +
      tileset +
      '/' +
      slashify(coords) +
      res +
      '?access_token=' +
      environment.MAPBOX_ACCESS_TOKEN
    );
  }
}
