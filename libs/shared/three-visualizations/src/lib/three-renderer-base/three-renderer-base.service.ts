import {
  ElementRef,
  EventEmitter,
  Injectable,
  NgZone,
  OnDestroy,
} from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { PointerLockControls } from 'three/examples/jsm/controls/PointerLockControls';
import { TopographyLoader } from './TopographyLoader';
import { Polygon } from '@turf/helpers';
import buffer from '@turf/buffer';
import bboxPolygon from '@turf/bbox-polygon';
import bbox from '@turf/bbox';
import TerrainGenerator from './terrain-generator';
import { DirectionalLight } from 'three';
import Stats from 'stats.js';

const BOUNDARY_BUFFER = 0.1;

@Injectable({
  providedIn: 'root',
})
export class ThreeRendererBaseService implements OnDestroy {
  canvas: HTMLCanvasElement | undefined;
  renderer: THREE.WebGLRenderer | undefined;

  scene = new THREE.Scene();
  center = [0, 0];
  centerCoords = [0, 0];
  camera: THREE.PerspectiveCamera | undefined;
  basePlane: THREE.Mesh | undefined;
  controls: OrbitControls | undefined;
  firstPersonControls: PointerLockControls | undefined;
  terrain: THREE.Object3D | undefined;
  terrainGenerator: TerrainGenerator | undefined;
  boundaries: Polygon | undefined;
  raycaster = new THREE.Raycaster();
  preview = false;
  boundingBox: THREE.Box3 | undefined;

  // Misc
  initialized = false;
  frameId: number | undefined;
  clock: THREE.Clock = new THREE.Clock();
  stats: Stats = new Stats();

  // Events
  shadowCameraChanged = new EventEmitter();
  boundingBoxChanged = new EventEmitter<THREE.Box3>();

  constructor(protected ngZone: NgZone) {}

  ngOnDestroy() {
    // Try to clean up after ThreeJS, because it does nothing on its own.
    // * Note: This is not exhaustive and probably has to be extended!
    // * TODO: Dispose BoxBufferGeometry created by scene background (see WebGLRenderer / WebGLBackground)
    // * TODO: Dispose Texture created by RenderTarget of Water
    {
      const disposeMaterial = (material: THREE.Material) => {
        if (material instanceof THREE.ShaderMaterial) {
          for (const uniform of Object.values(material.uniforms)) {
            if (uniform.value instanceof THREE.Texture) {
              uniform.value.dispose();
            }
          }
        }

        if (material instanceof THREE.MeshBasicMaterial && material.map) {
          material.map.dispose();
        }

        material.dispose();
      };

      // Scene
      this.scene.traverse((object) => {
        if (object instanceof THREE.Mesh || object instanceof THREE.Line) {
          object.geometry.dispose();

          if (Array.isArray(object.material)) {
            for (const material of object.material) {
              disposeMaterial(material);
            }
          } else {
            disposeMaterial(object.material);
          }
        } else if (object instanceof THREE.DirectionalLight) {
          object.shadow.map.dispose();
        }
      });

      if (this.scene.background instanceof THREE.Texture) {
        this.scene.background.dispose();
      }

      //this.scene.dispose(); //Scene selber hat keinen Dispose nur einzelne Elemente

      // Renderer
      if (this.renderer) {
        this.renderer.dispose();
        this.renderer.forceContextLoss();
      }
    }

    // Warn if we need to call dispose() on something
    if (this.renderer) {
      const memoryInfo = this.renderer.info.memory;
      if (memoryInfo.geometries > 0 || memoryInfo.textures > 0) {
        console.warn(
          'ThreeJS warning: Call dispose() on all geometries and textures.',
          memoryInfo
        );
      }
    }
  }

  initialize(
    canvas: ElementRef<HTMLCanvasElement>,
    center: number[] = [0, 0]
  ): void {
    this.preview = !center[0] && !center[1];
    this.canvas = canvas.nativeElement;
    // this.terrainGenerator = new CSVTopographyLoader(this.scene);
    this.terrainGenerator = new TerrainGenerator(this.scene);
    this.basePlane = new THREE.Mesh(
      new THREE.PlaneBufferGeometry(1e6, 1e6, 1, 1),
      new THREE.MeshBasicMaterial({
        opacity: 0,
        wireframe: true,
        visible: false,
      })
    );
    this.basePlane.rotation.x = -0.5 * Math.PI;
    this.scene.add(this.basePlane);

    // calculate center coords
    this.center = center;
    this.centerCoords = this.terrainGenerator.geodeticToLocal([0, 0]);

    // Create renderer
    {
      this.renderer = new THREE.WebGLRenderer({
        canvas: this.canvas,
        antialias: true,
      });
      this.renderer.setSize(this.canvas.width, this.canvas.height);
      this.renderer.shadowMap.enabled = true;
      this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
    }

    // Add camera
    {
      this.camera = new THREE.PerspectiveCamera(
        75.0,
        this.canvas.width / this.canvas.height,
        1 / 10,
        1e7
      );
      this.camera.position.z = 1;
      this.camera.position.y = 0.35;
      this.camera.layers.enable(0);
      this.camera.layers.enable(1);

      this.scene.add(this.camera);
    }

    // Add camera controls
    {
      this.controls = new OrbitControls(this.camera, this.canvas);
      this.controls.enableKeys = false;
      this.controls.target.setY(0);
      this.controls.maxPolarAngle = Math.PI / 2 - 0.1;
      this.controls.maxDistance = 175;
      this.controls.update();
      this.canvas.addEventListener('keypress', (e) => {
        if (e.key == 'b') {
          if (this.controls && this.camera) {
            this.controls.target.set(
              this.camera.position.x,
              1,
              this.camera.position.z + 200
            );
            this.controls.update();
            this.camera.position.setY(0.2);
            this.controls.dispose();
            this.controls = undefined;
            this.firstPersonControls = new PointerLockControls(
              this.camera,
              this.canvas
            );
            this.firstPersonControls.lock();
          } else {
            if (this.firstPersonControls && this.camera) {
              this.firstPersonControls.unlock();
              this.firstPersonControls.dispose();
              this.firstPersonControls = undefined;
              this.controls = new OrbitControls(this.camera, this.canvas);
              this.controls.target.set(
                this.camera.position.x,
                1,
                this.camera.position.z
              );
              this.camera.position.setY(10);
              this.controls.update();
            }
          }
        }
      });
    }

    // Add lights
    {
      const ambient = new THREE.AmbientLight(0xffffff, 0.8);
      ambient.name = 'lights.ambient';
      this.scene.add(ambient);

      const directional = new THREE.DirectionalLight(0xffffff);
      directional.name = 'lights.directional';
      directional.castShadow = true;
      directional.shadow.mapSize.width = 4096;
      directional.shadow.mapSize.height = 4096;

      directional.position.set(
        this.centerCoords[0],
        200,
        this.centerCoords[2] + 200
      );
      directional.target.position.set(
        this.centerCoords[0],
        0,
        this.centerCoords[2]
      );
      this.scene.add(directional);
      this.scene.add(directional.target);
    }

    this.boundingBoxChanged.subscribe(() => {
      this.updateShadowCamera();
    });

    this.initialized = true;
  }

  getZoom() {
    if (this.firstPersonControls) {
      return 12;
    }
    if (this.controls) {
      const pt = this.controls.target.distanceTo(this.controls.object.position);
      return Math.floor(
        Math.min(Math.max(Math.log(pt / 12000) / Math.log(0.5) + 3, 4), 22)
      );
    }
    return;
  }

  updateTiles() {
    const zoom = this.getZoom();

    const ul = { x: -1, y: -1, z: -1 } as const;
    const ur = { x: 1, y: -1, z: -1 } as const;
    const lr = { x: 1, y: 1, z: 1 } as const;
    const ll = { x: -1, y: 1, z: 1 } as const;

    const coordinates = [ul, ur, lr, ll, ul] as const;
    // switch to fixed radius mode
    let corners: Array<
      ReturnType<TopographyLoader['localToGeodetic']> | undefined
    > = [];
    if (this.firstPersonControls) {
      corners = coordinates.map((corner) => {
        if (this.terrainGenerator && zoom && this.camera) {
          return this.terrainGenerator.localToGeodetic(
            (corner.x * 5 * 65024) / Math.pow(2, zoom) + this.camera.position.x,
            (corner.z * 5 * 65024) / Math.pow(2, zoom) + this.camera.position.z
          );
        } else return;
      });
    } else if (this.controls && this.controls.getPolarAngle() > 0.6) {
      corners = coordinates.map((corner) => {
        if (this.terrainGenerator && zoom && this.controls) {
          return this.terrainGenerator.localToGeodetic(
            (corner.x * 5 * 65024) / Math.pow(2, zoom) + this.controls.target.x,
            (corner.z * 5 * 65024) / Math.pow(2, zoom) + this.controls.target.z
          );
        } else return;
      });
    } else {
      corners = coordinates.map((corner) => {
        if (this.camera && this.basePlane && this.terrainGenerator) {
          this.raycaster.setFromCamera(corner, this.camera);
          const pt = this.raycaster.intersectObject(this.basePlane)[0].point;
          return this.terrainGenerator.localToGeodetic(pt.x, pt.z);
        } else return;
      });
    }

    if (this.terrainGenerator) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const coordinates: number[][] = corners.filter(
        (corner) => corner !== undefined
      ) as ReturnType<TopographyLoader['localToGeodetic']>[];
      this.terrainGenerator.updateTerrain(
        {
          type: 'Polygon',
          coordinates: [coordinates],
        },
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        this.getZoom() as number
      );
    }
  }

  // center in longitude, latitude
  setupWorld(fieldBoundaries?: number[][]) {
    let box = {};
    if (!fieldBoundaries || !fieldBoundaries.length) {
      box = {
        type: 'Polygon',
        coordinates: [
          [
            [this.center[0] - 0.15, this.center[1] - 0.15],
            [this.center[0] - 0.15, this.center[1] + 0.15],
            [this.center[0] + 0.15, this.center[1] + 0.15],
            [this.center[0] + 0.15, this.center[1] - 0.15],
            [this.center[0] - 0.15, this.center[1] - 0.15],
          ],
        ],
      };
    } else {
      if (fieldBoundaries[0] != fieldBoundaries[-1])
        fieldBoundaries.push(fieldBoundaries[0]);
      box = {
        type: 'Polygon',
        coordinates: [fieldBoundaries],
      };
    }

    this.boundaries = buffer(bboxPolygon(bbox(box)), BOUNDARY_BUFFER).geometry;

    // center camera/controls
    if (this.camera) {
      this.camera.position.set(this.centerCoords[0], 5, this.centerCoords[2]);
    }
    if (this.controls) {
      this.controls.target.set(this.centerCoords[0], 0, this.centerCoords[2]);
      this.controls.update();
    }
    if (!this.preview && this.controls) {
      let updateTimeout = setTimeout(() => {
        this.updateTiles();
      }, 500);
      this.controls.addEventListener('change', (e) => {
        clearTimeout(updateTimeout);
        updateTimeout = setTimeout(() => {
          this.updateTiles();
        }, 500);
      });
    }
  }
  updateInstances() {}

  render() {
    this.frameId = requestAnimationFrame(() => {
      this.render();
    });

    if (this.firstPersonControls) {
      this.firstPersonControls.moveForward(0.01);
    }

    this.updateInstances();
    if (this.camera && this.renderer) {
      this.renderer.render(this.scene, this.camera);
    }
    this.stats.update();
  }

  animate() {
    // We have to run this outside angular zones,
    // because it could trigger heavy changeDetection cycles.
    this.ngZone.runOutsideAngular(() => {
      if (document.readyState != 'loading') {
        this.render();
      } else {
        window.addEventListener('DOMContentLoaded', () => {
          this.render();
        });
      }
    });
  }

  resize() {
    if (!this.initialized) {
      return;
    }

    if (this.canvas && this.camera && this.renderer) {
      // Fit canvas to container
      this.canvas.style.width = '100%';
      this.canvas.style.height = '100%';

      this.canvas.width = this.canvas.offsetWidth;
      this.canvas.height = this.canvas.offsetHeight;

      // Update camera aspect ratio and renderer size
      const width = this.canvas.width;
      const height = this.canvas.height;

      this.camera.aspect = width / height;
      this.camera.updateProjectionMatrix();

      this.renderer.setSize(width, height);
    }
  }

  async addAreaOutline(area: number[][], color: string | number | THREE.Color) {
    const points = [];
    for (const point of area) {
      if (this.terrainGenerator && point.length == 2) {
        const pointCoords = this.terrainGenerator.geodeticToLocal([
          point[0],
          point[1],
        ]);
        points.push(new THREE.Vector2(pointCoords[0], pointCoords[2]));
      }
    }

    // Generate shape and mesh from points
    const shape = new THREE.Shape(points);
    shape.autoClose = true;

    const line = new THREE.Line(
      new THREE.BufferGeometry().setFromPoints(shape.getPoints()),
      new THREE.LineBasicMaterial({
        color: color,
        linewidth: 3,
      })
    );
    line.name = 'area.line';
    line.layers.set(1);
    line.position.setY(1);
    line.rotateX(Math.PI / 2);

    this.scene.add(line);
  }

  async generateTerrain() {
    if (this.terrainGenerator && this.boundaries) {
      console.log(this.boundaries);
      this.terrainGenerator.updateTerrain(
        this.boundaries,
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        this.getZoom() as number
      );
    }
  }

  async addDebugHelpers() {
    // Add camera helper for shadow camera
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const directionalLight = this.scene.getObjectByName(
      'lights.directional'
    ) as DirectionalLight;
    if (directionalLight) {
      const shadowCameraHelper = new THREE.CameraHelper(
        directionalLight.shadow.camera
      );
      shadowCameraHelper.name = 'helpers.shadowCamera';
      shadowCameraHelper.frustumCulled = false;
      shadowCameraHelper.layers.set(1);
      this.scene.add(shadowCameraHelper);

      this.shadowCameraChanged.subscribe(() => {
        shadowCameraHelper.update();
      });
    }

    // Add box helper for bounding box
    if (this.boundingBox) {
      const boundingBoxHelper = new THREE.Box3Helper(this.boundingBox);
      boundingBoxHelper.name = 'helpers.boundingBox';
      boundingBoxHelper.visible = false;
      boundingBoxHelper.frustumCulled = false;
      boundingBoxHelper.layers.set(1);
      this.scene.add(boundingBoxHelper);

      this.boundingBoxChanged.subscribe((boundingBox: THREE.Box3) => {
        boundingBoxHelper.visible = true;
        boundingBoxHelper.box = boundingBox;
      });
    }
  }

  updateShadowCamera() {
    return;
    /* let directionalLight = <THREE.DirectionalLight>(
      this.scene.getObjectByName('lights.directional')
    );

    if (directionalLight == undefined) {
      return;
    }

    let shadowCamera = directionalLight.shadow.camera;

    let boundingBox = this.boundingBox;
    let minMax = [boundingBox.min, boundingBox.max];

    // Collect x/y/z-values of all boundig box vertices
    let values = { x: [], y: [], z: [] };
    for (let i = 0; i < 8; i++) {
      // Use bits for indexing
      let bit1 = (i >> 0) & 1;
      let bit2 = (i >> 1) & 1;
      let bit3 = (i >> 2) & 1;

      // Transform vertex to camera space
      let v = new THREE.Vector3(
        minMax[bit1].x,
        minMax[bit2].y,
        minMax[bit3].z
      ).applyMatrix4(shadowCamera.matrixWorldInverse);

      values.x.push(v.x);
      values.y.push(v.y);
      values.z.push(v.z);
    }

    // Camera parameters are min/max of the x/y/z-values in camera space
    let right = Math.max(...values.x);
    let left = Math.min(...values.x);
    let top = Math.max(...values.y);
    let bottom = Math.min(...values.y);
    let near = -Math.max(...values.z); // Camera looks down -z

    // Calculate 'far' via ray-plane-intersection from top to water
    let normal = new THREE.Vector3(0, 1, 0);
    let origin = new THREE.Vector3(0, top, 0).applyMatrix4(
      shadowCamera.matrixWorld
    );
    let direction = new THREE.Vector3(0, 0, Math.min(...values.z))
      .applyMatrix4(shadowCamera.matrixWorld)
      .normalize();
    let far = -origin.dot(normal) / direction.dot(normal);

    // Set camera parameters
    shadowCamera.left = left;
    shadowCamera.right = right;
    shadowCamera.top = top;
    shadowCamera.bottom = bottom;
    shadowCamera.near = near;
    shadowCamera.far = far;
    shadowCamera.updateProjectionMatrix();

    this.shadowCameraChanged.emit(); */
  }

  calculateBoundingBox() {
    return new THREE.Box3(
      new THREE.Vector3(this.centerCoords[0], 0, this.centerCoords[2]),
      new THREE.Vector3(this.centerCoords[0], 0, this.centerCoords[2])
    );
  }

  updateBoundingBox() {
    const boundingBox = this.calculateBoundingBox();
    if (boundingBox != this.boundingBox) {
      this.boundingBox = boundingBox;
      this.boundingBoxChanged.emit(boundingBox);
    }
  }
}
