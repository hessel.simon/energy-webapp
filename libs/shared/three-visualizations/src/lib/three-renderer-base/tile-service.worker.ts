/// <reference lib="webworker" />

import SphericalMercator from '@mapbox/sphericalmercator';
import { deslash, mPerPixel, slashify } from './util';

const basePlaneDimension = 65024;
const tilePixels = new SphericalMercator({ size: 128 });
const working = false;

const cols = 512;
const rows = 512;
const scaleFactor = 4;

const sixteenthPixelRanges = [];

for (let c = 0; c < scaleFactor; c++) {
  for (let r = 0; r < scaleFactor; r++) {
    //pixel ranges
    sixteenthPixelRanges.push([
      [r * (rows / scaleFactor - 1) + r, ((r + 1) * rows) / scaleFactor],
      [c * (cols / scaleFactor - 1) + c, ((c + 1) * cols) / scaleFactor],
    ]);
  }
}

addEventListener('message', ({ data }) => {
  const time = Date.now();

  const pixels = data[0];
  const coords = data[1]; //terrain tile coord
  const tiles = data[2]; //requested imagery coords
  const parserIndex = data[3];
  const z = coords[0];
  const x = coords[1];
  const y = coords[2];

  let elevations = [];

  if (pixels) {
    //colors => elevations
    for (let e = 0; e < pixels.data.length; e += 4) {
      const R = pixels.data[e];
      const G = pixels.data[e + 1];
      const B = pixels.data[e + 2];
      const elev = -10000 + (R * 256 * 256 + G * 256 + B) * 0.1;
      // set far below 0 to eliminate artifacts
      elevations.push(elev ? elev : -100);
    }
  } else elevations = new Array(1048576).fill(-100);

  // figure out tile coordinates of the 16 grandchildren of this tile
  const sixteenths = [];
  for (let c = 0; c < scaleFactor; c++) {
    for (let r = 0; r < scaleFactor; r++) {
      //tile coordinates
      sixteenths.push(
        slashify([z + 2, x * scaleFactor + c, y * scaleFactor + r])
      );
    }
  }

  //iterate through sixteenths...

  const tileSize = basePlaneDimension / Math.pow(2, z + 2);
  const vertices = 128;
  const segments = vertices - 1;
  const segmentLength = tileSize / segments;

  //check 16 grandchildren of this terrain tile
  sixteenths.forEach(function (d, i) {
    //if this grandchild is actually in view, proceed...
    if (tiles.indexOf(d) > -1) {
      d = deslash(d);
      const pxRange = sixteenthPixelRanges[i];
      const elev = [];

      const xOffset = (d[1] + 0.5) * tileSize - basePlaneDimension / 2;
      const yOffset = (d[2] + 0.5) * tileSize - basePlaneDimension / 2;
      let r = 0;
      let c = 0;
      //grab its elevations from the 4x4 grid
      for (r = pxRange[0][0]; r < pxRange[0][1]; r++) {
        for (c = pxRange[1][0]; c < pxRange[1][1]; c++) {
          const currentPixelIndex = r * cols + c;
          elev.push(elevations[currentPixelIndex]);
        }
      }
      const array = new Float32Array(vertices * vertices * 3);
      let dataIndex = 0;

      //iterate through rows
      for (r = 0; r < vertices; r++) {
        const yPx = d[2] * 128 + r;
        const pixelLat = tilePixels.ll([x * tileSize, yPx], d[0])[1]; //latitude of this pixel
        const metersPerPixel = mPerPixel(pixelLat, basePlaneDimension); //real-world distance this pixel represents

        // y position of vertex in world pixel coordinates
        const yPos = -r * segmentLength + tileSize / 2;

        //iterate through columns
        for (c = 0; c < vertices; c++) {
          const xPos = c * segmentLength - tileSize / 2;
          array[dataIndex * 3] = xPos + xOffset;
          array[dataIndex * 3 + 1] = elev[dataIndex] / metersPerPixel;
          array[dataIndex * 3 + 2] = -yPos + yOffset;
          dataIndex++;
        }
      }
      const arrayBuffer = array.buffer;
      postMessage({ makeMesh: [arrayBuffer, d] }, [arrayBuffer]);
    }
  });
});
