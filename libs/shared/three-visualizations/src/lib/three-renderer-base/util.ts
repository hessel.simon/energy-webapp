export function slashify(input: any[]) {
  return input.join('/');
}
export function deslash(input: string) {
  return input.split('/').map(function (str) {
    return parseInt(str);
  });
}
export function mPerPixel(latitude: number, base_plane_dimension: number) {
  return Math.abs(
    (40075000 * Math.cos((latitude * Math.PI) / 180)) / base_plane_dimension
  );
}
