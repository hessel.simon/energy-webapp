import * as THREE from 'three';
import RectangularHeliostat from './RectangularHeliostat';
import { Vector3 } from 'three';

export default class HeliostatGroup {
  public mesh: THREE.Group | undefined;
  constructor(
    protected config: {
      [x: string]: any;
      heliostat_type_name?: string;
      facet_surface_form?: string;
      focal_length?: number[];
      heliostat_shape?: string;
      facet_width?: number;
      facet_height?: number;
      num_horizontal_facets?: number;
      num_vertical_facets?: number;
      facet_horizontal_gap?: number;
      facet_vertical_gap?: number;
      canting?: string;
      off_axis_reference_angles?: { azimuth: number; altitude: number };
      pedestal_height?: number;
      cluster_pattern?: string;
      aiming_strategy?: string;
      heliostat_num_corners?: number;
      facet_outer_side_length?: number;
      facet_gap?: number;
    }
  ) {}

  build() {
    const count = this.config['number'] ? this.config['number'] : 3;
    const heliostat = new RectangularHeliostat(this.config);
    const singlemesh = heliostat.build();

    const mesh = new THREE.Group();
    mesh.add(singlemesh);

    const material_frame = new THREE.MeshStandardMaterial({
      color: 0x666666,
      side: THREE.DoubleSide,
    });

    const railbar_geometry = new THREE.CylinderGeometry(0.3, 0.3, 50, 5);
    railbar_geometry.merge(railbar_geometry.clone().translate(4, 0, 0));
    railbar_geometry.rotateZ(Math.PI / 2);
    const railbar_rods = new THREE.CylinderGeometry(0.1, 0.1, 6, 5);
    for (let i = 0; i < 5; i++) {
      railbar_geometry.merge(
        railbar_rods
          .clone()
          .rotateZ(-Math.PI / 4)
          .translate(-22 + (48.0 / 5) * i, 2, 0)
      );
      railbar_geometry.merge(
        railbar_rods
          .clone()
          .rotateZ(Math.PI / 4)
          .translate(-18 + (48.0 / 5) * i, 2, 0)
      );
    }
    railbar_geometry.translate(25, 0, 0);
    let copy = railbar_geometry.clone();
    railbar_geometry.merge(
      copy
        .clone()
        .rotateY(Math.PI / 3)
        .translate(0, 0, 0)
    );
    railbar_geometry.merge(
      copy
        .clone()
        .rotateY(-Math.PI / 3)
        .translate(25, 0, -Math.sqrt(50 ** 2 - 25 ** 2))
    );
    if (count == 6) {
      copy = railbar_geometry.clone();
      railbar_geometry.merge(
        copy.clone().translate(25, 0, -Math.sqrt(50 ** 2 - 25 ** 2))
      );
      railbar_geometry.merge(copy.clone().translate(50, 0, 0));
    }
    const railbar = new THREE.Mesh(railbar_geometry, material_frame);
    railbar.scale.setScalar(0.5);
    railbar.translateY(1);
    const group = new THREE.Group();
    group.add(railbar);
    heliostat.updateOrientation(new Vector3(0, 1, 10), new Vector3(0, 1, 0));
    group.add(singlemesh.clone());
    let nextmesh = singlemesh.clone();
    nextmesh.position.set(25, 0, 0);
    group.add(nextmesh);
    nextmesh = singlemesh.clone();
    nextmesh.position.set(12.5, 0, -Math.sqrt(25 ** 2 - 12.5 ** 2));
    group.add(nextmesh);
    group.scale.setScalar(0.0008979);
    this.mesh = group;
    return this.mesh;
  }
}
