import Heliostat from './heliostat';
import {
  CylinderGeometry,
  DoubleSide,
  ExtrudeGeometry,
  Geometry,
  Group,
  Matrix4,
  Mesh,
  MeshStandardMaterial,
  Shape,
} from 'three';
import { MATERIAL_FACET } from './materials';
import * as THREE from 'three';

export default class HeliostatPolygone extends Heliostat {
  constructor(config, position = new THREE.Vector3()) {
    super(config, position);
  }

  updateOrientation(sunDir, towerPos) {
    if (sunDir != null) this.sunDirection.copy(sunDir);

    if (towerPos != null) this.towerPosition.copy(towerPos);

    const lookAt = this.lookAtVector;

    /* This needs to be done as Object3D#lookAt seems
     * to calculate its vector depending on the world
     * position of the heliostat.
     */
    const groupPos = this.group.position.clone();
    this.group.position.set(0, 0, 0);

    const mesh_heliostat = this.group.getObjectByName('facets');
    mesh_heliostat.position.set(0, 0, 0);
    mesh_heliostat.lookAt(lookAt);
    mesh_heliostat.position.setY(this.config.pedestal_height);

    const mesh_frame = this.group.getObjectByName('frame');
    mesh_frame.position.set(0, 0, 0);
    // Move frame back one unit along lookAt direction
    mesh_frame.position.sub(lookAt.clone().setY(0).multiplyScalar(0.5));

    const mesh_truss = this.group.getObjectByName('truss');
    mesh_truss.position.set(0, 0, 0);
    // Rotate, but look at opposite side, because plane is visible on one site only
    mesh_truss.lookAt(lookAt.clone().multiplyScalar(-1));
    // Height
    mesh_truss.position.setY(this.config.pedestal_height);
    // Move frame back one unit along lookAt direction
    mesh_truss.position.sub(lookAt.clone().multiplyScalar(0.1));

    // Reset position
    this.group.position.copy(groupPos);
  }

  build() {
    const group = new Group();

    /**
     * Tilting option can be used, but it leads the frame
     * not being placed at the right place. This should
     * probably be fixed in the future.
     */
    const tiltAngle = 0;
    const numCorners = this.config.heliostat_num_corners;
    const facetSideLength = this.config.facet_outer_side_length;

    // Degree per facet: 2 * Math.PI / (2 * numCorners)
    // = 360° / (2 * numCorners) = 180° / numCorners
    const degreePerFacet = Math.PI / numCorners;
    const hypotenuse = facetSideLength / Math.sin(degreePerFacet);
    const ankathete = facetSideLength / Math.tan(degreePerFacet);

    // Build one triangle
    const shape = new Shape();
    shape.moveTo(0, 0);
    shape.lineTo(facetSideLength, ankathete);
    shape.lineTo(0, ankathete);
    shape.lineTo(0, 0);

    // Orientation of the heliostat
    const lookAt = this.lookAtVector;

    // Build facet mesh
    const geometry_facet_right = new ExtrudeGeometry(shape, {
      depth: 0.1,
      bevelEnabled: false,
    });
    // Fix origin point to be in center of triangle's depth + some offset for gaps
    const offsetX = this.config.facet_gap / 2;
    const offsetY =
      offsetX / Math.sin(degreePerFacet) + offsetX / Math.tan(degreePerFacet);
    geometry_facet_right.translate(offsetX, offsetY, -0.05);

    // Duplicate facet to define one facet group
    const geometry_facet_left = geometry_facet_right.clone();
    // Flip facet
    geometry_facet_left.applyMatrix(new Matrix4().makeScale(-1, 1, -1));

    // Tilting
    geometry_facet_right.rotateX(tiltAngle);
    geometry_facet_left.rotateX(tiltAngle);

    // The on facet group
    const geometry_facet = new Geometry();
    geometry_facet.merge(geometry_facet_right);
    geometry_facet.merge(geometry_facet_left);

    const geometry_facets = new Geometry();

    // Fill polygon
    for (let i = 0; i < numCorners; i++) {
      const geometry = geometry_facet.clone();
      geometry.rotateZ(2 * degreePerFacet * i);
      geometry_facets.merge(geometry);
    }

    // Material
    const material_facets = MATERIAL_FACET.clone();
    material_facets.envMap = this.skyboxCubemap;

    const mesh_heliostat = new Mesh(geometry_facets, material_facets);
    mesh_heliostat.name = 'facets';
    mesh_heliostat.castShadow = true;

    // Align
    mesh_heliostat.lookAt(lookAt);
    // Height
    mesh_heliostat.position.setY(this.config.pedestal_height);

    group.add(mesh_heliostat);

    // Frame supports
    const geometry_frame = new CylinderGeometry(
      0.5,
      0.5,
      this.config.pedestal_height,
      5
    );
    geometry_frame.translate(0, this.config.pedestal_height / 2, 0);
    const material_frame = new MeshStandardMaterial({
      color: 0x666666,
      side: DoubleSide,
    });

    const mesh_frame = new Mesh(geometry_frame, material_frame);
    mesh_frame.castShadow = true;
    mesh_frame.name = 'frame';
    // Move frame back one unit along lookAt direction
    mesh_frame.position.sub(lookAt.clone().setY(0).multiplyScalar(0.5));

    // Frame truss
    const geometry_rod = new THREE.PlaneGeometry(
      0.3,
      offsetY + ankathete * 0.66
    );
    geometry_rod.translate(0, geometry_rod.parameters.height / 2, 0);
    geometry_rod.rotateX(-tiltAngle);
    const geometry_truss = new Geometry();
    for (let i = 0; i < numCorners * 2; i++) {
      const copy = geometry_rod.clone();
      copy.rotateZ(degreePerFacet * i);
      geometry_truss.merge(copy);
    }

    // Add rings
    const geometry_ring = new THREE.RingGeometry(
      geometry_rod.parameters.height,
      geometry_rod.parameters.height + 0.3,
      numCorners * 2,
      1
    );

    geometry_truss.merge(geometry_ring);

    const mesh_truss = new Mesh(geometry_truss, material_frame);
    mesh_truss.castShadow = true;
    mesh_truss.name = 'truss';
    // Rotate, but look at opposite side, because plane is visible on one site only
    mesh_truss.lookAt(lookAt.clone().multiplyScalar(-1));
    // Height
    mesh_truss.position.setY(this.config.pedestal_height);
    // Move frame back one unit along lookAt direction
    mesh_truss.position.sub(lookAt.clone().multiplyScalar(0.1));

    group.add(mesh_frame);
    group.add(mesh_truss);

    group.position.copy(this.position);

    this.group = group;

    return group;
  }
}
