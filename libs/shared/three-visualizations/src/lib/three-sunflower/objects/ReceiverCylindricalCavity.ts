import Receiver from './Receiver';
import * as THREE from 'three';
import { MATERIAL_RECEIVER } from './materials';

export default class ReceiverCylindricalCavity extends Receiver {
  constructor(receiverConfig) {
    super(receiverConfig);
  }

  buildMesh(towerType, towerRadius = 0) {
    switch (towerType) {
      case 'rectangular':
        return this.buildMeshForRectangular();
      case 'cylindrical':
        return this.buildMeshForCylindrical(towerRadius);
    }
  }

  buildMeshForRectangular() {
    const halfDegree =
      Math.PI /
      this.config.cylindrical_cavity_receiver.num_panels_horizontal /
      2;
    const r =
      this.config.cylindrical_cavity_receiver.panel_width /
      2 /
      Math.tan(halfDegree);

    const geometry_receiver = new THREE.CylinderGeometry(
      r,
      r,
      this.config.height,
      this.config.cylindrical_cavity_receiver.num_panels_horizontal,
      1,
      true,
      Math.PI / 2,
      Math.PI
    );
    // Flip cylinder inside-out
    geometry_receiver.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));

    this.mesh = new THREE.Mesh(geometry_receiver, MATERIAL_RECEIVER);

    return this.mesh;
  }

  buildMeshForCylindrical(towerRadius) {
    // Calculate radius depending on panel width
    const alpha =
      (2 * Math.PI) /
      (2 * this.config.cylindrical_cavity_receiver.num_panels_horizontal + 1);
    const r =
      this.config.cylindrical_cavity_receiver.panel_width /
      2 /
      Math.tan(alpha / 2);

    const receiverAlpha =
      this.config.cylindrical_cavity_receiver.num_panels_horizontal * alpha;

    const geometry_receiver = new THREE.CylinderGeometry(
      r,
      r,
      this.config.height,
      this.config.cylindrical_cavity_receiver.num_panels_horizontal,
      1,
      true,
      -receiverAlpha / 2 + Math.PI, // Center receiver
      receiverAlpha
    );

    // Flip cylinder inside-out
    geometry_receiver.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));

    // Translate calculations
    const distToCenter = Math.cos(receiverAlpha / 2) * r;
    const receiverHalfWidth = Math.sin(receiverAlpha / 2) * r;

    const beta = Math.asin(receiverHalfWidth / towerRadius);
    const betaHeight = Math.cos(beta) * towerRadius;
    const offsetZ = towerRadius - betaHeight;

    geometry_receiver.translate(0, 0, distToCenter + towerRadius - offsetZ);

    this.mesh = new THREE.Mesh(geometry_receiver, MATERIAL_RECEIVER);

    return this.mesh;
  }
}
