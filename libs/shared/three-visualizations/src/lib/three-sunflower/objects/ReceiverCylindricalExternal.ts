import Receiver from './Receiver';
import * as THREE from 'three';
import { MATERIAL_RECEIVER } from './materials';

export default class ReceiverCylindricalExternal extends Receiver {
  constructor(receiverConfig) {
    super(receiverConfig);
  }

  buildMesh() {
    // Calculate radius depending on panel width
    const halfDegree =
      Math.PI / this.config.cylindrical_external_receiver.num_panels_horizontal;
    const r =
      this.config.cylindrical_external_receiver.panel_width /
      2 /
      Math.tan(halfDegree);

    const geometry = new THREE.CylinderGeometry(
      r,
      r,
      this.config.height,
      this.config.cylindrical_external_receiver.num_panels_horizontal
    );

    this.mesh = new THREE.Mesh(geometry, MATERIAL_RECEIVER);

    return this.mesh;
  }
}
