import Receiver from './Receiver';
import * as THREE from 'three';
import { MATERIAL_RECEIVER } from './materials';

export default class ReceiverFlat extends Receiver {
  constructor(receiverConfig) {
    super(receiverConfig);
  }

  buildMesh() {
    const geometry = new THREE.BoxGeometry(
      this.config.flat_receiver.width,
      this.config.height,
      1
    );

    // Tilting
    geometry.translate(0, this.config.height / 2, 0);
    // Degree to radian
    geometry.rotateX((this.config.flat_receiver.tilt_angle * Math.PI) / 180.0);
    geometry.translate(0, -this.config.height / 2, 0);

    this.mesh = new THREE.Mesh(geometry, MATERIAL_RECEIVER);

    return this.mesh;
  }
}
