import Heliostat from './heliostat';
import * as THREE from 'three';
import { MATERIAL_FACET } from './materials';

export default class RectangularHeliostat extends Heliostat {
  constructor(config: any, position = new THREE.Vector3()) {
    super(config, position);
  }

  updateOrientation(
    sunDir: THREE.Vector3 | null,
    towerPos: THREE.Vector3 | null
  ) {
    if (sunDir != null) this.sunDirection.copy(sunDir);

    if (towerPos != null) this.towerPosition.copy(towerPos);

    const lookAt = this.lookAtVector;

    /* This needs to be done as Object3D#lookAt seems
     * to calculate its vector depending on the world
     * position of the heliostat.
     */
    if (this.group) {
      const groupPos = this.group.position.clone();
      this.group.position.set(0, 0, 0);

      const mesh_heliostat = this.group.getObjectByName('facets');
      if (mesh_heliostat) {
        mesh_heliostat.position.set(0, 0, 0);
        mesh_heliostat.lookAt(lookAt);
        mesh_heliostat.position.setY(this.config.pedestal_height);
      }

      const mesh_frame = this.group.getObjectByName('frame');
      if (mesh_frame) {
        mesh_frame.position.set(0, 0, 0);
        mesh_frame.position.sub(lookAt.clone().setY(0).multiplyScalar(0.5));
      }

      const mesh_frame2 = this.group.getObjectByName('frame2');
      if (mesh_frame2) {
        mesh_frame2.position.set(0, 0, 0);
        mesh_frame2.lookAt(lookAt);
        mesh_frame2.position.setY(this.config.pedestal_height);
        // Move frame back one unit along lookAt direction
        mesh_frame2.position.sub(lookAt.clone().multiplyScalar(0.3));
      }

      const mesh_truss = this.group.getObjectByName('truss');
      if (mesh_truss) {
        mesh_truss.position.set(0, 0, 0);
        // Rotate, but look at opposite side, because plane is visible on one site only
        mesh_truss.lookAt(lookAt.clone().multiplyScalar(-1));
        // Height
        mesh_truss.position.setY(this.config.pedestal_height);
        // Move frame back one unit along lookAt direction
        mesh_truss.position.sub(lookAt.clone().multiplyScalar(0.1));
      }

      // Reset position
      this.group.position.copy(groupPos);
    }
  }

  build() {
    const group = new THREE.Group();
    const fullWidth =
      this.config.num_horizontal_facets * this.config.facet_width +
      (this.config.num_horizontal_facets - 1) *
        this.config.facet_horizontal_gap;
    const fullHeight =
      this.config.num_vertical_facets * this.config.facet_height +
      (this.config.num_vertical_facets - 1) * this.config.facet_vertical_gap;
    const lookAt = this.lookAtVector;

    {
      // Facets mesh
      const geometry_facet = new THREE.BoxGeometry(
        this.config.facet_width,
        this.config.facet_height,
        0.1
      );
      geometry_facet.translate(
        geometry_facet.parameters.width / 2,
        geometry_facet.parameters.height / 2,
        geometry_facet.parameters.depth / 2
      );

      const geometry_facets = new THREE.BufferGeometry();

      for (let x = 0; x < this.config.num_horizontal_facets; x++) {
        for (let y = 0; y < this.config.num_vertical_facets; y++) {
          const facet = geometry_facet.clone();
          facet.translate(
            x * (this.config.facet_width + this.config.facet_horizontal_gap),
            y * (this.config.facet_height + this.config.facet_vertical_gap),
            0
          );

          geometry_facets.merge(facet);
        }
      }

      geometry_facets.translate(-fullWidth / 2, -fullHeight / 2, 0);

      // Material
      const material_facets = MATERIAL_FACET.clone();
      material_facets.envMap = this.skyboxCubemap;

      const mesh_heliostat = new THREE.Mesh(geometry_facets, material_facets);
      mesh_heliostat.name = 'facets';
      // Enable shadow casting
      mesh_heliostat.castShadow = true;

      mesh_heliostat.lookAt(lookAt);
      mesh_heliostat.position.setY(this.config.pedestal_height);

      group.add(mesh_heliostat);
    }

    {
      // Frame building
      // Frame supports
      const geometry_frame = new THREE.CylinderGeometry(
        0.5,
        0.5,
        this.config.pedestal_height,
        5
      );
      geometry_frame.translate(0, this.config.pedestal_height / 2, 0);
      const material_frame = new THREE.MeshStandardMaterial({
        color: 0x666666,
        side: THREE.DoubleSide,
      });

      const mesh_frame = new THREE.Mesh(geometry_frame, material_frame);
      mesh_frame.castShadow = true;
      mesh_frame.name = 'frame';
      /* Move frame back the size of the support along lookAt direction
       * to prevent clipping it out through the facets.
       */
      mesh_frame.position.sub(
        lookAt
          .clone()
          .setY(0)
          .multiplyScalar(geometry_frame.parameters.radiusTop)
      );

      // Horizontal frame
      const geometry_frame2 = new THREE.CylinderGeometry(
        0.3,
        0.3,
        fullWidth - 1,
        5
      );
      geometry_frame2.rotateZ(Math.PI / 2);

      const mesh_frame2 = new THREE.Mesh(geometry_frame2, material_frame);
      mesh_frame2.castShadow = true;
      mesh_frame2.name = 'frame2';
      mesh_frame2.lookAt(lookAt);

      mesh_frame2.position.setY(this.config.pedestal_height);
      // Move frame back one unit along lookAt direction
      mesh_frame2.position.sub(
        lookAt.clone().multiplyScalar(geometry_frame2.parameters.radiusTop)
      );

      // Frame truss
      const geometry_rod = new THREE.PlaneGeometry(
        0.3,
        fullHeight - this.config.facet_height
      );
      geometry_rod.translate(this.config.facet_width / 2, 0.1, 0);
      const geometry_truss = new THREE.BufferGeometry();
      for (let i = 0; i < this.config.num_horizontal_facets; i++) {
        const copy = geometry_rod.clone();
        copy.translate(
          i * (this.config.facet_horizontal_gap + this.config.facet_width),
          0,
          0
        );
        geometry_truss.merge(copy);
      }
      // Center truss
      geometry_truss.translate(-fullWidth / 2, 0, 0);

      const mesh_truss = new THREE.Mesh(geometry_truss, material_frame);
      mesh_truss.castShadow = true;
      mesh_truss.name = 'truss';
      // Rotate, but look at opposite side, because plane is visible on one site only
      mesh_truss.lookAt(lookAt.clone().multiplyScalar(-1));
      // Height
      mesh_truss.position.setY(this.config.pedestal_height);
      // Move frame back one unit along lookAt direction
      mesh_truss.position.sub(lookAt.clone().multiplyScalar(0.1));

      group.add(mesh_frame);
      group.add(mesh_frame2);
      group.add(mesh_truss);
    }

    group.position.copy(this.position);

    this.group = group;

    return group;
  }
}
