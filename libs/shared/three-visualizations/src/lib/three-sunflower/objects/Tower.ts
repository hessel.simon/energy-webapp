export default class Tower {
  protected config;
  protected receiverConfig;
  public group;
  constructor(towerJson, receiverJson) {
    // Define properties
    this.config = towerJson;
    this.receiverConfig = receiverJson;
    this.group = null;
  }

  get receiverHeight() {
    return (
      this.config.height - this.config.dist_to_towertop - this.config.height / 2
    );
  }
}
