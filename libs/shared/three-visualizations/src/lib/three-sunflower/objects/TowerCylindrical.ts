import Tower from './Tower';
import ReceiverCylindricalCavity from './ReceiverCylindricalCavity';
import ReceiverFlat from './ReceiverFlat';
import ReceiverCylindricalExternal from './ReceiverCylindricalExternal';
import * as THREE from 'three';
import { MATERIAL_TOWER } from './materials';
const RADIAL_SEGMENTS = 20;
export default class TowerCylindrical extends Tower {
  constructor(towerconfig, receiverConfig) {
    super(towerconfig, receiverConfig);
  }

  build() {
    let mesh;
    let receiver;

    if (this.receiverConfig.type === 'flat') {
      mesh = this.buildMeshForFlat();
      receiver = new ReceiverFlat(this.receiverConfig).buildMesh();
      receiver.position.set(
        0,
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop,
        this.config.tower_rectangular.size.length / 2
      );
    } else if (this.receiverConfig.type === 'cylindrical_cavity') {
      mesh = this.buildMeshForCylindricalCavity();
      receiver = new ReceiverCylindricalCavity(this.receiverConfig).buildMesh(
        'cylindrical',
        this.config.tower_cylindrical.diameter / 2
      );

      receiver.position.set(
        0,
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop,
        0
      );
    } else if (this.receiverConfig.type === 'cylindrical_external') {
      mesh = this.buildMeshForFlat();
      receiver = new ReceiverCylindricalExternal(
        this.receiverConfig
      ).buildMesh();

      receiver.position.setY(
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop
      );
    } else {
      throw new ReferenceError(
        `Unknown receiver type '${this.receiverConfig.type}'`
      );
    }

    mesh.castShadow = true;
    receiver.castShadow = true;

    this.group = new THREE.Group();
    this.group.name = 'tower';
    this.group.add(mesh);
    this.group.add(receiver);

    return this.group;
  }

  buildMeshForFlat() {
    const geometry = new THREE.CylinderGeometry(
      this.config.tower_cylindrical.diameter / 2,
      this.config.tower_cylindrical.diameter / 2,
      this.config.height,
      RADIAL_SEGMENTS
    );
    geometry.translate(0, this.config.height / 2, 0);

    return new THREE.Mesh(geometry, MATERIAL_TOWER);
  }

  buildMeshForCylindricalCavity() {
    const towerRadius = this.config.tower_cylindrical.diameter / 2;
    const geometry_lowerTower = new THREE.CylinderGeometry(
      towerRadius,
      towerRadius,
      this.config.height -
        this.receiverConfig.dist_to_towertop -
        this.receiverConfig.height -
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
      RADIAL_SEGMENTS
    );
    const geometry_upperTower = new THREE.CylinderGeometry(
      towerRadius,
      towerRadius,
      this.receiverConfig.dist_to_towertop,
      RADIAL_SEGMENTS
    );

    // Fix height position
    geometry_lowerTower.translate(
      0,
      geometry_lowerTower.parameters.height / 2,
      0
    );
    geometry_upperTower.translate(
      0,
      geometry_upperTower.parameters.height / 2 +
        geometry_lowerTower.parameters.height +
        this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
      0
    );

    // Receiver case
    const alpha =
      (2 * Math.PI) /
      (2 *
        this.receiverConfig.cylindrical_cavity_receiver.num_panels_horizontal +
        1);
    const receiverAlpha =
      this.receiverConfig.cylindrical_cavity_receiver.num_panels_horizontal *
      alpha;
    const r =
      this.receiverConfig.cylindrical_cavity_receiver.panel_width /
      2 /
      Math.tan(alpha / 2);
    const distToCenter = Math.cos(receiverAlpha / 2) * r;
    const receiverHalfWidth = Math.sin(receiverAlpha / 2) * r;

    const beta = Math.asin(receiverHalfWidth / towerRadius);
    const betaHeight = Math.cos(beta) * towerRadius;
    const offsetZ = towerRadius - betaHeight;

    const geometry_case = new THREE.CylinderGeometry(
      towerRadius,
      towerRadius,
      this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
      RADIAL_SEGMENTS,
      1,
      true,
      beta, // Center receiver,
      2 * Math.PI - 2 * beta
    );
    geometry_case.translate(
      0,
      geometry_case.parameters.height / 2 +
        geometry_lowerTower.parameters.height,
      0
    );

    if (this.receiverConfig.cylindrical_cavity_receiver.raise_height != 0) {
      // Raise platform (copy of geometry_receiver)
      const geometry_platform = new THREE.CylinderGeometry(
        r,
        r,
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
        this.receiverConfig.cylindrical_cavity_receiver.num_panels_horizontal,
        1,
        true,
        -receiverAlpha / 2 + Math.PI, // Center receiver
        receiverAlpha
      );
      // Flip cylinder inside-out
      geometry_platform.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));
      geometry_platform.translate(
        0,
        geometry_platform.parameters.height / 2 +
          geometry_lowerTower.parameters.height,
        distToCenter + towerRadius - offsetZ
      );

      geometry_case.merge(geometry_platform);
    }

    const geometry_combined = new THREE.Geometry();
    geometry_combined.merge(geometry_lowerTower);
    geometry_combined.merge(geometry_upperTower);
    geometry_combined.merge(geometry_case);

    return new THREE.Mesh(geometry_combined, MATERIAL_TOWER);
  }
}
