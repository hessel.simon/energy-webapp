import Tower from './Tower';
import ReceiverFlat from './ReceiverFlat';
import ReceiverCylindricalCavity from './ReceiverCylindricalCavity';
import ReceiverCylindricalExternal from './ReceiverCylindricalExternal';
import * as THREE from 'three';
import { MATERIAL_TOWER } from './materials';

export default class TowerRectangular extends Tower {
  constructor(towerConfig, receiverConfig) {
    super(towerConfig, receiverConfig);
  }

  build() {
    let mesh;
    let receiver;

    if (this.receiverConfig.type === 'flat') {
      mesh = this.buildMeshForFlat();
      receiver = new ReceiverFlat(this.receiverConfig).buildMesh();
      receiver.position.set(
        0,
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop,
        this.config.tower_rectangular.size.length / 2
      );
    } else if (this.receiverConfig.type === 'cylindrical_cavity') {
      mesh = this.buildMeshForCylindricalCavity();
      receiver = new ReceiverCylindricalCavity(this.receiverConfig).buildMesh(
        'rectangular'
      );

      receiver.position.set(
        0,
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop,
        this.config.tower_rectangular.size.length / 2
      );
    } else if (this.receiverConfig.type === 'cylindrical_external') {
      mesh = this.buildMeshForFlat();
      receiver = new ReceiverCylindricalExternal(
        this.receiverConfig
      ).buildMesh();

      receiver.position.set(
        0,
        this.config.height -
          this.receiverConfig.height / 2 -
          this.receiverConfig.dist_to_towertop,
        0
      );
    } else {
      throw new ReferenceError(
        `Unknown receiver type '${this.receiverConfig.type}'`
      );
    }

    mesh.castShadow = true;
    receiver.castShadow = true;

    this.group = new THREE.Group();
    this.group.name = 'tower';
    this.group.add(mesh);
    this.group.add(receiver);

    return this.group;
  }

  buildMeshForFlat() {
    const geometry = new THREE.BoxGeometry(
      this.config.tower_rectangular.size.width,
      this.config.height,
      this.config.tower_rectangular.size.length
    );
    // Fix height, because origin is in the center of its geometry
    geometry.translate(0, this.config.height / 2, 0);

    return new THREE.Mesh(geometry, MATERIAL_TOWER);
  }

  buildMeshForCylindricalCavity() {
    const geometry_lowerTower = new THREE.BoxGeometry(
      this.config.tower_rectangular.size.width,
      this.config.height -
        this.receiverConfig.dist_to_towertop -
        this.receiverConfig.height -
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
      this.config.tower_rectangular.size.length
    );
    const geometry_upperTowser = new THREE.BoxGeometry(
      this.config.tower_rectangular.size.width,
      this.receiverConfig.dist_to_towertop,
      this.config.tower_rectangular.size.length
    );

    // Fix height position
    geometry_lowerTower.translate(
      0,
      geometry_lowerTower.parameters.height / 2,
      0
    );
    geometry_upperTowser.translate(
      0,
      geometry_upperTowser.parameters.height / 2 +
        geometry_lowerTower.parameters.height +
        this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
      0
    );

    // Receiver case
    const halfDegree =
      Math.PI /
      this.receiverConfig.cylindrical_cavity_receiver.num_panels_horizontal /
      2;
    const r =
      this.receiverConfig.cylindrical_cavity_receiver.panel_width /
      2 /
      Math.tan(halfDegree);
    const width = this.config.tower_rectangular.size.width;
    const length = this.config.tower_rectangular.size.length;
    const geometry_case = new THREE.PlaneGeometry(
      width / 2 - r,
      this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height
    );
    geometry_case.translate(
      -geometry_case.parameters.width / 2 - r,
      geometry_case.parameters.height / 2,
      length / 2
    );

    const geometry_plane1 = new THREE.PlaneGeometry(
      width / 2 - r,
      this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height
    );
    geometry_plane1.translate(
      geometry_case.parameters.width / 2 + r,
      geometry_case.parameters.height / 2,
      length / 2
    );

    const geometry_plane_back = new THREE.PlaneGeometry(
      width,
      this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height
    );
    geometry_plane_back.rotateY(Math.PI);
    geometry_plane_back.translate(
      0,
      geometry_plane_back.parameters.height / 2,
      -length / 2
    );

    const geometry_plane_left = new THREE.PlaneGeometry(
      length,
      this.receiverConfig.height +
        this.receiverConfig.cylindrical_cavity_receiver.raise_height
    );
    geometry_plane_left.rotateY(-Math.PI / 2);
    geometry_plane_left.translate(
      -width / 2,
      geometry_plane_left.parameters.height / 2,
      0
    );

    const geometry_plane_right = geometry_plane_left.clone();
    geometry_plane_right.rotateY(Math.PI);

    // Merge all together
    geometry_case.merge(geometry_plane1);
    geometry_case.merge(geometry_plane_back);
    geometry_case.merge(geometry_plane_left);
    geometry_case.merge(geometry_plane_right);

    if (this.receiverConfig.cylindrical_cavity_receiver.raise_height != 0) {
      // Raise platform (copy of geometry_receiver)
      const geometry_platform = new THREE.CylinderGeometry(
        r,
        r,
        this.receiverConfig.cylindrical_cavity_receiver.raise_height,
        this.receiverConfig.cylindrical_cavity_receiver.num_panels_horizontal,
        1,
        true,
        Math.PI / 2,
        Math.PI
      );
      // Flip cylinder inside-out
      geometry_platform.applyMatrix(new THREE.Matrix4().makeScale(-1, 1, 1));
      geometry_platform.translate(
        0,
        geometry_platform.parameters.height / 2,
        this.config.tower_rectangular.size.length / 2
      );

      geometry_case.merge(geometry_platform);
    }

    geometry_case.translate(0, geometry_lowerTower.parameters.height, 0);

    const geometry_combined = new THREE.Geometry();
    geometry_combined.merge(geometry_lowerTower);
    geometry_combined.merge(geometry_upperTowser);
    geometry_combined.merge(geometry_case);

    return new THREE.Mesh(geometry_combined, MATERIAL_TOWER);
  }
}
