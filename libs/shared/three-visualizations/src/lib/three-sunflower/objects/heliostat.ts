import { Texture, Vector3 } from 'three';
import * as THREE from 'three';

export default class Heliostat {
  protected config;
  protected position: Vector3;
  protected sunDirection: Vector3;
  protected towerPosition: Vector3;
  public skyboxCubemap: Texture;
  public group: THREE.Group | null;
  constructor(config: any, position = new THREE.Vector3()) {
    // Define properties
    this.config = config;
    this.position = position;

    this.sunDirection = new Vector3();
    this.towerPosition = new Vector3();
    this.skyboxCubemap = new Texture();
    this.group = null;
  }

  get lookAtVector() {
    const vector_toSun = this.sunDirection.normalize();
    const vector_toTower = new Vector3()
      .subVectors(this.towerPosition, this.position)
      .normalize();
    return new Vector3().addVectors(vector_toSun, vector_toTower).normalize();
  }
}
