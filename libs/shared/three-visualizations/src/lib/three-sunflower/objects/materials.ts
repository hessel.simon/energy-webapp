import * as THREE from 'three';

export const MATERIAL_FACET = new THREE.MeshStandardMaterial({
  color: 0x4781ff,
  envMapIntensity: 1,
  roughness: 0.1,
  metalness: 1,
});

export const MATERIAL_TOWER = new THREE.MeshStandardMaterial({
  map: new THREE.TextureLoader().load('./assets/textures/concrete.jpg'),
  roughness: 0.9,
});

export const MATERIAL_RECEIVER = new THREE.MeshStandardMaterial({
  color: 0xffffff,
  flatShading: true,
  emissive: 0x999999,
});
