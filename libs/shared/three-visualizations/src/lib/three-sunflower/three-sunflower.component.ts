import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { WEBGL } from 'three/examples/jsm/WebGL';
import { ThreeSunflowerService } from './three-sunflower.service';

@Component({
  selector: 'energy-platform-three-sunflower',
  templateUrl: './three-sunflower.component.html',
})
export class ThreeSunflowerComponent implements OnInit {
  @Input() rotationX: number | undefined;
  @Input() rotationY: number | undefined;
  @Input() rotationZ: number | undefined;
  @Input() positionX: number | undefined;
  @Input() positionY: number | undefined;
  @Input() positionZ: number | undefined;
  @Input() scaleX: number | undefined;
  @Input() scaleY: number | undefined;
  @Input() scaleZ: number | undefined;
  @Input() alpha: number | undefined;
  @Input() wireframe: boolean | undefined;

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement> | undefined;

  constructor(private threeSunflowerServ: ThreeSunflowerService) {}

  ngOnInit() {
    if (WEBGL.isWebGLAvailable() && this.rendererCanvas) {
      this.threeSunflowerServ.initialize(
        this.rendererCanvas,
        [-6.144722222, 37.25972222]
      );
      this.threeSunflowerServ.animate();
      this.threeSunflowerServ.setupWorld();
      this.threeSunflowerServ.generateTerrain();
      this.threeSunflowerServ.addObjects();
    } else {
      const warning = WEBGL.getWebGLErrorMessage();
      warning.style.width = '80%';
      const rendererWrapper = document.getElementById('rendererWrapper');
      if (rendererWrapper) {
        rendererWrapper.appendChild(warning);
      }
    }
  }

  onResize() {
    this.threeSunflowerServ.resize();
  }
}
