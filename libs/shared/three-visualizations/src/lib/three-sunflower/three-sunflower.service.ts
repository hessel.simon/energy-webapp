import { ElementRef, Injectable, NgZone, OnDestroy } from '@angular/core';
import * as THREE from 'three';
import { ThreeRendererBaseService } from '../three-renderer-base/three-renderer-base.service';
import HeliostatGroup from './objects/HeliostatGroup';

@Injectable({
  providedIn: 'root',
})
export class ThreeSunflowerService
  extends ThreeRendererBaseService
  implements OnDestroy
{
  constructor(protected ngZone: NgZone) {
    super(ngZone);
  }

  ngOnDestroy() {
    if (this.frameId != null) {
      cancelAnimationFrame(this.frameId);
    }
    super.ngOnDestroy();
  }

  addObjects() {
    const hg = new HeliostatGroup({
      heliostat_type_name: 'PS10 Heliostat',
      facet_surface_form: 'stepwise_focused',
      focal_length: [145, 160, 190, 210, 265, 290, 355, 395, 520, 700],
      heliostat_shape: 'polygone',
      facet_width: 3.21,
      facet_height: 1.35,
      num_horizontal_facets: 4,
      num_vertical_facets: 7,
      facet_horizontal_gap: 0.05,
      facet_vertical_gap: 0.05,
      canting: 'off_axis_angles',
      off_axis_reference_angles: {
        azimuth: 178.4,
        altitude: 12.3,
      },
      pedestal_height: 8.17,
      cluster_pattern: 'single',
      aiming_strategy: 'flux_map',
      heliostat_num_corners: 5,
      facet_outer_side_length: 5,
      facet_gap: 0.1,
    });
    this.scene.add(hg.build());
    if (hg.mesh) {
      hg.mesh.position.set(this.centerCoords[0], 0.5, this.centerCoords[2]);
      if (this.terrainGenerator) {
        this.terrainGenerator.waitForHeight(
          [this.centerCoords[0], this.centerCoords[2]],
          (mesh: THREE.Mesh) => {
            if (hg.mesh) {
              const raycaster = new THREE.Raycaster();
              raycaster.set(
                hg.mesh.position.clone().setY(11000),
                new THREE.Vector3(0, -1, 0)
              );
              const intersect = raycaster.intersectObject(mesh);
              hg.mesh.position.setY(intersect[0].point.y);
            }
          }
        );
      }
    }
  }

  initialize(
    canvas: ElementRef<HTMLCanvasElement>,
    center: number[] = [0, 0]
  ): void {
    super.initialize(canvas, center);
    // Add skybox
    {
      this.scene.background = new THREE.CubeTextureLoader().load([
        './assets/textures/skybox_xpos.png',
        './assets/textures/skybox_xneg.png',
        './assets/textures/skybox_ypos.png',
        './assets/textures/skybox_yneg.png',
        './assets/textures/skybox_zpos.png',
        './assets/textures/skybox_zneg.png',
      ]);
    }

    /*let h = new RectangularHeliostat({
        "heliostat_type_name": "PS10 Heliostat",
        "facet_surface_form": "stepwise_focused",
        "focal_length": [
          145,
          160,
          190,
          210,
          265,
          290,
          355,
          395,
          520,
          700
        ],
        "heliostat_shape": "polygone",
        "facet_width": 3.21,
        "facet_height": 1.35,
        "num_horizontal_facets": 4,
        "num_vertical_facets": 7,
        "facet_horizontal_gap": 0.05,
        "facet_vertical_gap": 0.05,
        "canting": "off_axis_angles",
        "off_axis_reference_angles": {
          "azimuth": 178.4,
          "altitude": 12.3
        },
        "pedestal_height": 5.17,
        "cluster_pattern": "single",
        "aiming_strategy": "flux_map",
        "heliostat_num_corners": 5,
        "facet_outer_side_length": 5,
        "facet_gap": 0.1
      }
      ,new Vector3(this.centerCoords[0],0.05,this.centerCoords[2]))
    this.logger.info(slashify(this.terrainGenerator.coordsToTile(this.centerCoords[0],this.centerCoords[2],12)))
    this.terrainGenerator.waitForTile(slashify(this.terrainGenerator.coordsToTile(this.centerCoords[0],this.centerCoords[2],12)), (mesh) => {
      this.logger.info("test")
      var raycaster = new THREE.Raycaster();
      raycaster.set(h.group.position.clone().setY(11000), new THREE.Vector3(0,-1,0));
      var intersect = raycaster.intersectObject(mesh);
      this.logger.info(intersect[0].point.y)
        //h.group.position.setY(intersect[0].point.y >= 0 ? intersect[0].point.y : 0)
    })
    h.skyboxCubemap = this.scene.background;
    h.build()
    h.group.scale.setScalar(10/this.terrainGenerator.heightToPixels(this.centerCoords[2]));
    h.group.position.setY(0.05)
    h.updateOrientation(new Vector3(0,1,0), new Vector3(0,1,0))
    this.scene.add(h.group)
    // @ts-ignore
    window.h = h;

    let t = new TowerRectangular({
        "height": 100,
        "type": "recangular",
        "tower_cylindrical": {
          "diameter": 10
        },
        "tower_rectangular": {
          "size": {
            "width": 10,
            "length": 10
          }
        }
      },{
        "type":"cylindrical_cavity",
        "flat_receiver":{
          "width":13.78,
          "tilt_angle":11.5,
          "orientation_angle":0.0
        },
        "cylindrical_external_receiver": {
          "panel_width": 10,
          "num_panels_horizontal": 5
        },
        "cylindrical_cavity_receiver": {
          "panel_width": 2,
          "num_panels_horizontal": 5,
          "raise_height": 2
        },
        "height":12.0,
        "dist_to_towertop":2.74
      }
    )
    // @ts-ignore
    window.t = t;
    this.scene.add(t.build())
    t.group.position.set(this.centerCoords[0],0.05,this.centerCoords[2]+10)
    t.group.scale.setScalar(4/this.terrainGenerator.heightToPixels(this.centerCoords[2]));
    t.group.rotation.y = Math.PI;*/

    this.initialized = true;
  }
}
