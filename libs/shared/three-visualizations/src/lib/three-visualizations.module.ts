import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThreeCubeComponent } from './three-cube/three-cube.component';
import { ThreeSunflowerComponent } from './three-sunflower/three-sunflower.component';
import { ThreeWindflowerComponent } from './three-windflower/three-windflower.component';
import { ThreeWindflowerPreviewComponent } from './three-windflower/three-windflower-preview.component';
import { ThreeWindflowerService } from './three-windflower/three-windflower.service';
import { ThreeCubeService } from './three-cube/three-cube.service';
import { ThreeRendererBaseService } from './three-renderer-base/three-renderer-base.service';
import { ThreeSunflowerService } from './three-sunflower/three-sunflower.service';

const components = [
  ThreeCubeComponent,
  ThreeSunflowerComponent,
  ThreeWindflowerComponent,
  ThreeWindflowerPreviewComponent,
];

@NgModule({
  declarations: components,
  exports: components,
  providers: [
    ThreeWindflowerService,
    ThreeCubeService,
    ThreeRendererBaseService,
    ThreeSunflowerService,
  ],
  imports: [CommonModule],
})
export class ThreeVisualizationModule {}
