import * as THREE from 'three';
import { OBJLoader } from 'three/examples/jsm/loaders/OBJLoader.js';
import { MTLLoader } from 'three/examples/jsm/loaders/MTLLoader.js';

export async function loadModels(paths: string[]): Promise<THREE.Mesh[]> {
  const promises: Promise<THREE.Mesh>[] = [];

  for (const path of paths) {
    promises.push(
      new Promise((resolve, reject) => {
        const mtlLoader = new MTLLoader();
        const objLoader = new OBJLoader();

        mtlLoader.load(
          path + '.mtl',
          (materialCreator) => {
            materialCreator.preload();
            objLoader.setMaterials(materialCreator);
            objLoader.load(
              path + '.obj',
              /* (object) => {
                resolve(object);
              }, */
              (error: any) => reject(error)
            );
          },
          (error) => reject(error)
        );
      })
    );
  }

  return Promise.all(promises);
}
