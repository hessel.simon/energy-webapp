import {
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { WEBGL } from 'three/examples/jsm/WebGL';
import { ThreeWindflowerService } from './three-windflower.service';

@Component({
  selector: 'energy-platform-three-windflower-preview',
  template: `
    <div
      id="rendererWrapper"
      style="width: 100%; height: 100%; position: relative;"
      (resized)="this.windflower.resize()"
    >
      <canvas
        #rendererCanvas
        id="rendererCanvas"
        style="width: 100%; height: 100%; position: relative;"
      ></canvas>
    </div>
  `,
  providers: [ThreeWindflowerService],
})
export class ThreeWindflowerPreviewComponent implements OnInit, OnChanges {
  @Input() hubHeight: number | undefined = 64.5;
  @Input() rotorDiameter: number | undefined = 80.0;
  @Input() debug = false;

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement> | undefined;

  constructor(public windflower: ThreeWindflowerService) {}

  ngOnInit() {
    if (WEBGL.isWebGLAvailable() && this.rendererCanvas) {
      this.windflower.initialize(this.rendererCanvas);
      this.windflower.preview = true;
      this.windflower.animate();

      // Add windmills
      if (this.hubHeight) {
        this.windflower.windmill.hubHeight = this.hubHeight;
      }
      if (this.rotorDiameter) {
        this.windflower.windmill.rotorDiameter = this.rotorDiameter;
      }
      this.windflower.addWindmills([[0, 0, 0]]);

      // Adjust camera
      if (this.windflower.camera) {
        this.windflower.camera.near = 1 / 99;
      }

      // Adjust controls
      if (this.windflower.controls) {
        this.windflower.controls.enablePan = false;
        this.windflower.controls.maxDistance = 2;
      }

      // Display debug information
      if (this.debug) {
        // Add stats panel showing FPS
        const stats = this.windflower.stats;
        stats.showPanel(0);
        stats.dom.style.position = 'absolute';
        stats.dom.style.top = '10px';
        stats.dom.style.left = '10px';
        const rendererWrapper = document.getElementById('rendererWrapper');
        if (rendererWrapper) {
          rendererWrapper.appendChild(stats.dom);
        }

        this.windflower.addDebugHelpers();
      }
    } else {
      const warning = WEBGL.getWebGLErrorMessage();
      warning.style.width = '80%';
      const rendererWrapper = document.getElementById('rendererWrapper');
      if (rendererWrapper) {
        rendererWrapper.appendChild(warning);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes['hubHeight'] || changes['rotorDiameter']) {
      if (this.hubHeight) {
        this.windflower.windmill.hubHeight = this.hubHeight;
      }
      if (this.rotorDiameter) {
        this.windflower.windmill.rotorDiameter = this.rotorDiameter;
      }
      this.windflower.updateWindmills();
    }
  }
}
