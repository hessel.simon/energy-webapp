import {
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { WEBGL } from 'three/examples/jsm/WebGL';
import { ThreeWindflowerService } from './three-windflower.service';

@Component({
  selector: 'energy-platform-three-windflower',
  template: `
    <div
      id="rendererWrapper"
      style="width: 100%; height: 100%; position: relative;"
      (resized)="this.windflower.resize()"
    >
      <canvas #rendererCanvas id="rendererCanvas" style="width"></canvas>
    </div>
  `,
  providers: [ThreeWindflowerService],
})
export class ThreeWindflowerComponent implements OnInit, OnChanges {
  @Input() center: number[] = [0, 0];
  @Input() positions: number[][] | undefined;
  @Input() fieldBoundaries: number[][] | undefined;
  @Input() restrictedAreas: number[][][] | undefined;
  @Input() hubHeight = 64.5;
  @Input() rotorDiameter = 80.0;
  @Input() rotation = 0.0;
  @Input() debug = false;

  @ViewChild('rendererCanvas', { static: true })
  public rendererCanvas: ElementRef<HTMLCanvasElement> | undefined;

  constructor(public windflower: ThreeWindflowerService) {}

  ngOnInit() {
    if (WEBGL.isWebGLAvailable()) {
      if (this.rendererCanvas) {
        this.windflower.initialize(this.rendererCanvas, this.center);
        this.windflower.animate();
      }

      // Important: Coordinates should be in lon/lat

      // Add terrain
      if (this.fieldBoundaries) {
        this.windflower.setupWorld(this.fieldBoundaries);
        this.windflower.generateTerrain();
      }

      // Add windmills
      if (this.positions) {
        this.windflower.windmill.hubHeight = this.hubHeight;
        this.windflower.windmill.rotorDiameter = this.rotorDiameter;
        this.windflower.addWindmills(this.positions);
      }

      // Add area outlines
      if (this.fieldBoundaries && this.restrictedAreas) {
        this.windflower.addAreaOutline(this.fieldBoundaries, 0x00ff00);
        for (const restrictedArea of this.restrictedAreas) {
          this.windflower.addAreaOutline(restrictedArea, 0xff0000);
        }
      }

      // Display debug information
      if (this.debug) {
        // Add stats panel showing FPS
        const stats = this.windflower.stats;
        stats.showPanel(0);
        stats.dom.style.position = 'absolute';
        stats.dom.style.top = '10px';
        stats.dom.style.left = '10px';
        const rendererWrapper = document.getElementById('rendererWrapper');
        if (rendererWrapper) {
          rendererWrapper.appendChild(stats.dom);
        }

        this.windflower.addDebugHelpers();
      }
    } else {
      const warning = WEBGL.getWebGLErrorMessage();
      warning.style.width = '80%';
      const rendererWrapper = document.getElementById('rendererWrapper');
      if (rendererWrapper) {
        rendererWrapper.appendChild(warning);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['hubHeight'] ||
      changes['rotorDiameter'] ||
      changes['rotation']
    ) {
      this.windflower.windmill.hubHeight = this.hubHeight;
      this.windflower.windmill.rotorDiameter = this.rotorDiameter;
      this.windflower.windmill.rotation = this.rotation;
      this.windflower.updateWindmills();
    }
  }
}
