import { ElementRef, Injectable, NgZone, OnDestroy } from '@angular/core';
import 'stats.js';
import * as THREE from 'three';
import { InstancedMesh } from 'three';
import { Water } from 'three/examples/jsm/objects/Water';
import { ThreeRendererBaseService } from '../three-renderer-base/three-renderer-base.service';
import * as AssetLoader from './asset-loader';

@Injectable({
  providedIn: 'root',
})
export class ThreeWindflowerService
  extends ThreeRendererBaseService
  implements OnDestroy
{
  windmill: {
    mesh: THREE.Group;
    instances: THREE.Object3D[];
    boundingBox: THREE.Box3;
    hubHeight: number;
    rotorDiameter: number;
    rotation: number;
  } = {
    mesh: new THREE.Group(),
    instances: [],
    boundingBox: new THREE.Box3(),
    hubHeight: 64.5,
    rotorDiameter: 80.0,
    rotation: 0.0,
  };

  water:
    | {
        mesh: Water;
        material: THREE.ShaderMaterial;
      }
    | undefined;

  constructor(protected ngZone: NgZone) {
    super(ngZone);
  }

  ngOnDestroy() {
    if (this.frameId != undefined) {
      cancelAnimationFrame(this.frameId);
    }

    if (!this.initialized) {
      return;
    }
    super.ngOnDestroy();
  }

  initialize(
    canvas: ElementRef<HTMLCanvasElement>,
    center: number[] = [0, 0]
  ): void {
    super.initialize(canvas, center);

    // Add water
    {
      const directionalLight = this.scene.getObjectByName('lights.directional');

      const normals = new THREE.TextureLoader().load(
        '/assets/textures/waternormals.jpg',
        (texture) => {
          texture.wrapS = THREE.RepeatWrapping;
          texture.wrapT = THREE.RepeatWrapping;
        }
      );

      if (directionalLight) {
        const mesh = new Water(new THREE.PlaneBufferGeometry(1, 1, 1, 1), {
          textureWidth: 512,
          textureHeight: 512,
          waterNormals: normals,
          alpha: 0.8, // alpha interferes with terrain tiles
          sunDirection: directionalLight.position.clone().normalize(),
          sunColor: 0xffffff,
          waterColor: 0x00284d,
          distortionScale: 3.7,
        });
        mesh.name = 'water.mesh';
        mesh.receiveShadow = true;
        mesh.rotation.x = -Math.PI / 2;
        this.water = {
          mesh: mesh,
          material: mesh.material,
        };
        if (this.preview) {
          mesh.geometry.scale(20, 20, 1);
          this.water.material.uniforms['size'].value = 50; // sets the animation speed of the water
        } else {
          mesh.geometry.scale(2000, 2000, 1);
          this.water.material.uniforms['size'].value = 10; // sets the animation speed of the water
        }

        this.scene.add(mesh);
      }
    }

    // Add skybox
    {
      this.scene.background = new THREE.CubeTextureLoader().load([
        './assets/textures/skybox_xpos.png',
        './assets/textures/skybox_xneg.png',
        './assets/textures/skybox_ypos.png',
        './assets/textures/skybox_yneg.png',
        './assets/textures/skybox_zpos.png',
        './assets/textures/skybox_zneg.png',
      ]);
    }

    this.initialized = true;
  }

  // center in longitude, latitude
  setupWorld(fieldBoundaries: number[][]) {
    super.setupWorld(fieldBoundaries);
    // set water slightly below 0 to avoid artifacts
    if (this.water) {
      this.water.mesh.position.set(
        this.centerCoords[0],
        -0.1,
        this.centerCoords[2]
      );
    }
  }

  async addWindmills(positions: number[][]) {
    // Load mesh if necessary
    if (this.windmill.mesh == undefined) {
      this.windmill.mesh = await this.getOrLoadWindmill();
      this.scene.add(this.windmill.mesh);
    }
    // Add new instances at positions
    for (let position of positions) {
      // throw away elevation
      if (position.length == 3) position = [position[0], position[2]];
      if (this.terrainGenerator) {
        const instance = this.createInstance();

        const positionScene = this.terrainGenerator.geodeticToLocal([
          position[0],
          position[1],
        ]);

        instance.position.set(positionScene[0], 0, positionScene[2]);
        // Set meter-scale
        instance.scale.setScalar(
          1 / this.terrainGenerator.heightToPixels(position[1])
        );

        if (!this.preview) {
          instance.visible = false;
          this.terrainGenerator.waitForHeight(position, (mesh: THREE.Mesh) => {
            const raycaster = new THREE.Raycaster();
            raycaster.set(
              instance.position.clone().setY(11000),
              new THREE.Vector3(0, -1, 0)
            );
            const intersect = raycaster.intersectObject(mesh);
            instance.position.setY(
              intersect[0].point.y >= 0 ? intersect[0].point.y : 0
            );
            instance.visible = true;
          });
        }
        this.windmill.instances.push(instance);
      }
    }

    this.updateBoundingBox();
  }

  async getOrLoadWindmill() {
    let result = this.windmill.mesh;

    // Mesh already loaded
    if (result != undefined) {
      return result;
    }

    // Load it otherwise
    result = new THREE.Group();

    const parts = await AssetLoader.loadModels([
      '/assets/objects/windmill_tower',
      '/assets/objects/windmill_turbine',
      '/assets/objects/windmill_blade',
    ]);
    const maxCount = 10000;
    const maxInstances = [maxCount, maxCount, 3 * maxCount];
    const names = ['tower', 'turbine', 'blade'];

    for (const [index, part] of parts.entries()) {
      /* const mesh = part.children[0]; */

      const instancedMesh = new THREE.InstancedMesh(
        part.geometry,
        part.material,
        maxInstances[index]
      );
      instancedMesh.name = 'windmill.' + names[index] + '.mesh';
      instancedMesh.count = 0;
      instancedMesh.castShadow = true;
      instancedMesh.frustumCulled = false;

      result.add(instancedMesh);
    }

    return result;
  }

  updateWindmills() {
    const towerScale = this.windmill.hubHeight / 64.5;
    const rotorScale = this.windmill.rotorDiameter / 80;

    for (const instance of this.windmill.instances) {
      // Windmill
      instance.rotation.set(0, (this.windmill.rotation * Math.PI) / 180, 0);

      // Tower
      const tower = instance.getObjectByName('windmill.tower');
      if (tower) {
        tower.scale.set(towerScale, towerScale, towerScale);
      }

      // Turbine
      const turbine = instance.getObjectByName('windmill.turbine');
      if (turbine) {
        turbine.position.setY(this.windmill.hubHeight - 1.5 * towerScale);
        turbine.scale.set(towerScale, towerScale, towerScale);
      }

      // Rotor
      const rotor = instance.getObjectByName('windmill.rotor.group');
      if (rotor) {
        rotor.position.set(0, this.windmill.hubHeight, 7 * towerScale);
        rotor.scale.set(rotorScale, rotorScale, rotorScale);
      }
    }

    this.updateBoundingBox();
  }

  updateInstances() {
    // update water
    const delta = this.clock.getDelta();

    // Animate water
    if (this.water) {
      this.water.material.uniforms['time'].value += 0.5 * delta;
    }

    // Animate windmill rotors
    for (const instance of this.windmill.instances) {
      const rotor = instance.getObjectByName('windmill.rotor.group');
      if (rotor) {
        rotor.rotation.y += 1.0 * delta;
      }
    }

    const mesh = this.windmill.mesh;
    const count = this.windmill.instances.length;

    if (mesh == undefined || count == 0) {
      return;
    }

    //Temporary Fix
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const towerMesh = mesh.getObjectByName(
      'windmill.tower.mesh'
    ) as InstancedMesh;
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const turbineMesh = mesh.getObjectByName(
      'windmill.turbine.mesh'
    ) as InstancedMesh;
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    const bladeMesh = mesh.getObjectByName(
      'windmill.blade.mesh'
    ) as InstancedMesh;

    if (towerMesh) {
      towerMesh.count = count;
    }
    if (turbineMesh) {
      turbineMesh.count = count;
    }
    if (bladeMesh) {
      bladeMesh.count = 3 * count;
    }

    // Update all instance matrices
    for (const [instanceId, instance] of this.windmill.instances.entries()) {
      // Probably not required to always update all Object3D matrices manually
      if (instance.matrixWorldNeedsUpdate) instance.updateMatrix();

      // Tower
      {
        const tower = instance.getObjectByName('windmill.tower');
        if (tower && towerMesh && tower.matrixWorldNeedsUpdate) {
          tower.updateMatrix();
          towerMesh.setMatrixAt(
            instanceId,
            instance.matrix.clone().multiply(tower.matrix)
          );
        }
      }

      // Turbine
      {
        const turbine = instance.getObjectByName('windmill.turbine');
        if (turbine && turbineMesh && turbine.matrixWorldNeedsUpdate) {
          turbine.updateMatrix();
          turbineMesh.setMatrixAt(
            instanceId,
            instance.matrix.clone().multiply(turbine.matrix)
          );
        }
      }

      // Rotor
      {
        const rotor = instance.getObjectByName('windmill.rotor.group');
        if (rotor) {
          rotor.updateMatrix();

          // Blades
          for (const [bladeId, blade] of rotor.children.entries()) {
            if (!blade.matrixWorldNeedsUpdate) continue;
            blade.updateMatrix();
            if (bladeMesh) {
              bladeMesh.setMatrixAt(
                3 * instanceId + bladeId,
                instance.matrix
                  .clone()
                  .multiply(rotor.matrix)
                  .multiply(blade.matrix)
              );
            }
          }
        }
      }
    }

    if (towerMesh) {
      towerMesh.instanceMatrix.needsUpdate = true;
    }
    if (turbineMesh) {
      turbineMesh.instanceMatrix.needsUpdate = true;
    }
    if (bladeMesh) {
      bladeMesh.instanceMatrix.needsUpdate = true;
    }
  }

  createInstance() {
    const result = new THREE.Group();
    result.name = 'windmill.group';
    result.rotation.set(0, (this.windmill.rotation * Math.PI) / 180, 0);
    result.scale.setScalar(0.005);
    const towerScale = this.windmill.hubHeight / 64.5;
    const rotorScale = this.windmill.rotorDiameter / 80;
    result.matrixWorldNeedsUpdate = true;
    // Tower
    {
      const tower = new THREE.Object3D();
      tower.name = 'windmill.tower';
      tower.scale.set(towerScale, towerScale, towerScale);
      tower.matrixWorldNeedsUpdate = true;
      result.add(tower);
    }

    // Turbine
    {
      const turbine = new THREE.Object3D();
      turbine.name = 'windmill.turbine';
      turbine.position.setY(this.windmill.hubHeight - 1.5 * towerScale);
      turbine.scale.set(towerScale, towerScale, towerScale);
      turbine.matrixWorldNeedsUpdate = true;
      result.add(turbine);
    }

    // Rotor
    {
      const rotor = new THREE.Group();
      rotor.name = 'windmill.rotor.group';
      rotor.rotation.set(-Math.PI / 2, (Math.random() * 180) / Math.PI, 0);
      rotor.position.set(0, this.windmill.hubHeight, 7 * towerScale);
      rotor.scale.set(rotorScale, rotorScale, rotorScale);

      // Blades
      for (let i = 0; i < 3; i++) {
        const blade = new THREE.Object3D();
        blade.name = 'windmill.blade';
        blade.rotateY(((i * 120) / 180) * Math.PI);
        blade.matrixWorldNeedsUpdate = true;
        rotor.add(blade);
      }

      result.add(rotor);
    }

    return result;
  }

  calculateBoundingBox() {
    const boundingBox = super.calculateBoundingBox();
    // Note: Since no Mesh is associated directly with the Object3D instances,
    // we have to use expandByPoint() to compute the bounding box manually

    // Expand box to fit positions
    for (const instance of this.windmill.instances) {
      boundingBox.expandByPoint(instance.position);
    }

    // Expand box to fit windmills
    const h = this.windmill.hubHeight;
    const r = 0.5 * this.windmill.rotorDiameter;
    boundingBox.expandByPoint(
      new THREE.Vector3(boundingBox.min.x, 1, boundingBox.max.z)
    );
    boundingBox.expandByVector(new THREE.Vector3(1, 0, 1));

    return boundingBox;
  }
}
