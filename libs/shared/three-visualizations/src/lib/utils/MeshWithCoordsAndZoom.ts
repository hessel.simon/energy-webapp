export type MeshWithCoordsAndZoom = THREE.Mesh & {
  coords: string;
  zoom: number | undefined;
};
