import { keys } from './object.util';

export enum LogLevel {
  log = 1,
  info = 1,
  debug = 1,
  trace = 1,
  warn = 2,
  error = 3,
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Logger = Record<keyof typeof LogLevel, (...args: any[]) => void>;

export const levels = keys(LogLevel).filter((e) => isNaN(parseInt(e, 10)));

export const generateLogger = (name: string) =>
  levels.reduce(
    (acc: Logger, level) => ({
      ...acc,
      [level.toLocaleLowerCase()]: console[level].bind(console, `[${name}]: `),
    }),
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    {} as Logger
  );

export class BaseStructure {
  logger: Logger;
  constructor() {
    this.logger = generateLogger(this.constructor.name);
  }
}
