export type Truthy<T> = T extends false | '' | null | 0 | undefined ? never : T;

export const isTruthy = <T>(value: T): value is Truthy<T> => Boolean(value);
