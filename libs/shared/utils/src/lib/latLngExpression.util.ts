import { LatLngExpression, LatLngTuple } from 'leaflet';

type LatLngExpressionDTOArg = 'lat' | 'lng';

interface LatLngExpressionDTO {
  get: (option: LatLngExpressionDTOArg) => number;
  update: (option: LatLngExpressionDTOArg, value: number) => void;
}

export const latLngExpressionToProxy = (
  input: LatLngExpression
): LatLngExpressionDTO => {
  if (Array.isArray(input))
    return {
      get: (option) => input[option === 'lng' ? 0 : 1],
      update: (option, value) => (input[option === 'lng' ? 0 : 1] = value),
    };
  return {
    get: (option) => input[option],
    update: (option, value) => (input[option] = value),
  };
};

export const latLngExpressionToLatLngTuple = (
  input: LatLngExpression
): LatLngTuple => {
  if (Array.isArray(input)) return input;
  return [input.lng, input.lat];
};
