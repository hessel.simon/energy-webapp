export type Keys<O extends Record<string | number | symbol, unknown>> = Array<
  keyof O
>;

export const keys = <O extends Record<string | number | symbol, unknown>>(
  o: O
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
) => Object.keys(o) as Keys<O>;

type Entries<T> = {
  [K in keyof T]: [K, T[K]];
}[keyof T][];

export const entries = <T extends Record<string | number | symbol, unknown>>(
  o: T
) =>
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  Object.entries(o) as Entries<T>;
