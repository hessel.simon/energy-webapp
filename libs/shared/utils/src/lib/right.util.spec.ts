import { Right } from '@energy-platform/shared/interfaces';
import { compareRights, getLowerRight } from './right.util';

describe('CompareRightsUtil', () => {
  it('current Right is higher than Right to compare', () => {
    const result = compareRights(Right.WRITE, Right.READ);
    expect(result).toBeGreaterThan(0);
  });
  it('current Right is lower than Right to compare', () => {
    const result = compareRights(Right.READ, Right.WRITE);
    expect(result).toBeLessThan(0);
  });
  it('current Right is equal than Right to compare', () => {
    const result = compareRights(Right.WRITE, Right.WRITE);
    expect(result).toEqual(0);
  });
  it('GetLowerRight:READ', () => {
    expect(() => getLowerRight(Right.READ)).toThrowError();
  });
  it('GetLowerRight:WRITE', () => {
    const result = getLowerRight(Right.WRITE);
    expect(result).toEqual(Right.READ);
  });
  it('GetLowerRight:MANAGE', () => {
    const result = getLowerRight(Right.MANAGE);
    expect(result).toEqual(Right.WRITE);
  });
  it('GetLowerRight:OWN', () => {
    const result = getLowerRight(Right.OWN);
    expect(result).toEqual(Right.WRITE);
  });
});
