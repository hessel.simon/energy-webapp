import { Right } from '@energy-platform/shared/interfaces';

const RightToNumberMapper: Record<Right, 1 | 2 | 3> = {
  [Right.READ]: 1,
  [Right.WRITE]: 2,
  [Right.MANAGE]: 3,
  [Right.OWN]: 3,
};

const RightOrder: Right[] = [Right.READ, Right.WRITE, Right.MANAGE];

export const compareRights = (currentRight: Right, rightToCompare: Right) =>
  RightToNumberMapper[currentRight] - RightToNumberMapper[rightToCompare];

export const getLowerRight = (currentRight: Right) => {
  if (currentRight === Right.READ)
    throw new Error('Right cant be lower then Read');
  else return RightOrder[RightToNumberMapper[currentRight] - 2];
};
