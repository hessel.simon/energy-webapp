import { Component, Input } from '@angular/core';
import { ScaleType } from '@swimlane/ngx-charts';
import { BarChartModel } from '../stacked-bar-chart/stacked-bar-chart.component';

@Component({
  selector: 'energy-platform-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.scss'],
})
export class BarChartComponent {
  @Input()
  public values: BarChartModel[] = [];
  title = 'Angular Charts';

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = '';
  showYAxisLabel = true;
  @Input()
  yAxisLabel = 'Costs';
  timeline = true;
  showDataLabel = true;

  colorScheme = {
    name: '',
    selectable: false,
    group: ScaleType.Linear,
    domain: ['#003b49', '#004e5e', '#266f7d', '#4d8894', '#80aab3', '#b3ccd1'],
    //domain: ["#003b49", "#4d8894"],
  };

  //pie
  showLabels = true;

  constructor() {}
}
