import {
  Component,
  OnInit,
  OnChanges,
  ElementRef,
  SimpleChanges,
  NgZone,
  Input,
} from '@angular/core';
import {
  ProjectParameter,
  resultsSensitivityAnalysis,
} from '@energy-platform/shared/interfaces';
import { Chart, ChartData } from 'chart.js';
import 'chartjs-chart-box-and-violin-plot';
enum UNITS {
  annual_efficiency = ' [%]',
  internal_rate_of_return = ' [%]',
  levelized_cost_of_energy = ' [Euro/MWh]',
  net_annual_energy_production = ' [GWh]',
  net_present_value = ' [Mio. Euro]',
  payback_period = ' [Years]',
}

enum BOXPLOTTYPE {
  AEP = 'annual_efficiency',
  IROR = 'internal_rate_of_return',
  LCOE = 'levelized_cost_of_energy',
  NAEP = 'net_annual_energy_production',
  NPV = 'net_present_value',
  PP = 'payback_period',
}

@Component({
  selector: 'energy-platform-boxplot-results',
  templateUrl: './boxplot-results.component.html',
  styleUrls: ['./boxplot-results.component.scss'],
})
export class BoxplotResultsComponent implements OnInit, OnChanges {
  @Input()
  targetFigure!: keyof resultsSensitivityAnalysis;
  @Input()
  targetFigureTranslated: any;
  @Input()
  results: ProjectParameter | undefined;
  private boxPlotData: ChartData | undefined;
  private chart: Chart | undefined;
  changeLog: any;

  constructor(
    private readonly elementRef: ElementRef,
    private readonly ngZone: NgZone
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (!this.chart) {
      return;
    }
    // TODO handle updates
    //this.chart.update();
    this.updateBoxPlotData();
  }

  ngOnInit() {
    if (this.results && this.targetFigure) {
      const targetFigure = this.targetFigure;
      const sensAnalysisRes = this.results.sensitivityAnalysis;
      const data = this.createData(sensAnalysisRes, targetFigure);
      this.setBoxPlotData(data);
      this.build();
    }
  }

  private updateBoxPlotData() {
    this.ngOnInit();
  }

  /**
   * Function to make the creation of Data modular and reusable for later updates
   */
  private createData(
    sensAnalysisRes: resultsSensitivityAnalysis,
    targetFigure: keyof resultsSensitivityAnalysis
  ) {
    if (targetFigure) {
      // save data for boxplot: [min, q1, q1, median, q3, q3, max]
      const data = [
        sensAnalysisRes[targetFigure].bound_min,
        sensAnalysisRes[targetFigure].p25,
        sensAnalysisRes[targetFigure].p25,
        sensAnalysisRes[targetFigure].p50,
        sensAnalysisRes[targetFigure].p75,
        sensAnalysisRes[targetFigure].p75,
        sensAnalysisRes[targetFigure].bound_max,
      ];
      // for AEP multiply data with 100, for NPV devide by 1000000
      switch (targetFigure) {
        case 'annual_efficiency': {
          data[0] = data[0] * 100;
          break;
        }
        case 'net_present_value': {
          data[0] = data[0] / 1000000;
          break;
        }
        default: {
          break;
        }
      }

      return data;
    }
    return [];
  }

  /**
   * Function to make the setting of box plot data modular and reusable for later updates
   */
  private setBoxPlotData(data: number[]) {
    this.boxPlotData = {
      labels: [],
      datasets: [
        {
          label: '',
          backgroundColor: '#266f7d',
          borderColor: '#4d8894',
          data: data,
        },
      ],
    };
  }

  private build() {
    this.ngZone.runOutsideAngular(() => {
      if (this.targetFigure) {
        const node: HTMLCanvasElement =
          this.elementRef.nativeElement.querySelector('canvas');
        if (node && this.boxPlotData) {
          this.chart = new Chart(node, {
            /* type: 'horizontalBoxplot' => passendes aus interface wählen*/
            type: 'bar',
            data: this.boxPlotData,
            options: {
              responsive: true,
              layout: {
                padding: {
                  left: 0,
                  right: 0,
                  top: 0,
                  bottom: 0,
                },
              },
              plugins: {
                legend: {
                  position: 'bottom',
                  align: 'end',
                  display: false,
                },
                title: {
                  text: this.targetFigureTranslated + UNITS[this.targetFigure],
                  display: true,
                },
              },

              scales: {
                xAxes: {
                  ticks: {
                    callback: () =>
                      this.targetFigureTranslated + UNITS[this.targetFigure],
                    display: false,
                  },
                },
              },
            },
          });
        }
      }
    });
  }
}
