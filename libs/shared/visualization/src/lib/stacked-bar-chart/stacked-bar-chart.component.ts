import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Input, OnInit } from '@angular/core';
import { ScaleType } from '@swimlane/ngx-charts';

export interface BarChartModel {
  name: string;
  series: SliderModel[];
}

export interface SliderModel {
  name: string;
  value: number;
}

@Component({
  selector: 'energy-platform-stacked-bar-chart',
  templateUrl: './stacked-bar-chart.component.html',
})
export class StackedBarChartComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @Input()
  public values: BarChartModel[] | undefined | null;

  title = 'Angular Charts';

  // options for the chart
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  @Input()
  public showLegend = false;
  showXAxisLabel = false;
  xAxisLabel = '';
  showYAxisLabel = true;
  @Input()
  yAxisLabel = 'Costs';
  timeline = true;
  showDataLabel = true;

  colorScheme = {
    name: '',
    selectable: false,
    group: ScaleType.Linear,
    domain: ['#003b49', '#004e5e', '#266f7d', '#4d8894', '#80aab3', '#b3ccd1'],
  };

  //pie
  showLabels = true;
  constructor() {
    super();
  }

  ngOnInit(): void {
    this.logger.info(this.values);
  }
}
