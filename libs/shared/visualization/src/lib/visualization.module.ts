import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StackedBarChartComponent } from './stacked-bar-chart/stacked-bar-chart.component';
import { WeibullChartComponent } from './weibull-chart/weibull-chart.component';
import { WindroseChartComponent } from './windrose-chart/windrose-chart.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { PlotlyModule } from 'angular-plotly.js';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { BoxplotResultsComponent } from './boxplot-results/boxplot-results.component';

const components = [
  StackedBarChartComponent,
  WeibullChartComponent,
  WindroseChartComponent,
  BarChartComponent,
  BoxplotResultsComponent,
];
@NgModule({
  imports: [CommonModule, NgxChartsModule, PlotlyModule],
  declarations: components,
  exports: components,
})
export class VisualizationModule {}
