import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WeibullChartComponent } from './weibull-chart.component';

describe('WeibullChartComponent', () => {
  let component: WeibullChartComponent;
  let fixture: ComponentFixture<WeibullChartComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [WeibullChartComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WeibullChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
