import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { WindWeibull } from '@energy-platform/shared/interfaces';
import { ScaleType } from '@swimlane/ngx-charts';
import * as d3shape from 'd3-shape';

@Component({
  selector: 'energy-platform-weibull-chart',
  templateUrl: './weibull-chart.component.html',
})
export class WeibullChartComponent implements OnChanges {
  @Input() weibull: WindWeibull | undefined;
  @Input() name = 'Weibull Distribution';
  @Input() min = 0;
  @Input() max = 40;
  @Input() step = 0.1;
  @Input() timeslot = 0;
  @Input() direction = 0;

  weibullData: any[][] | undefined;

  // ngx-charts
  plotData: any[] | undefined;
  colorScheme = {
    name: '',
    selectable: false,
    group: ScaleType.Linear,
    domain: ['#2680be'],
  };
  xAxisTicks: any[] = [];
  curve = d3shape.curveBasis;

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['weibull'] ||
      changes['name'] ||
      changes['min'] ||
      changes['max'] ||
      changes['step']
    ) {
      this.updateWeibull();
    }

    this.updatePlot();
  }

  updateWeibull() {
    const weibullData: any[][] = [];

    // Workaround for floating point precision in for-loop
    const step = (x: number) => parseFloat((x + this.step).toFixed(1));

    if (this.weibull) {
      for (const item of this.weibull.weibull) {
        // Calculate series of discrete values from weibull distribution
        const series = [];
        for (let x = this.min; x <= this.max; x = step(x)) {
          series.push({
            name: x + ' m/s',
            value: this.calculateWeibull(x, item.scale, item.shape),
          });
        }

        const multiSeries = [
          {
            name: this.name,
            series: series,
          },
        ];

        if (weibullData[item.timeslot] == undefined) {
          weibullData[item.timeslot] = [];
        }

        weibullData[item.timeslot][item.direction] = multiSeries;
      }
    }

    this.weibullData = weibullData;
  }

  updatePlot() {
    if (this.weibullData) {
      this.plotData = this.weibullData[this.timeslot][this.direction];
    }
    this.xAxisTicks = this.getXAxisTicks();
  }

  calculateWeibull(x: number, scale: number, shape: number) {
    if (x < 0) {
      return 0;
    }

    return (
      (shape / scale) *
      Math.pow(x / scale, shape - 1) *
      Math.exp(-Math.pow(x / scale, shape))
    );
  }

  getXAxisTicks() {
    const result = [];
    if (this.plotData) {
      const series = this.plotData[0].series;
      for (const tick of [0, 0.125, 0.25, 0.375, 0.5, 0.625, 0.75, 0.875, 1]) {
        result.push(series[(series.length - 1) * tick].name);
      }
    }
    return result;
  }
}
