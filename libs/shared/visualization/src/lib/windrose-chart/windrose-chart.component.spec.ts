import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WindroseChartComponent } from './windrose-chart.component';

describe('WindroseChartComponent', () => {
  let component: WindroseChartComponent;
  let fixture: ComponentFixture<WindroseChartComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [WindroseChartComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(WindroseChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
