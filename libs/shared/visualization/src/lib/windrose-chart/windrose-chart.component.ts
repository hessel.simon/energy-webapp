import {
  Component,
  OnChanges,
  SimpleChanges,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { WindBins } from '@energy-platform/shared/interfaces';
import { rgb } from 'd3';

@Component({
  selector: 'energy-platform-windrose-chart',
  templateUrl: './windrose-chart.component.html',
})
export class WindroseChartComponent implements OnChanges {
  @Input() bins: WindBins | undefined;
  @Input() timeslot = 0;
  @Input() thresholds: number[] = [2, 4, 6, 10, 15, 40];

  @Output() selected = new EventEmitter<any>();

  windroseData: any[] = [];
  currentDirIndex = 0;

  // plotly.js
  plotData: any[] = [];
  plotFullData: any[] = [];
  config = { displayModeBar: false };
  layout = {
    autosize: true,
    legend: {
      y: 0.5,
      bgcolor: 'rgba(0, 0, 0, 0)',
      itemclick: false,
      itemdoubleclick: false,
    },
    polar: {
      barmode: 'stack',
      bargap: 0.1,
      radialaxis: { tick0: 1, dtick: 2 },
      angularaxis: { direction: 'clockwise' },
    },
    margin: {
      l: 40,
      r: 40,
      t: 60,
      b: 60,
    },
    colorway: [
      '#e1fffd',
      '#96d5d5',
      '#77acac',
      '#0098ac',
      '#008a9e',
      '#007286',
      '#006c80',
      '#005566',
      '#004454',
      '#002c36',
      '#9cdbdb',
      '#a6e9e9',
      '#0092a6',
      '#00778a',
    ],
    plot_bgcolor: 'rgba(0, 0, 0, 0)',
    paper_bgcolor: 'rgba(0, 0, 0, 0)',
  };

  ngOnChanges(changes: SimpleChanges) {
    if (changes['thresholds']) {
      this.updateThresholds();
    }

    if (changes['bins']) {
      this.updateWindroseData();
      this.updatePlotData();
    }

    this.updatePlot();
    this.updateSelected();
  }

  updateThresholds() {
    const removeDuplicates = (array: number[]): number[] =>
      array.filter((x, i, a) => a.indexOf(x) == i);

    const removeBelowOne = (array: number[]): number[] =>
      array.filter((x) => x > 0);

    const sortAscending = (array: number[]): number[] =>
      array.sort((x, y) => x - y);

    let t = this.thresholds;
    t = removeDuplicates(t);
    t = removeBelowOne(t);
    t = sortAscending(t);

    this.thresholds = t;
  }

  updateWindroseData() {
    const result = [];
    const data: any[] = [];

    if (this.bins) {
      const directions = this.bins.directions;
      const probabilities = this.bins.direction_probability;

      // Create a WindroseData for every timeslot
      for (const [timeIndex, timeData] of this.bins.data.entries()) {
        const windroseData = {
          directions: directions,
          thresholds: this.thresholds,
          data,
        };

        // Create a WindroseItem for every direction
        for (const [dirIndex, dirData] of timeData.entries()) {
          const windroseItem: any = {
            direction: dirIndex * (360 / directions),
            probability: probabilities[timeIndex][dirIndex],
            data: [],
            fullData: dirData,
          };

          // Add bin data according to thresholds
          let lower = 0;
          for (const upper of this.thresholds) {
            if (upper > 0 && upper <= dirData.length) {
              let value: number;
              if (upper != lower) {
                // Sum up bin values for ranges
                value = dirData.slice(lower, upper).reduce((x, y) => x + y, 0);
              } else {
                value = dirData[upper];
              }
              windroseItem.data.push(value * windroseItem.probability);

              lower = upper;
            }
          }

          windroseData.data.push(windroseItem);
        }

        result.push(windroseData);
      }
    }

    this.windroseData = result;
  }

  updatePlotData() {
    const result = [];

    for (const windroseData of this.windroseData) {
      const plotData = [];
      const dirOffset = (360 / windroseData.directions) * 0.5;

      let lower = 0;
      for (const [index, upper] of windroseData.thresholds.entries()) {
        const totals = [];
        const values = [];
        const thetas = [];
        const ids = [];

        for (const direction of windroseData.data) {
          ids.push(
            direction.direction -
              dirOffset +
              '&#176; - ' +
              (direction.direction + dirOffset) +
              '&#176;'
          );
          totals.push(direction.probability * 1000);
          values.push(direction.data[index]);
          thetas.push(direction.direction);
        }

        plotData.push({
          ids: ids,
          theta: thetas,
          text: totals,
          name: lower + ' - ' + upper + ' m/s',
          r: values,
          type: 'barpolar',
          opacity: 1.0,
          hovertemplate:
            '<b>&#952;</b>: %{id}<br>' +
            '<b>&#931;</b>: %{text:.4f} %<br>' +
            '<b>%{fullData.name}</b>: %{r:.4f} %' +
            '<extra></extra>',
          selected: {
            marker: {
              color: rgb(this.layout.colorway[index]).brighter(0.7),
            },
          },
          unselected: {
            marker: {
              opacity: 1.0,
            },
          },
        });

        lower = upper;
      }

      result.push(plotData);
    }

    this.plotFullData = result;
  }

  updatePlot() {
    this.plotData = this.plotFullData[this.timeslot];
  }

  updateSelected() {
    // Emit selected direction
    const windroseData = this.windroseData[this.timeslot];
    const direction = windroseData.data[this.currentDirIndex].direction;
    const directions = windroseData.directions;

    this.selected.emit({
      theta: direction,
      lower: direction - (360 / directions) * 0.5,
      upper: direction + (360 / directions) * 0.5,
    });

    // Mark direction as selected in plot
    for (const trace of this.plotData) {
      trace.selectedpoints = [this.currentDirIndex];
    }
  }

  onHover(event: any) {
    const dirIndex = event.points[0].pointIndex;

    if (dirIndex != this.currentDirIndex) {
      this.currentDirIndex = dirIndex;
      this.updateSelected();
    }
  }
}
