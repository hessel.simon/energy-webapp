import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  NewsService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { News } from '@energy-platform/shared/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'environments/environment';

@Component({
  selector: 'energy-platform-admin-dashboard',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss'],
})
export class AdminDashboardComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  adminNews: News[] = [];
  dataSource = new MatTableDataSource<string>();
  form!: FormGroup;
  editMode!: boolean;
  editedPostId!: string;
  editedPostState!: boolean;
  displayedColumns: string[] = ['headline', 'news', 'state', 'date', 'action'];

  constructor(
    private newsService: NewsService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder,
    //public used in html
    public dateFormatterService: DateFormatterService
  ) {
    super();
  }

  ngOnInit() {
    this.createForm();
    this.loadNews();
  }

  loadNews() {
    this.newsService.getNewsListener().subscribe(
      (data: News[]) => (this.adminNews = [...data]),
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: environment.MAT_ERROR_DEFAULT,
        });
      }
    );
    this.newsService.fetchNews();
  }

  private createForm() {
    this.form = this.fb.group({
      headlineToPost: ['', Validators.required],
      messageToPost: ['', Validators.required],
      dateToPost: ['', Validators.required],
    });
  }

  onSubmit(asDraft: boolean) {
    if (this.editMode) {
      this.newsService.updatePost(
        this.editedPostId,
        this.form.value.headlineToPost,
        this.form.value.messageToPost,
        asDraft
      );
    } else {
      this.newsService.addPost(
        this.form.value.headlineToPost,
        this.form.value.messageToPost,
        asDraft
      );
    }
    this.editMode = false;
    this.form.reset();
    Object.keys(this.form.controls).forEach((key) => {
      this.form.controls[key].setErrors(null);
    });
  }

  deletePost(id: string) {
    this.newsService.deletePost(id);
  }

  editPost(id: string, index: number) {
    this.editedPostId = id;
    this.editedPostState = this.adminNews[index].draft;
    this.form.setValue({
      headlineToPost: this.adminNews[index].headline,
      messageToPost: this.adminNews[index].text,
      dateToPost: this.adminNews[index].date.toISOString().substr(0, 10),
    });
    this.editMode = true;
  }

  movePost(index: number, direction: number) {
    this.snackBar.open(`Not yet implemented`, 'Ok', {
      duration: environment.MAT_ERROR_DEFAULT,
    });
  }
}
