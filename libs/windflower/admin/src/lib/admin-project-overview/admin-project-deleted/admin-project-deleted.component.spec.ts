import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminProjectDeletedComponent } from './admin-project-deleted.component';

describe('AdminProjectDeletedComponent', () => {
  let component: AdminProjectDeletedComponent;
  let fixture: ComponentFixture<AdminProjectDeletedComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminProjectDeletedComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectDeletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
