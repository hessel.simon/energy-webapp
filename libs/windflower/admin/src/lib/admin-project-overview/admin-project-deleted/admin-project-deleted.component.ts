import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, OnInit } from '@angular/core';
import { Project } from '@energy-platform/shared/interfaces';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ProjectsService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-admin-project-deleted',
  templateUrl: './admin-project-deleted.component.html',
  styleUrls: ['./admin-project-deleted.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminProjectDeletedComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  private _projects: BehaviorSubject<Project[]>;
  dateFormatter: DateFormatterService;
  projects: Observable<Project[]>;

  columnsToDisplay: string[] = [
    'icon',
    'projectName',
    'startingTime',
    'action',
  ];
  expandedElement!: Project | null;

  constructor(
    private router: Router,
    private projectsService: ProjectsService,
    private dateFormatterService: DateFormatterService,
    private snackBar: MatSnackBar
  ) {
    super();
    this.dateFormatter = dateFormatterService;
    this._projects = new BehaviorSubject<Project[]>([]);
    this.projects = this._projects.asObservable();
  }

  ngOnInit() {
    this.getProjects();
  }

  private getProjects() {
    this.logger.info(' deleted tab component ');
    this.projectsService.getDeletedProjectsListener().subscribe(
      (projects) => {
        this.logger.info('look for deleted projects : ', projects);
        this._projects.next(projects);
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
  }

  recycleProject(project: any) {
    this.projectsService.recycleProject(project.id);
  }

  itemClicked(project: any) {
    // change type
    this.router.navigate(['/app', 'project', project.id]);
  }
}
