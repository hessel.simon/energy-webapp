import { Component } from '@angular/core';
import { User } from '@energy-platform/shared/interfaces';

@Component({
  selector: 'energy-platform-admin-project-overview',
  templateUrl: './admin-project-overview.component.html',
  styleUrls: ['./admin-project-overview.component.scss'],
})
export class AdminProjectOverviewComponent {
  selectedIndex = 0;

  users: User[] = [];

  constructor() {}

  indexChange(event: number) {
    this.selectedIndex = event;
  }
}
