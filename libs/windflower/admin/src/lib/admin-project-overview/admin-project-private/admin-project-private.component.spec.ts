import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminProjectPrivateComponent } from './admin-project-private.component';

describe('AdminProjectPrivateComponent', () => {
  let component: AdminProjectPrivateComponent;
  let fixture: ComponentFixture<AdminProjectPrivateComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminProjectPrivateComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectPrivateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
