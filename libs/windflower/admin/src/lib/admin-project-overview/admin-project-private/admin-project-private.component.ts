import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Project } from '@energy-platform/shared/interfaces';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { ProjectDetailDialogComponent } from '@energy-platform/shared/dialog';
import { DeleteProjectDialogComponent } from '@energy-platform/shared/dialog';
import {
  ProjectsService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

@Component({
  selector: 'energy-platform-admin-project-private',
  templateUrl: './admin-project-private.component.html',
  styleUrls: ['./admin-project-private.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminProjectPrivateComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  private _projects: BehaviorSubject<Project[]>;
  projects: Observable<Project[]>;
  dateFormatter: DateFormatterService;

  columnsToDisplay: string[] = [
    'icon',
    'projectName',
    'userCreator',
    'startingTime',
    'action',
  ];

  @ViewChild(MatTable, { static: true })
  table!: MatTable<any>;
  expandedElement!: Project | null;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private projectsService: ProjectsService,
    private dateFormatterService: DateFormatterService,
    private snackBar: MatSnackBar
  ) {
    super();
    this.dateFormatter = dateFormatterService;
    this._projects = new BehaviorSubject<Project[]>([]);
    this.projects = this._projects.asObservable();
  }

  ngOnInit() {
    this.getProjects();
  }

  private getProjects() {
    this.projectsService.getAllProjectsListener().subscribe(
      (projects) => {
        const privProj = projects.filter((project) => {
          if (project.projectStatus === 'hidden') {
            return true;
          }
          return false;
        });
        this._projects.next(privProj);
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
  }
  editProject(project: Project) {
    const dialogRef = this.dialog.open(ProjectDetailDialogComponent, {
      data: { project: project },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.table.renderRows();
      }
    });
  }
  deleteProject(project: Project) {
    const dialogRef = this.dialog.open(DeleteProjectDialogComponent, {
      data: { projectName: project.name },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.logger.info('The dialog was closed');
      if (result) {
        // delete project -> backend call to delete
        this.projectsService.deleteProject(project.id);
        const projs = this._projects.getValue();
        const projectToDelete = projs.indexOf(project);
        projs.splice(projectToDelete, 1);
        this._projects.next(projs);
        this.table.renderRows();
      }
    });
  }

  itemClicked(project: any) {
    // change type
    this.router.navigate(['/app', 'project', project.id]);
  }
}
