import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { AdminProjectPublishedComponent } from './admin-project-published.component';

describe('AdminProjectPublishedComponent', () => {
  let component: AdminProjectPublishedComponent;
  let fixture: ComponentFixture<AdminProjectPublishedComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AdminProjectPublishedComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminProjectPublishedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
