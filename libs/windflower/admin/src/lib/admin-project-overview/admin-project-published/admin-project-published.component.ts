import { Component, OnInit, ViewChild } from '@angular/core';
import { Project } from '@energy-platform/shared/interfaces';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ProjectsService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DeleteProjectDialogComponent } from '@energy-platform/shared/dialog';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';

@Component({
  selector: 'energy-platform-admin-project-published',
  templateUrl: './admin-project-published.component.html',
  styleUrls: ['./admin-project-published.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminProjectPublishedComponent implements OnInit {
  dateFormatter: DateFormatterService;
  _projects: BehaviorSubject<Project[]>;
  projects: Observable<Project[]>;

  columnsToDisplay: string[] = [
    'icon',
    'projectName',
    'latestVersion',
    'startingTime',
    'action',
  ];
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;
  expandedElement!: Project | null;

  constructor(
    private router: Router,
    private projectsService: ProjectsService,
    private dateFormatterService: DateFormatterService,
    private snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    this.dateFormatter = dateFormatterService;
    this._projects = new BehaviorSubject<Project[]>([]);
    this.projects = this._projects.asObservable();
  }

  ngOnInit() {
    this.getProjects();
  }

  private getProjects() {
    this.projectsService.getAllProjectsListener().subscribe(
      (projects) => {
        const privProj = projects.filter((project) => {
          if (
            project.releases.filter((it) => it.status === 'public').length > 0
          ) {
            return true;
          }
          return false;
        });

        this._projects.next(privProj);
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
  }
  publishToWebapp(project: any) {
    // this.projectsService.publishPublishedProjectToWebapp(project);
  }

  discardFromWebapp(project: any) {}

  itemClicked(project: any) {
    // change type
    this.router.navigate(['/app', 'project', project.id]);
  }

  deleteProject(project: Project) {
    const dialogRef = this.dialog.open(DeleteProjectDialogComponent, {
      data: { projectName: project.name },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        // delete project -> backend call to delete
        this.projectsService.deleteProject(project.id);
        const projs = this._projects.getValue();
        const projectToDelete = projs.indexOf(project);
        projs.splice(projectToDelete, 1);
        this._projects.next(projs);
        this.table.renderRows();
      }
    });
  }
}
