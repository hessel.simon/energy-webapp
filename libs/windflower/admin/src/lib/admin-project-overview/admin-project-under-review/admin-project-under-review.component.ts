import { Component, OnInit } from '@angular/core';
import { Project } from '@energy-platform/shared/interfaces';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ProjectsService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BehaviorSubject, Observable } from 'rxjs';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';

@Component({
  selector: 'energy-platform-admin-project-under-review',
  templateUrl: './admin-project-under-review.component.html',
  styleUrls: ['./admin-project-under-review.component.less'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminProjectUnderReviewComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  dateFormatter: DateFormatterService;
  private _projects: BehaviorSubject<Project[]>;
  projects: Observable<Project[]>;

  columnsToDisplay: string[] = [
    'icon',
    'projectName',
    'version',
    'startingTime',
    'action',
  ];
  expandedElement!: Project | null;

  private _releases: BehaviorSubject<any>;
  releases$: Observable<any>;

  constructor(
    private router: Router,
    private projectsService: ProjectsService,
    private dateFormatterService: DateFormatterService,
    private snackBar: MatSnackBar
  ) {
    super();
    this.dateFormatter = dateFormatterService;
    this._projects = new BehaviorSubject<Project[]>([]);
    this.projects = this._projects.asObservable();
    this._releases = new BehaviorSubject<any>([]);
    this.releases$ = this._releases.asObservable();
  }

  ngOnInit() {
    this.getProjects();
  }

  private getProjects() {
    this.projectsService.getAllProjectsListener().subscribe(
      (projects) => {
        const releases: any = [];
        projects.map((project) => {
          const arrRealease = project.releases
            .filter((rel) => rel.status === 'on_review')
            .map((rel) => {
              // eslint-disable-next-line prefer-const
              let release = { ...rel };
              // release.project = project;
              return release;
            });
          this.logger.info(arrRealease);
          releases.push(...arrRealease);
        });

        this._releases.next(releases);
        this.logger.info('from project review ', releases);
        // this._projects.next(underRev);
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
  }

  publishToGit(project: Project) {
    project.published_web_app = false;
    // this.projectsService.publishProjectRelease({project);
    this.projectsService.publishProjectRelease({
      project,
      id: project.releases[project.releases.length - 1].id || '',
    });
  }

  discardPublishRequest(project: Project) {
    // this.projectsService.discardProjectPublishRequest(project);
    this.projectsService.discardProjectPublishRequest({
      project,
      id: project.releases[project.releases.length - 1].id || '',
    });
  }

  itemClicked(project: any) {
    // change type
    this.router.navigate(['/app', 'project', project.id]);
  }
}
