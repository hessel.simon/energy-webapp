import { Component, OnInit, Input } from '@angular/core';
import { of, Subscription, Observable } from 'rxjs';
import {
  CalculationService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { Project, Task } from '@energy-platform/shared/interfaces';
import { Router } from '@angular/router';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';

@Component({
  selector: 'energy-platform-admin-all-jobs-list',
  templateUrl: './admin-all-jobs-list.component.html',
  styleUrls: ['./admin-all-jobs-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminAllJobsListComponent implements OnInit {
  @Input() jobsSearch = '';

  projects: Project[] = [];
  jobs: Observable<Task[]> = of([]);

  columnsToDisplay: string[] = [
    'id',
    'projectId',
    'taskTyp',
    'requestTime',
    'deprecated',
    'action',
  ];
  expandedElement: Project | null = null;

  /****************
   * Subscriptions
   ****************/
  private allProjectsSub!: Subscription;

  constructor(
    private calculationService: CalculationService,
    private projectsService: ProjectsService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getJobs();
  }

  ngOnDestroy() {
    if (this.allProjectsSub) this.allProjectsSub.unsubscribe();
  }

  private getJobs() {
    this.jobs = this.calculationService.getAllCalculations();
  }

  itemClicked(job: any) {
    // change type
    this.router.navigate(['/admin/job', job.id]);
  }

  abortJob(job: Task) {
    this.calculationService.stopCalculation(job.id);
  }
}
