import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { UserService, ProjectsService } from '@energy-platform/shared/services';
import { Subscription } from 'rxjs';
import { Project, User, Task } from '@energy-platform/shared/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-admin-expansion-item',
  templateUrl: './admin-expansion-item.component.html',
  styleUrls: ['./admin-expansion-item.component.scss'],
})
export class AdminExpansionItemComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit, OnDestroy
{
  @Input() status = '';
  @Input() jobs: Task[] = [];
  projects: Project[] = [];
  users: User[] = [];
  loading = true;

  displayedColumns: string[] = [
    'projectName',
    'jobType',
    'requestTime',
    'status',
    'action',
  ];

  /****************
   * Subscriptions
   ****************/
  private allProjectsSub!: Subscription;

  constructor(
    private usersService: UserService,
    private projectService: ProjectsService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  ngOnInit() {
    this.getItems();
  }

  ngOnDestroy() {
    if (this.allProjectsSub) this.allProjectsSub.unsubscribe();
  }

  private getItems() {
    this.allProjectsSub = this.projectService
      .getAllProjectsListener()
      .subscribe(
        (projects: Project[]) => {
          this.projects = projects;
        },
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
    this.usersService.getUserListener().subscribe(
      (data) => {
        this.users = data;
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.projectService.fetchAllAccessibleProjects();
    this.logger.info('Input ', this.jobs);
    this.usersService.fetchAllUsers();
  }

  itemClicked(row: Task) {
    this.logger.info(row);
  }

  clearList(status: string) {
    this.jobs = [];
    // API CALL TO DELETE JOBS WITH STATUS -> status passed as argument
  }

  clearItemfromList(job: Task) {
    const jobToDelete = this.jobs.indexOf(job);
    this.jobs.splice(jobToDelete, 1);
  }
}
