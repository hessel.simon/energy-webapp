import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Organization } from '@energy-platform/shared/interfaces';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import { Router } from '@angular/router';
import { OrganizationService } from '@energy-platform/shared/services';
import { Subscription } from 'rxjs';
import {
  trigger,
  state,
  style,
  animate,
  transition,
} from '@angular/animations';
import {
  OrganizationDetailDialogComponent,
  DeleteOrganizationComponent,
  MergeOrganizationsComponent,
  NewOrganizationAdminComponent,
} from '@energy-platform/shared/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-admin-all-organizations-list',
  templateUrl: './admin-all-organizations-list.component.html',
  styleUrls: ['./admin-all-organizations-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminAllOrganizationsListComponent implements OnInit {
  @Input() organizationsSearch = '';
  organizations: Organization[] = [];
  columnsToDisplay: string[] = ['logo', 'name', 'description', 'action'];
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  expandedElement: Organization | null = null;

  /****************
   * Subscriptions
   ****************/
  private orgaSub!: Subscription;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private organizationService: OrganizationService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit() {
    this.getOrganizations();
  }

  itemClicked(organization: any) {
    // change type
    this.router.navigate(['/admin/organizations', organization.id]);
  }

  private getOrganizations() {
    this.organizationService.getOrganizationListenerAll().subscribe(
      (data) => {
        const orgas: Organization[] = data.filter((organizationData) => {
          return (
            organizationData.organizationName.includes(
              this.organizationsSearch
            ) || organizationData.id === this.organizationsSearch
          );
        });
        this.organizations = orgas;
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.organizationService.fetchOrganizationsAll();
  }

  editOrganization(organization: Organization) {
    // change user type
    const dialogRef = this.dialog.open(OrganizationDetailDialogComponent, {
      data: { organization: organization },
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.table.renderRows();
      }
    });
  }

  deleteOrganization(organization: Organization) {
    if (!this.organizations) {
      return;
    }
    if (organization.users.length === 0 || this.organizations.length === 1) {
      const dialogDeleteOrga = this.dialog.open(DeleteOrganizationComponent, {
        data: { organization: organization },
      });
    } else {
      const dialogDeleteOrga = this.dialog.open(MergeOrganizationsComponent, {
        data: { organization: organization },
      });
    }
  }

  addNewOrganization() {
    const dialogNewOrga = this.dialog.open(NewOrganizationAdminComponent, {
      data: {
        name: '',
        description: '',
        logoUrl: '',
      },
    });
  }
}
