import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Input, OnChanges, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatTable } from '@angular/material/table';
import {
  DeleteDialogComponent,
  UserDetailDialogComponent,
} from '@energy-platform/shared/dialog';
import { User } from '@energy-platform/shared/interfaces';
import {
  UserService,
  DateFormatterService,
} from '@energy-platform/shared/services';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-admin-all-users-list',
  templateUrl: './admin-all-users-list.component.html',
  styleUrls: ['./admin-all-users-list.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0' })),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class AdminAllUsersListComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnChanges
{
  @Input() usersSearch = '';
  dateFormatter: DateFormatterService;
  users: User[] = [];
  usersMobileView: any[] = [];
  columnsToDisplay: string[] = [
    'name',
    'lastName',
    'email',
    'organization',
    'deactivate',
    'action',
  ];
  @ViewChild(MatTable, { static: true }) table!: MatTable<any>;

  expandedElement: User | null = null;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private usersService: UserService,
    private snackBar: MatSnackBar,
    private dateFormatterService: DateFormatterService
  ) {
    super();
    this.dateFormatter = dateFormatterService;
  }

  ngOnChanges() {
    this.getUsers();
  }

  private getUsers() {
    this.usersService.getUserListener().subscribe(
      (data) => {
        const users: User[] = data.filter((usersData) => {
          // changeType
          return (
            usersData.deleted === false &&
            (usersData.lastName.includes(this.usersSearch) ||
              usersData.id === this.usersSearch ||
              usersData.organization.organizationName.includes(
                this.usersSearch
              ) ||
              usersData.firstName.includes(this.usersSearch))
          );
        });
        this.users = users;
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
    this.usersService.fetchAllUsers();
  }

  itemClicked(user: any) {
    // change type
    this.router.navigate(['/admin/users', user.id]);
  }

  deactivate(userClicked: User) {
    const deactUser = this.users[this.users.indexOf(userClicked)];
    deactUser.active = this.isDeactivated(deactUser);
    this.usersService.editUser(deactUser);
    this.table.renderRows();
  }

  isDeactivated(user: User): boolean {
    const test = this.users.find(
      (userToDeactivate) => userToDeactivate.id === user.id
    );
    if (test) return !test.active;
    return false;
  }

  editUser(user: User) {
    // change user type
    const dialogRef = this.dialog.open(UserDetailDialogComponent, {
      data: { user: user },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.logger.info('The dialog was closed');
      if (result) {
        this.table.renderRows();
      }
    });
  }

  deleteUser(user: User) {
    // change user type
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: { firstName: user.firstName, lastName: user.lastName },
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.logger.info('The dialog was closed');
      if (result) {
        this.usersService.deleteUser(user.id);
        const userToDelete = this.users.indexOf(user);
        this.users.splice(userToDelete, 1);
        this.table.renderRows();
      }
    });
  }
}
