import { Component, OnInit } from '@angular/core';
import { fromEvent } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'energy-platform-admin-user-overview',
  templateUrl: './admin-user-overview.component.html',
  styleUrls: ['./admin-user-overview.component.scss'],
})
export class AdminUserOverviewComponent implements OnInit {
  selectedIndex = 0;
  usersSearch = '';
  jobsSearch = '';
  organizationSearch = '';

  ngOnInit() {
    this.subscribeForSearch();
  }

  subscribeForSearch() {
    // eslint-disable-next-line prefer-const
    let event = document.getElementById('searchInput');
    if (event)
      fromEvent(event, 'keyup')
        .pipe(map((data: any) => data.srcElement.value))
        .subscribe((dataFromSearchInput) => {
          this.distinctionOfActions(dataFromSearchInput, this.selectedIndex);
        });
  }

  distinctionOfActions(searchInput: string, tab: number) {
    switch (tab) {
      case 0:
        this.usersSearch = searchInput;
        break;
      case 1:
        this.organizationSearch = searchInput;
        break;
      case 2:
        this.jobsSearch = searchInput;
        break;
    }
  }

  indexChange(event: number) {
    this.selectedIndex = event;
  }
}
