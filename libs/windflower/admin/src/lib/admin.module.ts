import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
// Components
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
// AdminProjectOverview
import { AdminProjectOverviewComponent } from './admin-project-overview/admin-project-overview.component';
import { AdminProjectDeletedComponent } from './admin-project-overview/admin-project-deleted/admin-project-deleted.component';
import { AdminProjectPrivateComponent } from './admin-project-overview/admin-project-private/admin-project-private.component';
import { AdminProjectPublishedComponent } from './admin-project-overview/admin-project-published/admin-project-published.component';
import { AdminProjectUnderReviewComponent } from './admin-project-overview/admin-project-under-review/admin-project-under-review.component';
// UserOverview
import { AdminUserOverviewComponent } from './admin-user-overview/admin-user-overview.component';
import { AdminAllJobsListComponent } from './admin-user-overview/admin-all-jobs-list/admin-all-jobs-list.component';
import { AdminAllOrganizationsListComponent } from './admin-user-overview/admin-all-organizations-list/admin-all-organizations-list.component';
import { AdminAllUsersListComponent } from './admin-user-overview/admin-all-users-list/admin-all-users-list.component';
// Material Components
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatListModule } from '@angular/material/list';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';

import { TranslateModule } from '@ngx-translate/core';
import { LayoutModule } from '@energy-platform/shared/layout';

// import { AsyncPipe } from '@angular/common';
@NgModule({
  imports: [
    // AsyncPipe,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    LayoutModule,

    MatCheckboxModule,
    MatButtonModule,
    MatInputModule,
    MatAutocompleteModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatRadioModule,
    MatSelectModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatStepperModule,
    MatTabsModule,
    MatExpansionModule,
    MatButtonToggleModule,
    MatChipsModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatProgressBarModule,
    MatDialogModule,
    MatTooltipModule,
    MatSnackBarModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    TranslateModule.forChild(),
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard',
      },
      {
        path: 'dashboard',
        component: AdminDashboardComponent,
        // resolve: {
        //   userData: UserProfileResolver,
        // },
      },
      {
        path: 'users',
        component: AdminUserOverviewComponent,
        // canActivate: [AuthenticationGuard],
      },
      // {
      //   path: "users/:id",
      //   component: AdminUserDetailsComponent,
      //   canActivate: [AuthenticationGuard],
      // },
      {
        path: 'projects',
        component: AdminProjectOverviewComponent,
      },
      // {
      //   path: "projects/:id",
      //   component: AdminProjectDetailsComponent,
      // },
      // {
      //   path: "jobs",
      //   component: AdminJobOverviewComponent,
      // },
    ]),
  ],
  declarations: [
    AdminDashboardComponent,
    AdminProjectOverviewComponent,
    AdminProjectDeletedComponent,
    AdminProjectPrivateComponent,
    AdminProjectPublishedComponent,
    AdminProjectUnderReviewComponent,

    AdminUserOverviewComponent,
    AdminAllJobsListComponent,
    AdminAllOrganizationsListComponent,
    AdminAllUsersListComponent,
  ],
})
export class AdminModule {}
