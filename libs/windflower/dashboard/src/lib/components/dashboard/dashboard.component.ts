import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { NewProjectModalComponent } from '@energy-platform/shared/dialog';
import {
  Project,
  ProjectStatus,
  // TEMP Release,
  Right,
  User,
} from '@energy-platform/shared/interfaces';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import {
  ProjectsService,
  ReleasesService,
  UserService,
} from '@energy-platform/shared/services';
import { ProjectMapComponent } from '@energy-platform/shared/layout';
import { MatCheckboxChange } from '@angular/material/checkbox';

export enum SortOptions {
  NEWEST_FIRST = 'newestFirst',
  OLDEST_FIRST = 'oldestFirst',
}

@Component({
  selector: 'energy-platform-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent
  extends UnsubscribeOnDestroyAdapter
  implements AfterViewInit
{
  @ViewChild(ProjectMapComponent) private Map!: ProjectMapComponent;

  // Projects
  privateProjects: Project[] = [];
  publicProjects: Project[] = [];
  privateProjectsFiltered: Project[] = [];
  publicProjectsFiltered: Project[] = [];

  filters = {
    displayVisible: true,
    displayPrivate: true,
    displayPublic: true,
  };

  // Sort order
  selectedSortOption_private: string = SortOptions.NEWEST_FIRST;
  selectedSortOption_public: string = SortOptions.NEWEST_FIRST;

  // User
  user: User | undefined;

  // Subscriptions
  private privateProjectsSub: Subscription | undefined;
  private publicProjectsSub: Subscription | undefined;
  sortOptions = SortOptions; // used in HTML file

  // TEMP releases: Release[];
  // TEMP defaultRelease: any = "1";

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private activatedRoute: ActivatedRoute,
    private projectsService: ProjectsService,
    private snackBar: MatSnackBar,
    private releaseService: ReleasesService,
    private userService: UserService
  ) {
    super();
    this.userService.getOwnUserProfile().subscribe(
      (user) => {
        this.user = user;
        this.logger.info('logged in: ', this.user);
        this.getProjects();
      },
      (error) => {
        this.logger.error('not logged in: ', error);
      }
    );
  }
  ngAfterViewInit() {
    this.getProjects();
  }
  updateMap() {
    this.Map.setPublicProjects(this.publicProjects);
    this.Map.setPrivateProjects(this.privateProjects);
  }

  getProjects() {
    if (this.user) {
      this.subs.sink = this.projectsService
        .fetchAllAccessibleProjects(true, this.user.id)
        .subscribe(
          () => {},
          (error) => {
            this.snackBar.open(
              `Error ${error.status}: ${error.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
          }
        );
      this.privateProjectsSub = this.projectsService
        .getPrivateProjectsListener()
        .subscribe(
          (projectsPrivate: Project[]) => {
            this.logger.info('list of private projects ', projectsPrivate);
            this.privateProjects = projectsPrivate;
            this.privateProjectsFiltered = this.privateProjects;
            this.Map.setPrivateProjects(this.privateProjects);
            this.sortPrivateProjects();
          },
          (error) => {
            this.snackBar.open(
              `Error ${error.status}: ${error.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
          }
        );
    } else {
      this.logger.info('not logged in');
    }

    this.publicProjectsSub = this.projectsService
      .getOpenProjectsListener()
      .subscribe(
        (projectsPublic: Project[]) => {
          this.logger.info(' list of all public projects ', projectsPublic);
          this.publicProjects = projectsPublic;
          this.publicProjectsFiltered = this.publicProjects;
          this.Map.setPublicProjects(this.publicProjects);
          this.sortPublicProjects();
        },
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );

    this.subs.add(this.privateProjectsSub, this.publicProjectsSub);
  }

  private sortPrivateProjects() {
    // Sorts projects by current selected sort option
    switch (this.selectedSortOption_private) {
      case SortOptions.OLDEST_FIRST:
        this.privateProjects = this.privateProjects.sort((a, b) => {
          return a.creationDate.getTime() - b.creationDate.getTime();
        });
        break;
      default:
        // Default sort option is "created-newest-first"
        this.privateProjects = this.privateProjects.sort((a, b) => {
          return b.creationDate.getTime() - a.creationDate.getTime();
        });
        break;
    }
  }

  private sortPublicProjects() {
    // Sorts projects by current selected sort option
    switch (this.selectedSortOption_private) {
      case SortOptions.OLDEST_FIRST:
        this.publicProjects = this.publicProjects.sort((a, b) => {
          return a.creationDate.getTime() - b.creationDate.getTime();
        });
        break;
      default:
        // Default sort option is "created-newest-first"
        this.publicProjects = this.publicProjects.sort((a, b) => {
          return b.creationDate.getTime() - a.creationDate.getTime();
        });
        break;
    }
  }

  addNewProject() {
    if (this.user) {
      this.dialog
        .open(NewProjectModalComponent, {
          data: {
            projectID: -1,
            userId: this.user.id,
            // firstName: this.user.firstName,
            // lastName: this.user.lastName,
          },
        })
        .afterClosed()
        .subscribe(
          () => this.getProjects(),
          (error) => {
            this.snackBar.open(
              `Error ${error.status}: ${error.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
          }
        );
    } else {
      this.snackBar.open(`Error not Logged in`, 'Ok', {
        duration: 1500,
      });
    }
  }

  onChangeSortOptionsPrivate(newSortOption: string) {
    this.selectedSortOption_private = newSortOption;
    this.sortPrivateProjects();
  }

  onChangeSortOptionsPublic(newSortOption: string) {
    this.selectedSortOption_private = newSortOption;
    this.sortPublicProjects();
  }

  /**
   * Return permission number from user right for this project
   */
  getProjectPermission(project: Project): number {
    if (project.projectStatus === ProjectStatus.PUBLIC) {
      return 1;
    } else {
      for (let i = 0; i < project.contributors.length; i++) {
        if (this.user) {
          if (project.contributors[i].user.id === this.user.id) {
            const userRight: Right = project.contributors[i].rights;
            switch (userRight) {
              case Right.READ:
                return 1;
              case Right.WRITE:
                return 2;
              case Right.MANAGE:
                return 3;
              case Right.OWN:
                return 3;
            }
          }
        } else {
          this.logger.error('not logged in');
        }
      }
    }
    return 0; // should never happen
  }

  /**
   * Return 0, if no tasks or only aborted tasks
   * 0.5, if there exists one running task
   * 1, if there exists no running task and there is one finished task
   */
  getProjectProgressBarValues(project: Project): {
    progress: number;
    status: string;
  } {
    if (project.tasks.length > 0) {
      const result = { progress: 0, status: 'aborted' };
      for (let i = 0; i < project.tasks.length; i++) {
        if (project.tasks[i].status.finished) {
          result.progress = 1;
          result.status = 'finished';
        } else if (project.tasks[i].status.running) {
          return { progress: 0.5, status: 'running' };
        }
      }
      return result;
    } else {
      return { progress: 0, status: 'No tasks started yet' };
    }
  }

  toggleExpand(id: string, initallyExpanded: boolean = true): void {
    const element = document.getElementById(id);

    if (element == null) {
      this.logger.error('expected element is null');
      return;
    }

    if (element.style.flexGrow == '')
      element.style.flexGrow = initallyExpanded ? '1' : '0';

    element.style.flexGrow = element.style.flexGrow == '0' ? '1' : '0';
  }
  setfilter(filter: string, event: MatCheckboxChange) {
    switch (filter) {
      case 'visible':
        this.filters.displayVisible = event.checked;
        if (event.checked) {
          this.getCurProj();
        }
        break;
      case 'privat':
        this.filters.displayPrivate = event.checked;
        break;
      case 'public':
        this.filters.displayPublic = event.checked;
        break;
    }
    this.Map.setFilter(this.filters);
  }
  getCurProj() {
    this.privateProjectsFiltered = this.Map.getPrivateProjectsFiltered();
    this.publicProjectsFiltered = this.Map.getPublicProjectsFiltered();
  }
}
