import { PipesModule } from '@energy-platform/shared/pipes';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { JobsComponent } from './jobs/jobs.component';
import { ExpansionItemComponent } from './jobs/expansion-item/expansion-item.component';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
// import { UserNameByIdPipe } from "./pipes/user-name-by-id.pipe";

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: JobsComponent },
    ]),
    MatExpansionModule,
    MatIconModule,
    MatTooltipModule,
    MatCardModule,
    TranslateModule,
    MatTableModule,
    PipesModule,
  ],
  declarations: [JobsComponent, ExpansionItemComponent],
})
export class JobsModule {}
