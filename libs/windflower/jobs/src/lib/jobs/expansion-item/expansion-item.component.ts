import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Task } from '@energy-platform/shared/interfaces';

@Component({
  selector: 'energy-platform-expansion-item',
  templateUrl: './expansion-item.component.html',
  styleUrls: ['./expansion-item.component.less'],
})
export class ExpansionItemComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  @Input()
  status!: string;
  @Input() jobs: Task[] = [];
  private sortedjobs: Task[] = [];

  // displayedColumns: string[] = ['projectName', 'jobType', 'startingTime', 'expectedEndTIme', 'expectedDuration', 'userCreator'];
  displayedColumns: string[] = [
    'projectName',
    'taskType',
    'startingTime',
    'expectedEndTIme',
    // "expectedDuration",
    'userCreator',
    'action',
  ];

  @Output() abortJobEmitter = new EventEmitter<string>();

  /****************
   * Subscriptions
   ****************/

  constructor() {
    super();
  }

  ngOnInit() {
    const sortByDate = (arr: Task[]) => {
      const sorter = (a: Task, b: Task) => {
        return new Date(a.times.start) > new Date(b.times.start) ? -1 : 1;
      };
      arr.sort(sorter);
    };
    this.sortedjobs = this.jobs;
    sortByDate(this.sortedjobs);
  }
  // itemClicked(row: Job) {
  //   this.logger.info(row);
  // }

  clearList(status: string) {
    this.jobs = [];
  }

  clearItemfromList(job: Task) {
    const jobToDelete = this.jobs.indexOf(job);
    const jobsCache = this.jobs;
    jobsCache.splice(jobToDelete, 1);
    this.jobs = [...jobsCache];
  }

  abortJob(job: Task) {
    this.abortJobEmitter.emit(job.id);
  }
}
