import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import { Component, OnInit } from '@angular/core';
import { JobStatus, TasksWithStatus } from '@energy-platform/shared/interfaces';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import {
  UserService,
  CalculationService,
} from '@energy-platform/shared/services';

@Component({
  selector: 'energy-platform-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.scss'],
})
export class JobsComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  tasksWithStatus!: Observable<TasksWithStatus[]>;

  constructor(
    private userService: UserService,
    private calculationService: CalculationService
  ) {
    super();
  }

  ngOnInit() {
    this.updateTasksWithStatus();
  }

  updateTasksWithStatus() {
    this.tasksWithStatus = this.userService.getOwnUserProfile().pipe(
      map((resp) => {
        const user = resp;
        const tasks = user.tasks;
        this.logger.info(' project overview', user);
        return [
          {
            status: JobStatus.INPROGRESS,
            tasks: tasks.filter((task) => task.status.running),
          },
          {
            status: JobStatus.DONE,
            tasks: tasks.filter((task) => task.status.finished),
          },
          {
            status: JobStatus.ABORTED,
            tasks: tasks.filter((task) => task.status.aborted),
          },
          // TODO: JobStatus.ERROR
        ];
      })
    );
  }

  abortJob(taskId: string) {
    this.subs.sink = this.calculationService
      .stopCalculation(taskId)
      .subscribe();
  }
}
