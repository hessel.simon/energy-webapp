import { Component } from '@angular/core';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
@Component({
  selector: 'energy-platform-landingpage',
  templateUrl: './landingpage.component.html',
  styleUrls: ['./landingpage.component.scss', './scenery.scss'],
})
export class LandingpageComponent extends UnsubscribeOnDestroyAdapter {
  constructor() {
    super();
  }

  pathTo(item: string): string {
    return `assets/images/scenery/${item}.svg`;
  }
}
