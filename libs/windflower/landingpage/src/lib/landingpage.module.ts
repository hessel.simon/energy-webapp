import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LandingpageComponent } from './components/landingpage/landingpage.component';
import { LayoutModule } from '@energy-platform/shared/layout';
// Material Components
import { MatIconModule } from '@angular/material/icon';

@NgModule({
  imports: [
    TranslateModule.forChild(),
    CommonModule,
    MatIconModule,
    LayoutModule,
    RouterModule.forChild([
      { path: '', pathMatch: 'full', component: LandingpageComponent },
    ]),
  ],
  declarations: [LandingpageComponent],
})
export class LandingpageModule {}
