import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'energy-platform-cable-type-row-field',
  templateUrl: './cable-type-row-field.component.html',
  styleUrls: ['./cable-type-row-field.component.scss'],
})
export class CableTypeRowFieldComponent implements OnInit {
  @Input() unit: string | undefined;
  @Input() min: number | undefined;
  @Input() max: number | undefined;
  @Input() step: number | undefined;
  @Input() value: number | undefined;
  @Input() disabled = false;
  @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

  input!: FormControl;

  constructor() {}

  ngOnInit() {
    if (!this.value) this.value = this.min;

    const validators = [];
    if (this.min !== undefined) validators.push(Validators.min(this.min));
    if (this.max !== undefined) validators.push(Validators.max(this.max));
    this.input = new FormControl('', validators);
  }

  onChangeInput(val: any) {
    // validate input each time value change
    this.value = val;
    this.valueChange.emit(val);
  }
}
