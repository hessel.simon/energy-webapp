import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CableType } from '@energy-platform/shared/interfaces';

@Component({
  selector: 'energy-platform-cable-type-row',
  templateUrl: './cable-type-row.component.html',
  styleUrls: ['./cable-type-row.component.scss'],
})
export class CableTypeRowComponent {
  @Input() value: CableType | undefined;
  @Input() disabled = false;
  @Output() valueChange = new EventEmitter<CableType>();
  @Output() delete = new EventEmitter<CableType>();

  constructor() {}

  removeCableType() {
    this.delete.emit(this.value);
  }

  changeValue(attribute: keyof CableType, newValue: number) {
    if (this.value) {
      this.value[attribute] = newValue;
    }
    this.valueChange.emit(this.value);
  }
}
