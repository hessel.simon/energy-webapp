import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CablingParameterComponent } from './cabling-parameter.component';

describe('CablingParameterComponent', () => {
  let component: CablingParameterComponent;
  let fixture: ComponentFixture<CablingParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [CablingParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CablingParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
