import {
  Component,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Cabling, Config } from '@energy-platform/shared/interfaces';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { ParameterSliderComponent } from '../parameter-slider/parameter-slider.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-cabling-parameter',
  templateUrl: './cabling-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './cabling-parameter.component.scss',
  ],
})
export class CablingParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  container: ViewContainerRef | undefined;
  cabling: Observable<Cabling | undefined>;
  private _cabling: BehaviorSubject<Cabling | undefined>;
  /**
   * enabled or disable the save button depending on the validation of the inputs
   */
  public validForSaving = false;
  /**
   * GET child elements containing the parameters
   */
  @ViewChild('pwrLossFactor') pwrLossFactor:
    | ParameterSliderComponent
    | undefined;
  @ViewChild('cblVoltg') cblVoltg: ParameterSliderComponent | undefined;
  @ViewChild('currFreq') currFreq: ParameterSliderComponent | undefined;
  @ViewChild('turbCableConCost') turbCableConCost:
    | ParameterSliderComponent
    | undefined;
  @ViewChild('safetyGen') safetyGen: ParameterSliderComponent | undefined;
  @ViewChild('autoBest') autoBest: ParameterSliderComponent | undefined;

  @Input() projectId: string | undefined;

  public $schema!: Observable<{
    cabling: {
      properties: {
        power_loss_percentage: Config;
        cable_voltage: Config;
        site_main_frequency: Config;
        cost_cable_connect: Config;
        cost_generator: Config;
        max_cable_types: Config;
      };
    };
  }>;

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._cabling = new BehaviorSubject<Cabling | undefined>(undefined);
    this.cabling = this._cabling.asObservable();
  }

  ngOnInit() {
    this.$schema = this.parameterService.getSchemas();
    this.validate();
    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'cabling')
        .subscribe((response) => {
          this._cabling.next(response.cabling);
        });
    }
  }

  onSave() {
    this.logger.info(' on saved clicked ');
    if (this.projectId) {
      this.projectService.setProjectParameters(this.projectId, {
        cabling: this._cabling.getValue(),
      });
    }
  }

  addCablingType() {
    const cables = this._cabling.getValue();
    if (cables) {
      cables.cable_types.push({
        ac_resistance: 0.01,
        cable_capacitance: 0.01,
        current_rating: 100,
        id: cables.cable_types.length + 1,
        insulation_loss_factor: 0.01,
        material_cost: 50,
      });
    }
    this._cabling.next(cables);
  }

  /**
   * remove cable types
   * @param i index of the cable type to remove
   */
  deleteCableType(i: number) {
    const cables = this._cabling.getValue();
    if (cables) {
      cables.cable_types.splice(i, 1);
      this._cabling.next(cables);
    }
  }

  /**
   * validate slider info
   */
  validate() {
    this.validForSaving = this.buttonValidator([
      this.pwrLossFactor,
      this.cblVoltg,
      this.currFreq,
      this.turbCableConCost,
      this.safetyGen,
      this.autoBest,
    ]);
  }
}
