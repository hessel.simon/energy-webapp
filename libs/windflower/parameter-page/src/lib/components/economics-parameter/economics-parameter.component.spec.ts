import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { EconomicsParameterComponent } from './economics-parameter.component';

describe('EconomicsParameterComponent', () => {
  let component: EconomicsParameterComponent;
  let fixture: ComponentFixture<EconomicsParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [EconomicsParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(EconomicsParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
