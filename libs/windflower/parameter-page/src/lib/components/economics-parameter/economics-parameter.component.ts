import { Component, Input, OnInit, Renderer2 } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import * as shape from 'd3-shape';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { BarChartModel } from '@energy-platform/shared/visualization';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { xAxisTicks } from './constants';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Config, Economics } from '@energy-platform/shared/interfaces';

@Component({
  selector: 'energy-platform-economics-parameter',
  templateUrl: './economics-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './economics-parameter.component.scss',
  ],
})
export class EconomicsParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  xAxisLabel = 'Timeslot';
  yAxisLabel = '€/MWh';
  xAxisTicks = xAxisTicks;

  curve: any = shape.curveStepAfter;
  priceTarifChartData: Array<any> | undefined;

  errorValueOutOfBounds = false;
  errorInvalidValue = false;

  @Input()
  projectId: string | undefined;

  private _economics: BehaviorSubject<Economics | undefined>;
  economics: Observable<Economics | undefined>;

  public $schema!: Observable<{
    economics: {
      properties: {
        cost_per_turbine_foundation: Config;
        project_management_cost: Config;
        cost_per_substation: Config;
        cost_per_turbine: Config;
        cable_laying_cost_per_cable_length: Config;
        cost_per_operation: Config;
        project_lifetime: Config;
        interest_rate: Config;
        energy_price_tariff: {
          items: Config;
        };
      };
    };
  }>;

  constructor(
    private translateService: TranslateService,
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar,
    private formBuilder: FormBuilder
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this.xAxisLabel = this.translateService.instant(
      'parameter.general.graphTimeslot'
    );

    //
    this._economics = new BehaviorSubject<Economics | undefined>(undefined);
    this.economics = this._economics.asObservable();
  }
  public sliderFormGroup: FormGroup | undefined;
  private chartDataSubject = new BehaviorSubject<BarChartModel[]>([
    {
      name: 'Total',
      series: [],
    },
  ]);
  public chartData$ = this.chartDataSubject.asObservable();

  ngOnInit() {
    this.$schema = this.parameterService.getSchemas();
    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'economics')
        .subscribe((response) => {
          this._economics.next(response.economics);
          this.priceTarifChartData = this.getPriceTarifChartData();
          this.sliderFormGroup = this.formBuilder.group({
            annualOACost: response.economics.cost_per_operation,
            costPerCablLen:
              response.economics.cable_laying_cost_per_cable_length,
            costPerTurb: response.economics.cost_per_turbine,
            substCost: response.economics.cost_per_substation,
            projManagmCost: response.economics.project_management_cost,
          });
          // Change of chart when changing values from sliders
          this.sliderFormGroup.valueChanges.subscribe((values) => {
            this.chartDataSubject.next([
              {
                name: 'Total',
                series: [
                  {
                    name: 'Annual Maintenance',
                    value: values.annualOACost,
                  },
                  {
                    name: 'Cable',
                    value: values.costPerCablLen,
                  },
                  {
                    name: 'Turbine',
                    value: values.costPerTurb,
                  },
                  {
                    name: 'Substation',
                    value: values.substCost,
                  },
                  {
                    name: 'Project Management',
                    value: values.projManagmCost,
                  },
                ],
              },
            ]);
          });
          this.sliderFormGroup.updateValueAndValidity();
        });
    }
  }

  getInputValue(index: number) {
    const ecom = this._economics.getValue();
    return ecom ? ecom.energy_price_tariff[index] : null;
  }

  getPriceTarifChartData(): Array<any> {
    const ecom = this._economics.getValue();

    if (ecom) {
      const data = [
        {
          name: 'Tarif',
          series: ecom.energy_price_tariff.map(function (x, i) {
            const rObj = { name: i, value: x };
            return rObj;
          }),
        },
      ];
      data[0].series.push({ name: 24, value: data[0].series[23]['value'] });
      return data;
    }
    return [];
  }

  onSubmit() {
    const ecom = this._economics.getValue();

    /*
    // implemente below
    for (let i = 0; i < ecom.energy_price_tariff.length; i++) {
      const slot = ecom.energy_price_tariff[i];
      // Check if slot is a number
      if (!+slot) {
        // Slot is not a valid number
        this.errorInvalidValue = true;
        return;
      }
      // Slot is a valid number
      ecom.energy_price_tariff[i] = +slot;
    }*/

    this._economics.next(ecom);
    // Rerender plot
    this.priceTarifChartData = this.getPriceTarifChartData();
  }

  onChange(event: any, index: number) {
    // Reset error values
    this.errorValueOutOfBounds = false;
    this.errorInvalidValue = false;

    const ecom = this._economics.getValue();
    /* const max =
      this.schema.economics.properties.energy_price_tariff.items.maximum;
    const min =
      this.schema.economics.properties.energy_price_tariff.items.minimum; */

    this.subs.sink = this.$schema.subscribe((schema) => {
      const max = schema.economics.properties.energy_price_tariff.items.maximum;
      const min = schema.economics.properties.energy_price_tariff.items.minimum;

      const slot = event.target.value;
      this.logger.info('slot value: ', slot);

      if (ecom) {
        //check if value is valid
        if (!+slot) {
          // Slot is not a valid number
          this.errorInvalidValue = true;
          ecom.energy_price_tariff[index] = min;
          return;
        }
        // Check if value is out of bounds
        if (slot > max) {
          this.errorValueOutOfBounds = true;
          ecom.energy_price_tariff[index] = max;
        } else if (slot < min) {
          this.errorValueOutOfBounds = true;
          ecom.energy_price_tariff[index] = min;
        } else {
          ecom.energy_price_tariff[index] = slot;
        }
        event.target.value = ecom.energy_price_tariff[index];
      }
    });

    this._economics.next(ecom);
    this.onSubmit();
  }

  // Necessary function to track index of input fields in html template
  trackByFn(index: any, item: any) {
    return index;
  }

  onSave() {
    if (this.projectId) {
      this.projectService.setProjectParameters(this.projectId, {
        economics: this._economics.getValue(),
      });
    }
  }
}
