import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MeteoParameterComponent } from './meteo-parameter.component';

describe('MeteoParameterComponent', () => {
  let component: MeteoParameterComponent;
  let fixture: ComponentFixture<MeteoParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [MeteoParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(MeteoParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
