import { Component, OnInit, Renderer2, Input, ViewChild } from '@angular/core';

import {
  WindWeibullJSON,
  WindBinsJSON,
} from '@energy-platform/shared/visualization';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { UploadDialogComponent } from '@energy-platform/shared/dialog';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { Config, Meteo } from '@energy-platform/shared/interfaces';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-meteo-parameter',
  templateUrl: './meteo-parameter.component.html',
  styleUrls: [
    './meteo-parameter.component.scss',
    '../../parameter-page/parameter-page.component.scss',
  ],
})
export class MeteoParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  @ViewChild('wind_measurement_height') wmh: any;
  @ViewChild('number_direction_slots') nds: any;
  @Input()
  projectId: string | undefined;
  meteo: Observable<Meteo | undefined>;
  private _meteo: BehaviorSubject<Meteo | undefined>;

  public $schema!: Observable<{
    meteo: {
      properties: {
        wind_measurement_height: Config;
        number_direction_slots: Config;
      };
    };
  }>;

  windBins = {
    wind_measurement_height: WindBinsJSON.wind_measurement_height,
    data: WindBinsJSON.data,
    direction_probability: WindBinsJSON.direction_probability,
    directions: WindBinsJSON.directions,
  };

  windWeibull = {
    comment: WindWeibullJSON.comment,
    comment_wind_measurement_height:
      WindWeibullJSON.comment_wind_measurement_height,
    wind_measurement_height: WindWeibullJSON.wind_measurement_height,
    weibull: WindWeibullJSON.weibull,
  };

  selected = {
    theta: 0,
    lower: -15,
    upper: 15,
  };

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar,
    private dialog: MatDialog
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._meteo = new BehaviorSubject<Meteo | undefined>(undefined);
    this.meteo = this._meteo.asObservable();
  }

  ngOnInit() {
    this.$schema = this.parameterService.getSchemas();

    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'meteo')
        .subscribe((response) => {
          this._meteo.next(response.meteo);
        });
    }
  }

  uploadDirectionMeasurements() {
    this.dialog.open(UploadDialogComponent, {
      width: '400px',
    });
  }

  uploadSpeedMeasurements() {
    this.dialog.open(UploadDialogComponent, {
      width: '400px',
    });
  }

  uploadEnergyPlusFile() {
    this.dialog.open(UploadDialogComponent, {
      width: '400px',
    });
  }

  generateWindFile() {
    // Backend Call
  }

  onSave() {
    if (this.projectId) {
      this.projectService.setProjectParameters(this.projectId, {
        meteo: this._meteo.getValue(),
      });
    }
  }
}
