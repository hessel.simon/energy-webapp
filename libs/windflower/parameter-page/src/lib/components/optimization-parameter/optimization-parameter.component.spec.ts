import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { OptimizationParameterComponent } from './optimization-parameter.component';

describe('OptimizationParameterComponent', () => {
  let component: OptimizationParameterComponent;
  let fixture: ComponentFixture<OptimizationParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [OptimizationParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(OptimizationParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
