import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { Component, OnInit, Renderer2, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ProjectsService,
  ParameterService,
} from '@energy-platform/shared/services';
import { Config, Optimization } from '@energy-platform/shared/interfaces';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-optimization-parameter',
  templateUrl: './optimization-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './optimization-parameter.component.scss',
  ],
})
export class OptimizationParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  @Input()
  public projectId: string | undefined;

  public optimization: Observable<Optimization | undefined>;

  public $schema!: Observable<{
    optimization: {
      properties: {
        local_search: {
          properties: {
            termination_relative_convergence: Config;
            num_circles: Config;
            radial_distance: Config;
            num_circle_positions: Config;
            iteration_range: { items: Config };
          };
        };
        spiral_grid: {
          properties: {
            density_interval: { items: Config };
            relative_zoom_interval: { items: Config };
            vertical_stretch_interval: { items: Config };
          };
        };
        slanted_grid: {
          properties: {
            shearing_angle_interval: { items: Config };
            rows_distance_interval: { items: Config };
            cols_distance_interval: { items: Config };
          };
        };
        honeycomb_grid: {
          properties: {
            expansion_interval: { items: Config };
          };
        };
        hexagonal_grid: {
          properties: {
            rotation_angle_interval: { items: Config };
            hexagon_side_interval: { items: Config };
            vertical_stretch_interval: { items: Config };
          };
        };
        close_packing: {
          properties: {
            number_threshold_steps: Config;
            interpolated_positions_per_side: Config;
            matrix_simulations_per_side: Config;
            threshold_range: { items: Config };
          };
        };
        multi_step: {
          properties: {
            pattern_combinations_per_parameter: Config;
            num_turbines: Config;
          };
        };
      };
    };
  }>;

  private _optimization: BehaviorSubject<Optimization | undefined>;

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._optimization = new BehaviorSubject<Optimization | undefined>(
      undefined
    );
    this.optimization = this._optimization.asObservable();
  }

  ngOnInit() {
    this.$schema = this.parameterService.getSchemas();
    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'optimization')
        .subscribe((response) => {
          this._optimization.next(response.optimization);
          if (
            response.optimization.multi_step.optimization_steps.length === 2
          ) {
            response.optimization.multi_step.optimization_steps.pop();
            this._optimization.next(response);
          }
        });
    }
  }

  onSave() {
    if (this.projectId) {
      this.logger.info(this.optimization);
      this.projectService.setProjectParameters(this.projectId, {
        optimization: this._optimization.getValue(),
      });
    }
  }
}
