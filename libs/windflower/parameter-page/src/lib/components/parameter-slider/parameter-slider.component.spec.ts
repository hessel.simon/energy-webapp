import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { ParameterSliderComponent } from './parameter-slider.component';

describe('ParameterSliderComponent', () => {
  let component: ParameterSliderComponent;
  let fixture: ComponentFixture<ParameterSliderComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ParameterSliderComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ParameterSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
