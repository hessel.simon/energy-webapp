import {
  Component,
  OnInit,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MatSlider } from '@angular/material/slider';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'energy-platform-parameter-slider',
  templateUrl: './parameter-slider.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './parameter-slider.component.scss',
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ParameterSliderComponent),
      multi: true,
    },
  ],
})
export class ParameterSliderComponent implements OnInit, ControlValueAccessor {
  @Input() id: string | undefined;
  @Input() title: string | undefined;
  @Input() unit: string | undefined;
  @Input() min: number | undefined;
  @Input() max: number | undefined;
  @Input() step: number | undefined;
  @Input() value: number | undefined;
  @Input() disabled = false;
  @Output() valueChange: EventEmitter<number> = new EventEmitter<number>();

  input!: FormControl;

  @ViewChild('slider', { static: true })
  public slider: ElementRef<MatSlider> | undefined;
  private onChange = (value: number) => {};

  constructor() {}

  writeValue(obj: any): void {
    if (this.value !== obj) {
      this.value = obj;
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {}

  setDisabledState?(isDisabled: boolean): void {}

  get commaValue() {
    return this.value ? this.numberWithCommas(this.value) : '';
  }

  // instead of using a comma: sep = ","
  // a space was used upon Pascal's request.
  // the comma was returned again, upon Pascal's request.
  numberWithCommas(x: number, sep = ',') {
    return x % 1 === 0 ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep) : x;
  }

  validator(slider: any) {
    if (slider.value <= slider.max && slider.value >= slider.min) return false;
    return true;
  }

  ngOnInit() {
    if (!this.value) this.value = this.min;

    const validators = [];
    if (this.min !== undefined) validators.push(Validators.min(this.min));
    if (this.max !== undefined) validators.push(Validators.max(this.max));
    this.input = new FormControl('', validators);
  }

  onChangeInput(sliderValue: number) {
    this.value = sliderValue;
    this.valueChange.emit(this.value);
    this.onChange(sliderValue);
  }
}
