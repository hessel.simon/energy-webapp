import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import {
  Project,
  ProjectParameter,
  Task,
  TaskTypes,
} from '@energy-platform/shared/interfaces';
import {
  CalculationService,
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { BarChartModel } from '@energy-platform/shared/visualization';
import { circle, LatLng, latLng, polygon, tileLayer } from 'leaflet';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

/*************************************************************************
 *   Utility Types
 **************************************************************************/

enum BOXPLOTTYPE {
  AEP = 'annual_efficiency',
  IROR = 'internal_rate_of_return',
  LCOE = 'levelized_cost_of_energy',
  NAEP = 'net_annual_energy_production',
  NPV = 'net_present_value',
  PP = 'payback_period',
}

interface ITaskSummary {
  id: string;
  projectId?: string;
  type?: TaskTypes;
  runningStatus: boolean;
}

interface ICalculationTasks {
  simulation: ITaskSummary | undefined;
  optimization: ITaskSummary | undefined;
  sensitivityAnalysis: ITaskSummary | undefined;
}

@Component({
  selector: 'energy-platform-result-page',
  templateUrl: './public-result-page.component.html',
  styleUrls: ['./public-result-page.component.scss'],
})
export class PublicResultPageComponent implements OnInit {
  // TODO replace with service call to check if parameters have changed when opening the page
  simulationParameterChanged = false;
  optimizationParameterChanged = true;
  sensitivityParametersChange = true;

  // Set initial state of the buttons to false0
  sensitivityAnalysIsRunning = false;
  optimizationIsRunning = false;
  simulationIsRunning = false;

  // extract name of the project
  nameOfProject = '';

  // End of TODO
  stlFile: Task | undefined;
  print: any;
  project!: Project;
  project$: Observable<Project>;
  _project: BehaviorSubject<any>;
  print_restricted_area = false;
  print_topography = false;
  print_logo = false;
  wind_direction = 1;

  id = '';
  userId = '';
  private _results: BehaviorSubject<ProjectParameter>;
  results$: Observable<ProjectParameter>;
  projectId = '';
  releaseId = '';

  BoxplotTypeEnum = BOXPLOTTYPE;
  boxplotInputQuantils: number[] = [];

  projectCalculations: BehaviorSubject<any>;
  projectCalculations$: Observable<any>;

  //simulationResults
  //values needed for windrose chart
  energyProductionData: any[] = [];

  energyProductionConfig = {};
  energyProductionLayout = {};

  //values needed for economics stack chart
  economicsData: any[] = [];
  economicsConfig: { displayModeBar: boolean } | undefined;
  economicsLayout:
    | {
        title: string;
        barmode: string;
        showlegend: boolean;
        legend: { orientation: string };
        yaxis: { fixedrange: boolean };
        xaxis: { fixedrange: boolean };
      }
    | undefined;

  // economicsStackData: BarChartModel[];

  //optimizationResults
  optimizationBarData: BarChartModel[] | undefined;

  // leaflet related
  leafletOptions: any;
  leafletLayers: any[] | undefined;
  mapCenter: LatLng | undefined;
  simulationChanged = false;
  optimizationChanged = false;
  sensitivityAnalysisChanged = false;
  projectParameter: any;
  /***********************************************************************
   *    Notification (snakbar)
   ***********************************************************************/
  private message =
    'Taskt was created successfully, check the jobs page to see the status of the Task';
  private notificationDuration = 6000;

  /***********************************************************************
   *   Structures to keep track of the task status
   ***********************************************************************/
  private _calculationsTasks: BehaviorSubject<ICalculationTasks>;
  calculationsTasks: Observable<ICalculationTasks>;

  // at any point in time there, we can just have one running for a given
  // project. This variable keep track on the active task
  activeTask: ITaskSummary | undefined;

  /*************************************************************************
   * Interval ( this variable store an interval with poll information  )
   ***********************************************************************/
  private POLL_BACKEND: any;

  /************************************************************************
   *  Ends attributes declaration
   ************************************************************************/
  // this variables are needed to keep track tof all the tasks

  constructor(
    private projectService: ProjectsService,
    private activatedRoute: ActivatedRoute,
    private calculationService: CalculationService,
    private snackBar: MatSnackBar,
    private parameterService: ParameterService
  ) {
    this.projectCalculations = new BehaviorSubject<any>({});
    this.projectCalculations$ = this.projectCalculations.asObservable();
    this._project = new BehaviorSubject<any>({});
    this.project$ = this._project.asObservable();
    this._calculationsTasks = new BehaviorSubject<ICalculationTasks>({
      simulation: undefined,
      optimization: undefined,
      sensitivityAnalysis: undefined,
    });
    this.calculationsTasks = this._calculationsTasks.asObservable();

    // load active task form localstrorage

    this._results = new BehaviorSubject<ProjectParameter>(
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      {} as ProjectParameter
    );
    this.results$ = this._results.asObservable();
  }

  ngOnInit() {
    this.getProjectIdAndReleaseId()
      .pipe(take(1))
      .subscribe(({ projectId, releaseId }) => {
        this.projectId = projectId;
        this.releaseId = releaseId;
        this.projectService
          .getPublicReleasedProjectsById(this.projectId)
          .subscribe((response) => {
            const { version } = response.releases.filter(
              (rel: { id: any }) => rel.id === releaseId
            )[0];
            this.nameOfProject = `${response.name} - ${version}`;
          });
        this.updatesResultsData(this.projectId, this.releaseId);
      });
  }

  // Needed
  private updatesResultsData(projectId: string, releaseId: string) {
    this.parameterService
      .getParameterOfRelease(projectId, releaseId)
      .subscribe((projectParameters) => {
        this._results.next(projectParameters);

        this.leafLetOptimizationMap(projectParameters);
        this.optimizationBarData =
          this.extractOptimizaionBarData(projectParameters);
        //this.createEnergyWindroseChart(projectParameters);
        this.createCostStackedBarChart(projectParameters);

        this.print = projectParameters.printer3d_result.slice(1, -1);
      });
  }

  // Needed
  private getProjectIdAndReleaseId(): Observable<any> {
    return this.activatedRoute.paramMap.pipe(
      map((params) => {
        const projectId = params.get('id');
        const releaseId = params.get('releaseId');
        return { projectId, releaseId };
      })
    );
  }
  // Needed
  private leafLetOptimizationMap(projectParameters: ProjectParameter) {
    const site = projectParameters.site;
    if (site) {
      const fieldBoundaries = site.field_boundaries_long_lat.map((longLat) =>
        latLng(longLat[1], longLat[0])
      );
      const restrictedAreas = site.restricted_areas_long_lat.map(
        (restrictedArea) =>
          restrictedArea.map((longLat) => latLng(longLat[1], longLat[0]))
      );
      const substations = site.substations_long_lat.map((longLat) =>
        latLng(longLat[1], longLat[0])
      );
      const mainLayer = tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      );
      const positions = projectParameters.positions
        .trimRight()
        .split('\n')
        .map((position) => {
          const splitted = position.split(';');
          return latLng(
            Number.parseFloat(splitted[0]),
            Number.parseFloat(splitted[1])
          );
        });
      this.leafletOptions = {
        center: this.calculateCentroid(fieldBoundaries),
        zoom: 10,
        layers: [mainLayer],
      };
      this.mapCenter = this.calculateCentroid(fieldBoundaries);
      const restrictedAreaPolygons = restrictedAreas.map((restrictedArea) =>
        polygon(restrictedArea, { color: 'red' })
      );
      const substationCircles = substations.map((substation) =>
        circle(substation, { color: 'yellow', radius: 5 })
      );
      const positionCircles = positions.map((position) =>
        circle(position, { color: 'green', radius: 5 })
      );
      this.leafletLayers = [
        polygon(fieldBoundaries),
        restrictedAreaPolygons,
        substationCircles,
        positionCircles,
      ];
    }
  }

  // Needed
  calculateCentroid(coordinates: any[]) {
    let area = 0;
    let long = 0;
    let lat = 0;

    const xs = coordinates.map((c: { lng: any }) => c.lng);
    xs.push(xs[0]);
    const ys = coordinates.map((c: { lat: any }) => c.lat);
    ys.push(ys[0]);

    for (let i = 0; i < coordinates.length; i += 1) {
      area += xs[i] * ys[i + 1] - xs[i + 1] * ys[i];
      long += (xs[i] + xs[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
      lat += (ys[i] + ys[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
    }

    area /= 2;
    long /= 6 * area;
    lat /= 6 * area;

    return latLng(lat, long);
  }

  // Needed
  // eslint-disable-next-line @typescript-eslint/member-ordering
  private setting = {
    element: {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      dynamicDownload: null as unknown as HTMLElement,
    },
  };

  //Needed
  downloadParametersAsZip() {
    const route = this.projectService.downloadParamsAndResultsAsZip(
      this.projectId,
      this.releaseId
    );
    window.open(route, '_blank');
  }

  // Needed
  downloadParameters() {
    this.results$.subscribe((res) => {
      this.dyanmicDownloadByHtmlTag({
        fileName: 'projectParameters.json',
        text: JSON.stringify(res),
      });
    });
  }

  // Needed
  private dyanmicDownloadByHtmlTag(arg: { fileName: string; text: string }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType =
      arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/stl';
    element.setAttribute(
      'href',
      `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`
    );
    element.setAttribute('download', arg.fileName);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }

  /**************************************************************************************
   * Utility fonctions
   **************************************************************************************/

  download3DPrint() {
    this.results$.subscribe((res) => {
      this.dyanmicDownloadByHtmlTag({
        fileName: '3DPrint.stl',
        text: this.print,
      });
    });
  }

  // Needed
  private normalizeData(a: number, b: number) {
    return Math.max(0, (a / b) * 100);
  }

  // Needed
  private extractOptimizaionBarData(projectParameters: ProjectParameter) {
    const optimizationResult =
      projectParameters.optimization_result.final_result.economics;
    const simulationResult =
      projectParameters.simulation_result.final_result.economics;

    return [
      {
        name: 'Annual Energie Production',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.net_annual_energy_production,
              simulationResult.net_annual_energy_production
            ),
          },
        ],
      },
      {
        name: 'Internal rate of return',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.internal_rate_of_return,
              simulationResult.internal_rate_of_return
            ),
          },
        ],
      },
      {
        name: 'Levelized energy cost',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.levelized_cost_of_energy,
              simulationResult.levelized_cost_of_energy
            ),
          },
        ],
      },
      {
        name: 'Net present value',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.net_present_value,
              simulationResult.net_present_value
            ),
          },
        ],
      },
      {
        name: 'Total investment costs',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.investment_costs,
              simulationResult.investment_costs
            ),
          },
        ],
      },
    ];
  }

  // Needed
  /* private createEnergyWindroseChart(projectParameters: ProjectParameter) {
    const aep_inputs = [];
    const aep_inputs_directions = [];
    let sum = 0;
    let grandTotal = 0;
    const numberOfDirections =
      projectParameters.simulation_result.final_result['matrix_index_direction']
        .length;
    //this.logger.info("Number of directions "+numberOfDirections);
    this.logger.info('Project Parameters: ', projectParameters);
    const degreeStepInWindrose = 15;
    const degreeInWindrose = 360; //Degree in a circle
    const jumpsizeInWindrose =
      degreeStepInWindrose * (numberOfDirections / degreeInWindrose);
    let max = 0; // used to find the maximum value
    //this.logger.info("jumpsizeInWindrose: "+jumpsizeInWindrose);
    for (
      let i = 0;
      i <
      projectParameters.simulation_result.final_result[
        'matrix_direction_speed_revenue'
      ].length;
      i++
    ) {
      for (
        let k = 0;
        k <
        projectParameters.simulation_result.final_result[
          'matrix_direction_speed_revenue'
        ][i].length;
        k++
      ) {
        sum +=
          projectParameters.simulation_result.final_result[
            'matrix_direction_speed_revenue'
          ][i][k];
      }
      grandTotal += sum;
      aep_inputs.push(sum);
      max = sum > max ? sum : max;
      sum = 0;
    }

    let temp = 1;
    let maxtemp = max * 1.1;
    while (Math.floor(maxtemp / 10) > 0) {
      maxtemp /= 10;
      temp *= 10;
    }

    const tickStart = temp / 2;
    const tickStepsize = temp / 2;

    grandTotal = parseFloat(grandTotal.toFixed(3));

    for (let i = 0; i < numberOfDirections; i++) {
      //indicate the directions with degree
      aep_inputs_directions.push(
        parseFloat(
          projectParameters.simulation_result.final_result[
            'matrix_index_direction'
          ][i]
        ).toFixed(3) + '°'
      );
    }

    this.energyProductionData = [
      {
        r: aep_inputs,
        theta: aep_inputs_directions,
        marker: { color: 'rgb(0,85,102)' },
        type: 'barpolar',
      },
    ];
    this.energyProductionConfig = { displayModeBar: false };
    this.energyProductionLayout = {
      title: grandTotal + '	[GWh]', //Add the unit to the grandTotal
      autosize: true,
      polar: {
        barmode: 'stack',
        bargap: 0.01,
        radialaxis: { tick0: tickStart, dtick: tickStepsize },
        angularaxis: {
          direction: 'clockwise',
          tick0: 0,
          dtick: jumpsizeInWindrose,
        },
        yaxis: { fixedrange: true },
        xaxis: { fixedrange: true },
      },
      margin: {
        l: 40,
        r: 40,
        t: 60,
        b: 60,
      },
    };
  } */

  // Needed
  createCostStackedBarChart(projectParameters: ProjectParameter) {
    const costs_cablingCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .cabling_costs,
      ],
      name: 'Cabling Costs',
      type: 'bar',
      marker: { color: '#002a38' },
    };
    const costs_turbineCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .foundation_costs,
      ],
      name: 'Turbine Costs',
      type: 'bar',
      marker: { color: '#004e5e' },
    };
    const costs_substationCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .substation_costs,
      ],
      name: 'Substation Costs',
      type: 'bar',
      marker: { color: '#80aab3' },
    };

    const grandTotal =
      costs_cablingCosts.y[0] +
      costs_turbineCosts.y[0] +
      costs_substationCosts.y[0];
    //this.logger.info("Costs grandTotal: " + grandTotal);
    this.economicsData = [
      costs_cablingCosts,
      costs_turbineCosts,
      costs_substationCosts,
    ];
    this.economicsConfig = { displayModeBar: false };
    this.economicsLayout = {
      title:
        parseFloat('' + grandTotal / Math.pow(10, 6)).toFixed(3) + ' Mio €',
      barmode: 'stack',
      showlegend: true,
      legend: { orientation: 'h' },
      yaxis: { fixedrange: true },
      xaxis: { fixedrange: true },
    };
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }
}
