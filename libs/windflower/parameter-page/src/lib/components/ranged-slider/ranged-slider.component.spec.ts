import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RangedSliderComponent } from './ranged-slider.component';

describe('RangedSliderComponent', () => {
  let component: RangedSliderComponent;
  let fixture: ComponentFixture<RangedSliderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RangedSliderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RangedSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
