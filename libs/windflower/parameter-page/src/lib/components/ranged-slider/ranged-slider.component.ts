import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Options } from '@angular-slider/ngx-slider';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'energy-platform-ranged-slider',
  templateUrl: './ranged-slider.component.html',
  styleUrls: [
    './ranged-slider.component.scss',
    '../../parameter-page/parameter-page.component.scss',
  ],
})
export class RangedSliderComponent implements OnInit {
  @Input() min: number | undefined;
  @Input() max: number | undefined;
  @Input() unit: string | undefined;
  @Input() step: number | undefined;
  minValue: number | undefined;
  maxValue: number | undefined;
  options: Options | undefined;
  @Output() minValueChange: EventEmitter<number> = new EventEmitter<number>();
  @Output() maxValueChange: EventEmitter<number> = new EventEmitter<number>();

  inputMin!: FormControl;
  inputMax!: FormControl;

  constructor() {}

  ngOnInit(): void {
    this.options = {
      floor: this.min,
      ceil: this.max,
      step: this.step,
    };
    if (this.min && this.max) {
      this.minValue = Math.floor(this.min + Math.abs(this.max) * 0.2);
      this.maxValue = Math.ceil(this.max - Math.abs(this.max) * 0.2);

      const validators = [];
      if (this.min !== undefined) validators.push(Validators.min(this.min));
      if (this.max !== undefined) validators.push(Validators.max(this.max));
      this.inputMin = new FormControl('', validators);
      this.inputMax = new FormControl('', validators);
    }
  }

  onChangeMinInput() {
    this.minValueChange.emit(this.minValue);
  }

  onChangeMaxInput() {
    this.maxValueChange.emit(this.maxValue);
  }
}
