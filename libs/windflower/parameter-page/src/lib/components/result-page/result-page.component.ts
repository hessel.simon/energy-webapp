import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit, Renderer2 } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { PublishWarningComponent } from '@energy-platform/shared/dialog';
import {
  Project,
  ProjectParameter,
  ProjectStatus,
  Task,
  TaskTypes,
} from '@energy-platform/shared/interfaces';
import {
  CalculationService,
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { BarChartModel } from '@energy-platform/shared/visualization';
import { circle, LatLng, latLng, polygon, tileLayer } from 'leaflet';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

/*************************************************************************
 *   Utility Types
 **************************************************************************/

enum BOXPLOTTYPE {
  AEP = 'annual_efficiency',
  IROR = 'internal_rate_of_return',
  LCOE = 'levelized_cost_of_energy',
  NAEP = 'net_annual_energy_production',
  NPV = 'net_present_value',
  PP = 'payback_period',
}

interface ITaskSummary {
  id: string;
  projectId?: string;
  type?: TaskTypes;
  runningStatus: boolean;
}

interface ICalculationTasks {
  simulation: ITaskSummary | undefined;
  optimization: ITaskSummary | undefined;
  sensitivityAnalysis: ITaskSummary | undefined;
}

@Component({
  selector: 'energy-platform-result-page',
  templateUrl: './result-page.component.html',
  styleUrls: ['./result-page.component.scss'],
})
export class ResultPageComponent implements OnInit, OnDestroy {
  // TODO replace with service call to check if parameters have changed when opening the page
  simulationParameterChanged = false;
  optimizationParameterChanged = true;
  sensitivityParametersChange = true;

  // Set initial state of the buttons to false0
  sensitivityAnalysIsRunning = false;
  optimizationIsRunning = false;
  simulationIsRunning = false;

  // extract name of the project
  nameOfProject = '';

  // End of TODO
  stlFile: Task | undefined;
  print: any;
  project!: Project;
  project$: Observable<Project>;
  _project: BehaviorSubject<any>;
  print_restricted_area = false;
  print_topography = false;
  print_logo = false;
  wind_direction = 1;

  id: string | undefined;
  userId: string | undefined;
  private _results: BehaviorSubject<ProjectParameter>;
  results$: Observable<ProjectParameter>;
  projectId!: string;

  BoxplotTypeEnum = BOXPLOTTYPE;
  boxplotInputQuantils: number[] = [];

  projectCalculations: BehaviorSubject<any>;
  projectCalculations$: Observable<any>;

  //simulationResults
  //values needed for windrose chart
  //simulationResults
  //values needed for windrose chart
  energyProductionData: any[] = [];

  energyProductionConfig = {};
  energyProductionLayout = {};

  //values needed for economics stack chart
  //values needed for economics stack chart
  economicsData: any[] = [];
  economicsConfig = { displayModeBar: true };
  economicsLayout:
    | {
        title: string;
        barmode: 'stack';
        showlegend: boolean;
        legend: { orientation: 'h' };
        yaxis: { fixedrange: true };
        xaxis: { fixedrange: true };
      }
    | undefined;

  // economicsStackData: BarChartModel[];

  //optimizationResults
  // economicsStackData: BarChartModel[];
  //optimizationResults
  optimizationBarData: BarChartModel[] = [];

  // leaflet related
  leafletOptions: any;
  leafletLayers: any[] = [];
  mapCenter: LatLng | undefined;
  simulationChanged: boolean | undefined;
  optimizationChanged: boolean | undefined;
  sensitivityAnalysisChanged: boolean | undefined;
  projectParameter: any;

  /***********************************************************************
   *    Notification (snakbar)
   ***********************************************************************/
  private message =
    'Taskt was created successfully, check the jobs page to see the status of the Task';
  private notificationDuration = 6000;

  /***********************************************************************
   *   Structures to keep track of the task status
   ***********************************************************************/
  private _calculationsTasks: BehaviorSubject<ICalculationTasks>;
  calculationsTasks: Observable<ICalculationTasks>;

  // at any point in time there, we can just have one running for a given
  // project. This variable keep track on the active task
  activeTask!: ITaskSummary;

  /*************************************************************************
   * Interval ( this variable store an interval with poll information  )
   ***********************************************************************/
  private POLL_BACKEND: any;

  /************************************************************************
   *  Ends attributes declaration
   ************************************************************************/
  // this variables are needed to keep track tof all the tasks

  constructor(
    private projectService: ProjectsService,
    private renderer: Renderer2,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private calculationService: CalculationService,
    private snackBar: MatSnackBar,
    private parameterService: ParameterService,
    private dialog: MatDialog,
    private http: HttpClient
  ) {
    this.projectCalculations = new BehaviorSubject<any>({});
    this.projectCalculations$ = this.projectCalculations.asObservable();
    this._project = new BehaviorSubject<any>({});
    this.project$ = this._project.asObservable();
    this._calculationsTasks = new BehaviorSubject<ICalculationTasks>({
      simulation: undefined,
      optimization: undefined,
      sensitivityAnalysis: undefined,
    });
    this.calculationsTasks = this._calculationsTasks.asObservable();
    // load active task form localstrorage

    this._results = new BehaviorSubject<ProjectParameter>(
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      {} as ProjectParameter
    );
    this.results$ = this._results.asObservable();

    this.POLL_BACKEND = setInterval(() => {
      const { id: taskId, runningStatus } = this.activeTask;
      if (taskId !== '' && runningStatus) {
        this.calculationService
          .getCalculationStatus(taskId)
          .subscribe((response) => {
            const status = response;
            this.activeTask.runningStatus = status.running;
            if (!status.running && this.activeTask.projectId) {
              if (!status.aborted && this.activeTask.type) {
                this.snackBar.open('Task completed sucessfully ', 'OK', {
                  duration: 2000,
                });
                this.projectId &&
                  this.updateSingleResultsProperty(this.activeTask.type);
              }
              this.updatesResultsData(this.activeTask.projectId);
            }
            this.saveActiveTaskIdFromLocalStorage(this.activeTask);
          });
      }
    }, 3000);
  }

  ngOnInit() {
    this.getProjectId()
      .pipe(take(1))
      .subscribe((projectId) => {
        //
        this.projectId = projectId;
        // get activeTak from cache
        this.activeTask = this.loadActiveTaskIdFromLocalStorage(projectId);
        this.saveActiveTaskIdFromLocalStorage(this.activeTask);
        this.getProject(projectId).subscribe((project) => {
          this.nameOfProject = project.name;
          this._project.next(project);

          this.updatesResultsData(projectId);

          this.project = project;
          project.projectStatus;
        });
      });
  }

  private updateSingleResultsProperty(type: TaskTypes) {
    switch (type) {
      case TaskTypes.optimization:
        this.parameterService
          .getParameter(this.projectId, 'optimization_result')
          .subscribe(({ optimization_result }) => {
            const params = this._results.getValue();
            params.optimization_result = optimization_result;
            this.optimizationBarData = this.extractOptimizaionBarData(params);
            this._results.next(params);
          });
        break;
      case TaskTypes.simulation:
        this.parameterService
          .getParameter(this.projectId, 'simulation_result')
          .subscribe(({ simulation_result }) => {
            const params = this._results.getValue();
            params.simulation_result = simulation_result;
            // update the windrose chart
            //this.createEnergyWindroseChart(params);
            // update the total construction cost chart
            this.createCostStackedBarChart(params);
            this._results.next(params);
          });
        break;
      case TaskTypes.sensitivityAnalysis:
        this.parameterService
          .getParameter(this.projectId, 'optimization_result')
          .subscribe(({ sensitivityAnalysis }) => {
            const params = this._results.getValue();
            params.sensitivityAnalysis = sensitivityAnalysis;
            this._results.next(params);
          });
        break;
    }
  }

  private updatesResultsData(projectId: string) {
    this.getProjectParameter(projectId).subscribe((projectParameters) => {
      this._results.next(projectParameters);

      this.leafLetOptimizationMap(projectParameters);
      // this.economicsStackData = this.extractEconomicsData(projectParameters);
      this.optimizationBarData =
        this.extractOptimizaionBarData(projectParameters);
      //this.createEnergyWindroseChart(projectParameters);
      this.createCostStackedBarChart(projectParameters);

      this.print = projectParameters.printer3d_result.slice(1, -1);

      if (this.activeTask.type) {
        this.disableStatusButton(this.activeTask.type);
      }
    });
  }
  private getProjectId(): Observable<string> {
    return this.activatedRoute.paramMap.pipe(
      map((params) => {
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        return params.get('id') as string;
      })
    );
  }

  private getProject(projectId: string): Observable<Project> {
    return this.projectService.getProject(this.projectId);
  }

  private getProjectCalculations(projectId: string): Observable<any> {
    return this.calculationService
      .getCalculationsOfProject(this.projectId)
      .pipe(
        map((tasks) => {
          const calculationMap: Partial<Record<TaskTypes, Task>> = {};

          for (const task of tasks) {
            if (!task.deprecated) {
              calculationMap[task.taskType] = task;
            }
          }
          return calculationMap;
        })
      );
  }

  private getProjectParameter(projectId: string) {
    return this.projectService.getProjectParameters(projectId);
  }

  private leafLetOptimizationMap(projectParameters: ProjectParameter) {
    const site = projectParameters.site;
    if (site) {
      const fieldBoundaries = site.field_boundaries_long_lat.map((longLat) =>
        latLng(longLat[1], longLat[0])
      );
      const restrictedAreas = site.restricted_areas_long_lat.map(
        (restrictedArea) =>
          restrictedArea.map((longLat) => latLng(longLat[1], longLat[0]))
      );
      const substations = site.substations_long_lat.map((longLat) =>
        latLng(longLat[1], longLat[0])
      );
      const mainLayer = tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
      );
      const positions = projectParameters.positions
        .trimRight()
        .split('\n')
        .map((position) => {
          const splitted = position.split(';');
          return latLng(
            Number.parseFloat(splitted[0]),
            Number.parseFloat(splitted[1])
          );
        });
      this.leafletOptions = {
        center: this.calculateCentroid(fieldBoundaries),
        zoom: 10,
        layers: [mainLayer],
      };
      this.mapCenter = this.calculateCentroid(fieldBoundaries);
      const restrictedAreaPolygons = restrictedAreas.map((restrictedArea) =>
        polygon(restrictedArea, { color: 'red' })
      );
      const substationCircles = substations.map((substation) =>
        circle(substation, { color: 'yellow', radius: 5 })
      );
      const positionCircles = positions.map((position) =>
        circle(position, { color: 'green', radius: 5 })
      );
      this.leafletLayers = [
        polygon(fieldBoundaries),
        restrictedAreaPolygons,
        substationCircles,
        positionCircles,
      ];
    }
  }

  calculateCentroid(coordinates: any[]) {
    let area = 0;
    let long = 0;
    let lat = 0;

    const xs = coordinates.map((c) => c.lng);
    xs.push(xs[0]);
    const ys = coordinates.map((c) => c.lat);
    ys.push(ys[0]);

    for (let i = 0; i < coordinates.length; i += 1) {
      area += xs[i] * ys[i + 1] - xs[i + 1] * ys[i];
      long += (xs[i] + xs[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
      lat += (ys[i] + ys[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
    }

    area /= 2;
    long /= 6 * area;
    lat /= 6 * area;

    return latLng(lat, long);
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  private setting = {
    element: {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      dynamicDownload: null as unknown as HTMLElement,
    },
  };

  /* //This is the desired function but it does not work like intended
  downloadParametersAsZip() {
    const authToken = localStorage.getItem('authToken');
    const header = {
      Authorization: 'Bearer ' + authToken,
      responseType: 'arraybuffer',
    };

    const route = this.projectService.downloadParamsAndResultsAsZip(
      this.projectId
    );
    const headers = new HttpHeaders(header);
    // I do not know how to pass the headers to a get request from external page
    this.http
      .get<any>(route, { headers: headers })
      .subscribe((data) =>
      );
  } */

  downloadParameters() {
    this.results$.subscribe((res) => {
      this.dyanmicDownloadByHtmlTag({
        fileName: 'projectParameters.json',
        text: JSON.stringify(res),
      });
    });
  }

  private dyanmicDownloadByHtmlTag(arg: { fileName: string; text: string }) {
    if (!this.setting.element.dynamicDownload) {
      this.setting.element.dynamicDownload = document.createElement('a');
    }
    const element = this.setting.element.dynamicDownload;
    const fileType =
      arg.fileName.indexOf('.json') > -1 ? 'text/json' : 'text/stl';
    element.setAttribute(
      'href',
      `data:${fileType};charset=utf-8,${encodeURIComponent(arg.text)}`
    );
    element.setAttribute('download', arg.fileName);

    const event = new MouseEvent('click');
    element.dispatchEvent(event);
  }

  startSimulation() {
    this.calculationService
      .startCalculation(this.projectId, 'simulation')
      .subscribe(
        (taskID: string) => {
          this.updateActiveTask(taskID, TaskTypes.simulation, true);
          this.saveActiveTaskIdFromLocalStorage(this.activeTask);
          if (this.activeTask.type) {
            this.disableStatusButton(this.activeTask.type);
          }
        },
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
  }

  stopSimulation() {
    this.calculationService.stopCalculation(this.activeTask.id);
    if (this.activeTask.type === TaskTypes.simulation) {
      this.activeTask.runningStatus = false;
      this.saveActiveTaskIdFromLocalStorage(this.activeTask);
      if (this.activeTask.type) {
        this.disableStatusButton(this.activeTask.type);
      }
    }
  }

  startOptimization() {
    this.calculationService
      .startCalculation(this.projectId, 'optimization')
      .subscribe(
        (taskID: string) => {
          this.updateActiveTask(taskID, TaskTypes.optimization, true);
          this.saveActiveTaskIdFromLocalStorage(this.activeTask);
          if (this.activeTask.type) {
            this.disableStatusButton(this.activeTask.type);
          }
        },
        (error) => {
          this.snackBar.open(`Optimization Task already running`, 'Ok', {
            duration: 1500,
          });
        }
      );
  }

  stopOptimization() {
    this.calculationService
      .stopCalculation(this.activeTask.id)
      .subscribe((response) => {});
    if (this.activeTask.type === TaskTypes.optimization) {
      this.activeTask.runningStatus = false;
      this.saveActiveTaskIdFromLocalStorage(this.activeTask);

      this.disableStatusButton(this.activeTask.type);
    }
  }

  startSensitivityAnalysis() {
    this.calculationService
      .startCalculation(this.projectId, 'sensitivityAnalysis')
      .subscribe(
        (taskID: string) => {
          this.updateActiveTask(taskID, TaskTypes.sensitivityAnalysis, true);
          this.saveActiveTaskIdFromLocalStorage(this.activeTask);
          if (this.activeTask.type) {
            this.disableStatusButton(this.activeTask.type);
          }
        },
        (error) => {
          this.snackBar.open(`${error.message}`, 'Ok', {
            duration: 1500,
          });
        }
      );
  }

  stopSensitivityAnalysis() {
    this.calculationService.stopCalculation(this.activeTask.id);
    if (this.activeTask.type === TaskTypes.sensitivityAnalysis) {
      this.activeTask.runningStatus = false;
      this.saveActiveTaskIdFromLocalStorage(this.activeTask);

      this.disableStatusButton(this.activeTask.type);
    }
  }

  generate3DPrint(id: string) {
    this.calculationService
      .startCalculation(id, 'generate3dPrinterFile')
      .subscribe(
        (taskID: string) => {
          if (this.stlFile) {
            this.stlFile.id = taskID;
            this.stlFile.taskType = TaskTypes.generate3dPrinterFile;
            this.stlFile.projectType = 'wind';
            this.stlFile.projectId = id;
          }
        },
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
  }

  /**************************************************************************************
   * Utility fonctions
   **************************************************************************************/

  loadActiveTaskIdFromLocalStorage(projectId: string): ITaskSummary {
    const jsonValue = localStorage.getItem('windflower:activeTask');
    // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
    if (jsonValue) {
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      const activeTask = JSON.parse(jsonValue) as ITaskSummary;
      if (!activeTask) {
        return {
          projectId,
          type: undefined,
          id: '',
          runningStatus: false,
        };
      }
      activeTask.projectId = projectId;
      return activeTask;
    }
    return {
      id: '',
      runningStatus: false,
    };
  }

  saveActiveTaskIdFromLocalStorage(activeTask: ITaskSummary) {
    if (activeTask) {
      const jsonValue = JSON.stringify(activeTask);
      localStorage.setItem('windflower:activeTask', jsonValue);
    }
  }

  updateActiveTask(taskId: string, taskType: TaskTypes, state: boolean = true) {
    this.activeTask.id = taskId;
    this.activeTask.type = taskType;
    this.activeTask.runningStatus = state;
  }

  private upudateIsRunningVariables(
    simulationRunning: boolean,
    optimizationRunning: boolean,
    sensitivityAnalysisRunning: boolean
  ) {
    this.simulationIsRunning = simulationRunning;
    this.sensitivityAnalysIsRunning = sensitivityAnalysisRunning;
    this.optimizationIsRunning = optimizationRunning;
  }
  disableStatusButton(type: TaskTypes) {
    if (type === TaskTypes.simulation) {
      if (this.activeTask.runningStatus) {
        this.upudateIsRunningVariables(true, false, false);
      } else {
        this.upudateIsRunningVariables(false, false, false);
      }
    }
    if (type === TaskTypes.optimization) {
      if (this.activeTask.runningStatus) {
        this.upudateIsRunningVariables(false, true, false);
      } else {
        this.upudateIsRunningVariables(false, false, false);
      }
    }
    if (type === TaskTypes.sensitivityAnalysis) {
      if (this.activeTask.runningStatus) {
        this.upudateIsRunningVariables(false, false, true);
      } else {
        this.upudateIsRunningVariables(false, false, false);
      }
    }
  }

  download3DPrint() {
    this.results$.subscribe((res) => {
      this.dyanmicDownloadByHtmlTag({
        fileName: '3DPrint.stl',
        text: this.print,
      });
    });
  }

  private normalizeData(a: number, b: number) {
    return Math.max(0, (a / b) * 100);
  }

  private extractOptimizaionBarData(projectParameters: ProjectParameter) {
    const optimizationResult =
      projectParameters.optimization_result.final_result.economics;
    const simulationResult =
      projectParameters.simulation_result.final_result.economics;

    return [
      {
        name: 'Annual Energie Production',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.net_annual_energy_production,
              simulationResult.net_annual_energy_production
            ),
          },
        ],
      },
      {
        name: 'Internal rate of return',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.internal_rate_of_return,
              simulationResult.internal_rate_of_return
            ),
          },
        ],
      },
      {
        name: 'Levelized energy cost',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.levelized_cost_of_energy,
              simulationResult.levelized_cost_of_energy
            ),
          },
        ],
      },
      {
        name: 'Net present value',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.net_present_value,
              simulationResult.net_present_value
            ),
          },
        ],
      },
      {
        name: 'Total investment costs',
        series: [
          {
            name: 'Old value',
            value: 100,
          },
          {
            name: 'Optimized value',
            value: this.normalizeData(
              optimizationResult.investment_costs,
              simulationResult.investment_costs
            ),
          },
        ],
      },
    ];
  }

  // The goal is to create a wind rose diagram for the annual energy production per wind direction
  // Create a new dataset to not modify the data given in the Project Parameters
  /* private createEnergyWindroseChart(projectParameters: ProjectParameter) {
    var aep_inputs = [];
    var aep_inputs_directions = [];
    var sum = 0;
    var grandTotal = 0;
    //Alle Interface Einträge gibt es weder hier noch im alten Projekt 
    var numberOfDirections =
      projectParameters.simulation_result.final_result['matrix_index_direction']
        .length;
    const degreeStepInWindrose = 15;
    const degreeInWindrose = 360; //Degree in a circle
    const jumpsizeInWindrose =
      degreeStepInWindrose * (numberOfDirections / degreeInWindrose);
    var max = 0; // used to find the maximum value
    for (
      var i = 0;
      i <
      projectParameters.simulation_result.final_result[
        'matrix_direction_speed_revenue'
      ].length;
      i++
    ) {
      for (
        var k = 0;
        k <
        projectParameters.simulation_result.final_result[
          'matrix_direction_speed_revenue'
        ][i].length;
        k++
      ) {
        sum +=
          projectParameters.simulation_result.final_result[
            'matrix_direction_speed_revenue'
          ][i][k];
      }
      grandTotal += sum;
      aep_inputs.push(sum);
      max = sum > max ? sum : max;
      sum = 0;
    }

    var temp = 5;
    while (Math.floor(max / 10) > 0) {
      max /= 10;
      temp *= 10;
    }

    const tickStart = temp / 5;
    const tickStepsize = temp / 5;

    grandTotal = parseFloat(grandTotal.toFixed(3));

    for (var i = 0; i < numberOfDirections; i++) {
      //indicate the directions with degree
      aep_inputs_directions.push(
        parseFloat(
          projectParameters.simulation_result.final_result[
            'matrix_index_direction'
          ][i]
        ).toFixed(3) + '°'
      );
    }

    this.energyProductionData = [
      {
        r: aep_inputs,
        theta: aep_inputs_directions,
        marker: { color: 'rgb(0,85,102)' },
        type: 'barpolar',
      },
    ];
    this.energyProductionConfig = { displayModeBar: false };
    this.energyProductionLayout = {
      title: grandTotal + '	[GWh]', //Add the unit to the grandTotal
      autosize: true,
      polar: {
        barmode: 'stack',
        bargap: 0.01,
        radialaxis: { tick0: tickStart, dtick: tickStepsize },
        angularaxis: {
          direction: 'clockwise',
          tick0: 0,
          dtick: jumpsizeInWindrose,
        },
        yaxis: { fixedrange: true },
        xaxis: { fixedrange: true },
      },
      margin: {
        l: 40,
        r: 40,
        t: 60,
        b: 60,
      },
    };
  } */

  createCostStackedBarChart(projectParameters: ProjectParameter) {
    const costs_cablingCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .cabling_costs,
      ],
      name: 'Cabling Costs',
      type: 'bar',
      marker: { color: '#002a38' },
    };
    const costs_turbineCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .foundation_costs,
      ],
      name: 'Turbine Costs',
      type: 'bar',
      marker: { color: '#004e5e' },
    };
    const costs_substationCosts = {
      x: ['Total'],
      y: [
        projectParameters.simulation_result.final_result.economics
          .substation_costs,
      ],
      name: 'Substation Costs',
      type: 'bar',
      marker: { color: '#80aab3' },
    };

    const grandTotal =
      costs_cablingCosts.y[0] +
      costs_turbineCosts.y[0] +
      costs_substationCosts.y[0];
    this.economicsData = [
      costs_cablingCosts,
      costs_turbineCosts,
      costs_substationCosts,
    ];
    this.economicsConfig = { displayModeBar: false };
    this.economicsLayout = {
      title:
        parseFloat('' + grandTotal / Math.pow(10, 6)).toFixed(3) + ' Mio €',
      barmode: 'stack',
      showlegend: true,
      legend: { orientation: 'h' },
      yaxis: { fixedrange: true },
      xaxis: { fixedrange: true },
    };
  }

  adoptOptimisedLayout(id: string) {
    //TODO add call to set layout in backend
  }

  change3DPrintParams() {
    //TODO update visualization of 3D print
  }

  scroll(el: HTMLElement) {
    el.scrollIntoView();
  }

  ngOnDestroy(): void {
    this.saveActiveTaskIdFromLocalStorage(this.activeTask);
    clearInterval(this.POLL_BACKEND);
  }

  publishToGit() {
    if (this.project) {
      this.dialog
        .open(PublishWarningComponent, { data: { projectId: this.project.id } })
        .afterClosed()
        .subscribe((publish: boolean) => {
          if (publish && this.project) {
            console.log('Published project to review process.');
            this.project.projectStatus = ProjectStatus.UNDER_REVIEW;
            this.projectService.pushProjectToPublishReview(this.project);
            setTimeout(() => {
              this.snackBar
                .open(`Project published successfully`, 'Ok', {
                  duration: 1500,
                })
                .afterDismissed()
                .subscribe((res: any) => {
                  this.router.navigate([`app/projects`]);
                });
            }, 1500);
          }
        });
    }
  }
}
