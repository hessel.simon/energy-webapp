import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SimulationParameterComponent } from './simulation-parameter.component';

describe('SimulationParameterComponent', () => {
  let component: SimulationParameterComponent;
  let fixture: ComponentFixture<SimulationParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [SimulationParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SimulationParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
