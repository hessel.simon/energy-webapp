import { Component, OnInit, Renderer2, Input } from '@angular/core';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { Router, ActivatedRoute } from '@angular/router';
import { Config, SimulationModel } from '@energy-platform/shared/interfaces';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-simulation-parameter',
  templateUrl: './simulation-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './simulation-parameter.component.scss',
  ],
})
export class SimulationParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  @Input()
  public projectId: string | undefined;

  private _simulation: BehaviorSubject<SimulationModel | undefined>;
  public simulation: Observable<SimulationModel | undefined>;

  public schema:
    | {
        simulation_model: {
          properties: {
            num_wind_speeds: Config;
            num_wind_directions: Config;
            loss_plant_performance_percentage: Config;
            loss_power_curve_percentage: Config;
            loss_wake_effect_percentage: Config;
            loss_wind_speed_percentage: Config;
          };
        };
      }
    | undefined;

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._simulation = new BehaviorSubject<SimulationModel | undefined>(
      undefined
    );
    this._simulation = new BehaviorSubject<SimulationModel | undefined>(
      undefined
    );
    this.simulation = this._simulation.asObservable();
  }

  ngOnInit() {
    this.parameterService.getSchemas().subscribe((data) => {
      this.schema = data;
    });

    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'simulationModel')
        .subscribe((response) => {
          this._simulation.next(response.simulationModel);
        });
    }
  }

  onSave() {
    if (this.projectId) {
      this.projectService.setProjectParameters(this.projectId, {
        simulationModel: this._simulation.getValue(),
      });
    }
  }
}
