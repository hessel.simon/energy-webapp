import { Component, Input, OnInit } from '@angular/core';
import { Project } from '@energy-platform/shared/interfaces';
import { SimulationOutput } from './simulationTableElement';

@Component({
  selector: 'energy-platform-simulation-results-table',
  templateUrl: './simulation-results-table.component.html',
  styleUrls: ['./simulation-results-table.component.scss'],
})
export class SimulationResultsTableComponent implements OnInit {
  @Input()
  results: any;
  simulationOutput: SimulationOutput[] = [];
  displayedColumns: string[] = ['parameter', 'value', 'unit'];
  project: Project | undefined;
  constructor() {}

  ngOnInit() {
    const simulationRes = this.results.simulation_result.final_result.economics;
    this.simulationOutput = [
      {
        parameter: 'investment_costs',
        value: Math.round(simulationRes.investment_costs / 1000000),
        unit: '[Mio €]',
      },
      {
        parameter: 'annual_o_and_m_costs',
        value: Math.round(simulationRes.annual_o_and_m_costs / 1000000),
        unit: '[Mio € / Year]',
      },
      {
        parameter: 'internal_rate_of_return',
        value: Math.round(simulationRes.internal_rate_of_return * 100) / 100,
        unit: '[%]',
      },
      {
        parameter: 'levelized_cost_of_energy',
        value: Math.round(simulationRes.levelized_cost_of_energy * 100) / 100,
        unit: '[€/MWh]',
      },
      {
        parameter: 'net_annual_energy_production',
        value: Math.round(simulationRes.net_annual_energy_production),
        unit: '[GWh]',
      },
      {
        parameter: 'net_present_value',
        value: Math.round(simulationRes.net_present_value / 1000000),
        unit: '[Mio €]',
      },
      {
        parameter: 'payback_period',
        value: Math.round(simulationRes.payback_period * 100) / 100,
        unit: '[Years]',
      },
    ];
  }
}
