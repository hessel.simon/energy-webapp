export interface SimulationOutput {
    parameter: string;
    value: number;
    unit: string;
  }