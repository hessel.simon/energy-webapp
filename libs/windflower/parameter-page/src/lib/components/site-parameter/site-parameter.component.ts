import {
  Component,
  Renderer2,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ProjectsService,
  ParameterService,
} from '@energy-platform/shared/services';
// import { Project } from '@energy-platform/shared/interfaces';
import { Config, Project, Site } from '@energy-platform/shared/interfaces';
import {
  Circle,
  circle,
  LatLng,
  latLng,
  LatLngLiteral,
  Polygon,
  polygon,
  tileLayer,
} from 'leaflet';
import { BehaviorSubject, Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import {
  latLngExpressionToLatLngTuple,
  latLngExpressionToProxy,
} from '@energy-platform/shared/utils';

@Component({
  selector: 'energy-platform-site-parameter',
  templateUrl: './site-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './site-parameter.component.scss',
  ],
})
export class SiteParameterComponent
  extends ParameterPageComponent
  implements OnChanges, OnInit
{
  public bndryCollapse: boolean;
  public mainLayer: any;
  public leafletOptions: any;
  public fieldBoundaries: (LatLng | LatLngLiteral)[] = [];
  public restrictedAreas: (LatLng | LatLngLiteral)[][] = [];
  public substations: (LatLng | LatLngLiteral)[] = [];
  public restrictedAreaPolygons: Polygon[] = [];
  public substationCircles: Circle[] = [];
  public leafletLayers: any;
  public mapCenter: LatLng | undefined;
  public location: any;
  @Input()
  public projectId: string | undefined;

  @Input()
  public project: Project | undefined;

  private _site: BehaviorSubject<Site | undefined>;
  public site: Observable<Site | undefined>;
  public schema:
    | {
        site: {
          properties: {
            surface_roughness: Config;
          };
        };
      }
    | undefined;

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    private projectsService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectsService,
      parameterService,
      snackBar
    );
    this.bndryCollapse = true;
    this._site = new BehaviorSubject<Site | undefined>(undefined);
    this.site = this._site.asObservable();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.project) {
      //  this.logger.info("the project informations are the following ", this.project);
      this.location = this.project.location;
      this.mapCenter = latLng(this.location.lat, this.location.lng);
    }
  }

  ngOnInit() {
    this.parameterService.getSchemas().subscribe((data) => {
      this.schema = data;
    });
    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'site')
        .subscribe((response) => {
          this._site.next(response.site);

          this.fieldBoundaries = response.site.field_boundaries_long_lat.map(
            (longLat: number[]) => latLng(longLat[1], longLat[0])
          );
          this.restrictedAreas = response.site.restricted_areas_long_lat.map(
            (restrictedArea: any[]) =>
              restrictedArea.map((longLat: number[]) =>
                latLng(longLat[1], longLat[0])
              )
          );
          this.substations = response.site.substations_long_lat.map(
            (longLat: number[]) => latLng(longLat[1], longLat[0])
          );

          this.mainLayer = tileLayer(
            'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
          );
          this.leafletOptions = {
            center: this.calculateCentroid(this.fieldBoundaries),
            zoom: 10,
            layers: [this.mainLayer],
          };
          if (this.restrictedAreas) {
            this.restrictedAreaPolygons = this.restrictedAreas.map(
              (restrictedArea) => polygon([...restrictedArea], { color: 'red' })
            );
          }
          if (this.substations) {
            this.substationCircles = this.substations.map((substation) =>
              circle(substation, { color: 'yellow', radius: 5 })
            );
          }
          this.leafletLayers = [
            polygon([...this.fieldBoundaries]),
            ...this.restrictedAreaPolygons,
            ...this.substationCircles,
          ];
          this.mapCenter = this.calculateCentroid(this.fieldBoundaries);
        });
    }
  }

  onSave() {
    const siteValues = this._site.getValue();
    if (
      this.projectId &&
      siteValues &&
      this.fieldBoundaries &&
      this.restrictedAreas
    ) {
      this.projectsService.setProjectParameters(this.projectId, {
        site: {
          surface_roughness: siteValues.surface_roughness,
          field_boundaries_long_lat: this.fieldBoundaries.map(
            latLngExpressionToLatLngTuple
          ),
          restricted_areas_long_lat: this.restrictedAreas.map(
            (restrictedArea) =>
              restrictedArea.map(latLngExpressionToLatLngTuple)
          ),
          substations_long_lat: siteValues.substations_long_lat,
        },
      });
    }
  }

  changeFieldBoundaryLat(newLat: number, index: number) {
    if (this.fieldBoundaries) {
      latLngExpressionToProxy(this.fieldBoundaries[index]).update(
        'lat',
        newLat
      );
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
      if (this.fieldBoundaries.length >= 3) {
        this.mapCenter = this.calculateCentroid(this.fieldBoundaries);
      } else {
        this.mapCenter = latLng(this.fieldBoundaries[index]);
      }
    }
  }

  changeFieldBoundaryLng(newLng: number, index: number) {
    if (this.fieldBoundaries) {
      latLngExpressionToProxy(this.fieldBoundaries[index]).update(
        'lng',
        newLng
      );
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
      if (this.fieldBoundaries.length >= 3) {
        this.mapCenter = this.calculateCentroid(this.fieldBoundaries);
      } else {
        this.mapCenter = latLng(this.fieldBoundaries[index]);
      }
    }
  }

  changeRestrictedAreaLat(
    newLat: number,
    areaIndex: number,
    pointIndex: number
  ) {
    if (this.restrictedAreas) {
      latLngExpressionToProxy(
        this.restrictedAreas[areaIndex][pointIndex]
      ).update('lat', newLat);
      this.restrictedAreaPolygons = this.restrictedAreas.map((restrictedArea) =>
        polygon(restrictedArea, { color: 'red' })
      );
    }
    if (this.fieldBoundaries) {
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
    }
  }

  changeRestrictedAreaLng(
    newLng: number,
    areaIndex: number,
    pointIndex: number
  ) {
    if (this.restrictedAreas) {
      latLngExpressionToProxy(
        this.restrictedAreas[areaIndex][pointIndex]
      ).update('lng', newLng);
      this.restrictedAreaPolygons = this.restrictedAreas.map((restrictedArea) =>
        polygon(restrictedArea, { color: 'red' })
      );
    }
    if (this.fieldBoundaries) {
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
    }
  }

  changeSubstationLat(newLat: number, index: number) {
    if (this.substations) {
      latLngExpressionToProxy(this.substations[index]).update('lat', newLat);
      this.substationCircles = this.substations.map((substation) =>
        circle(substation, { color: 'yellow', radius: 5 })
      );
    }
    if (this.fieldBoundaries) {
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
    }
  }

  changeSubstationLng(newLng: number, index: number) {
    if (this.substations) {
      latLngExpressionToProxy(this.substations[index]).update('lng', newLng);
      this.substationCircles = this.substations.map((substation) =>
        circle(substation, { color: 'yellow', radius: 5 })
      );
    }
    if (this.fieldBoundaries) {
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
    }
  }

  addSubstation() {
    if (this.substations) {
      this.substations.push({
        lat: 0,
        lng: 0,
      });
    }
  }

  removeSubstation(index: number) {
    if (this.substations) {
      this.substations.splice(index, 1);
      this.substationCircles = this.substations.map((substation) =>
        circle(substation, { color: 'yellow', radius: 5 })
      );
    }
    if (this.fieldBoundaries) {
      this.leafletLayers = [
        polygon(this.fieldBoundaries),
        this.restrictedAreaPolygons,
        this.substationCircles,
      ];
    }
  }

  removeFieldBoundaryPoint(index: number) {
    if (
      this.fieldBoundaries &&
      this.restrictedAreaPolygons &&
      this.substationCircles
    ) {
      this.fieldBoundaries.splice(index, 1);
      this.leafletLayers = [
        polygon([...this.fieldBoundaries]),
        ...this.restrictedAreaPolygons,
        ...this.substationCircles,
      ];
    }
  }

  addFieldBoundaryPoint() {
    if (this.fieldBoundaries) {
      this.fieldBoundaries.push({
        lat: 0,
        lng: 0,
      });
    }
  }

  removeRestrictedAreaPoint(areaIndex: number, pointIndex: number) {
    if (this.restrictedAreas) {
      this.restrictedAreas[areaIndex].splice(pointIndex, 1);
      this.restrictedAreaPolygons = this.restrictedAreas.map((restrictedArea) =>
        polygon([...restrictedArea], { color: 'red' })
      );
    }
    if (
      this.fieldBoundaries &&
      this.restrictedAreaPolygons &&
      this.substationCircles
    ) {
      this.leafletLayers = [
        polygon([...this.fieldBoundaries]),
        ...this.restrictedAreaPolygons,
        ...this.substationCircles,
      ];
    }
  }

  removeRestrictedArea(areaIndex: number) {
    if (this.restrictedAreas) {
      this.restrictedAreas.splice(areaIndex, 1);
      this.restrictedAreaPolygons = this.restrictedAreas.map((restrictedArea) =>
        polygon([...restrictedArea], { color: 'red' })
      );
    }

    if (
      this.fieldBoundaries &&
      this.restrictedAreaPolygons &&
      this.substationCircles
    ) {
      this.leafletLayers = [
        polygon([...this.fieldBoundaries]),
        ...this.restrictedAreaPolygons,
        ...this.substationCircles,
      ];
    }
  }

  addRestrictedAreaPoint(areaIndex: number) {
    if (this.restrictedAreas) {
      this.restrictedAreas[areaIndex].push({
        lat: 0,
        lng: 0,
      });
    }
  }

  addRestrictedArea() {
    if (this.restrictedAreas) {
      this.restrictedAreas.push([
        {
          lat: 0,
          lng: 0,
        },
      ]);
    }
  }

  calculateCentroid(coordinates: any[] | undefined) {
    this.logger.info(coordinates);
    let area = 0;
    let long = 0;
    let lat = 0;

    if (coordinates) {
      const xs = coordinates.map((c: { lng: any }) => c.lng);
      xs.push(xs[0]);
      const ys = coordinates.map((c: { lat: any }) => c.lat);
      ys.push(ys[0]);

      for (let i = 0; i < coordinates.length; i += 1) {
        area += xs[i] * ys[i + 1] - xs[i + 1] * ys[i];
        long += (xs[i] + xs[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
        lat += (ys[i] + ys[i + 1]) * (xs[i] * ys[i + 1] - xs[i + 1] * ys[i]);
      }
    }

    area /= 2;
    long /= 6 * area;
    lat /= 6 * area;

    return latLng(lat, long);
  }
}
