import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TestParameterComponent } from './test-parameter.component';

describe('TestParameterComponent', () => {
  let component: TestParameterComponent;
  let fixture: ComponentFixture<TestParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TestParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TestParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
