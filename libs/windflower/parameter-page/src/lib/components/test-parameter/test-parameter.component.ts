import { Component, Renderer2 } from '@angular/core';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ProjectsService,
  ParameterService,
} from '@energy-platform/shared/services';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-test-parameter',
  templateUrl: './test-parameter.component.html',
  styleUrls: ['../../parameter-page/parameter-page.component.scss'],
})
export class TestParameterComponent extends ParameterPageComponent {
  id: number;
  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this.id = 1;
  }

  changeValue(id: number, property: string, event: any) {}

  updateList(id: number, property: string, event: any) {}
}
