export const predefinedTurbines = [
  {
    version: '1.5',
    name: 'Vestas V164-8MW',
    cut_in_speed: 4,
    cut_out_speed: 25,
    hub_height: 102,
    rotor_diameter: 164,
    ct_table: [
      0, 0, 0, 0, 0.92, 0.85, 0.82, 0.8, 0.78, 0.76, 0.73, 0.67, 0.52, 0.39,
      0.3, 0.24, 0.19, 0.16, 0.14, 0.12, 0.1, 0.09, 0.08, 0.07, 0.06, 0.05, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    power_table: [
      0, 0, 0, 0, 0.07, 0.28, 0.75, 1.45, 2.4, 3.78, 5.6, 7.18, 7.75, 8, 8, 8,
      8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
  },
  {
    version: '1.5',
    name: 'Bonus B82/2300',
    cut_in_speed: 4,
    cut_out_speed: 25,
    hub_height: 69,
    rotor_diameter: 82.4,
    ct_table: [
      0, 0, 0, 0, 0.856, 0.851, 0.838, 0.858, 0.886, 0.861, 0.763, 0.666, 0.592,
      0.455, 0.347, 0.271, 0.221, 0.179, 0.152, 0.129, 0.109, 0.095, 0.082,
      0.073, 0.063, 0.056, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
    power_table: [
      0, 0, 0, 0, 0.056, 0.148, 0.288, 0.476, 0.729, 1.055, 1.419, 1.769, 2.041,
      2.198, 2.267, 2.291, 2.298, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3, 2.3,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    ],
  },
];
