import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TurbineParameterComponent } from './turbine-parameter.component';

describe('TurbineParameterComponent', () => {
  let component: TurbineParameterComponent;
  let fixture: ComponentFixture<TurbineParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TurbineParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TurbineParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
