import {
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnInit,
  Renderer2,
  ViewChild,
  ViewContainerRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { Config, Turbine } from '@energy-platform/shared/interfaces';
import {
  ParameterService,
  ProjectsService,
} from '@energy-platform/shared/services';
import { TurbineTableRowComponent } from '../turbine-table-row/turbine-table-row.component';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { predefinedTurbines } from './constants';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-turbine-parameter',
  templateUrl: './turbine-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './turbine-parameter.component.scss',
  ],
})
export class TurbineParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  @ViewChild('ctTableBody', { read: ViewContainerRef, static: false })
  ctContainer: ViewContainerRef | undefined;
  @ViewChild('pwrTableBody', { read: ViewContainerRef, static: false })
  pwrContainer: ViewContainerRef | undefined;

  componentFactory = this.resolver.resolveComponentFactory(
    TurbineTableRowComponent
  );

  predefinedTurbines = predefinedTurbines;
  ctRows: ComponentRef<TurbineTableRowComponent>[] = [];
  ctIndex = 0;

  pwrRows: ComponentRef<TurbineTableRowComponent>[] = [];
  pwrIndex = 0;

  @Input()
  projectId: string | undefined;

  public turbine: Observable<Turbine | undefined>;
  private _turbine: BehaviorSubject<Turbine | undefined>;

  public $schema!:
    Observable<{
        turbine: {
          properties: {
            rotor_diameter: Config;
            hub_height: Config;
            cut_in_speed: Config;
            cut_out_speed: Config;
          };
        };
      }>

  constructor(
    private resolver: ComponentFactoryResolver,
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar,
    private cdRef: ChangeDetectorRef
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._turbine = new BehaviorSubject<Turbine | undefined>(undefined);
    this.turbine = this._turbine.asObservable();
  }

  ngOnInit() {
    this.$schema = this.parameterService.getSchemas()

    if (this.projectId) {
      this.parameterService
        .getParameter(this.projectId, 'turbine')
        .subscribe((response) => {
          this.logger.info('turbine parameter are ', response);
          this._turbine.next(response.turbine);
          this.cdRef.detectChanges();
        });
    }
  }

  setPreDefinedTurbine(turbine: any) {
    Object.assign(this.turbine, this.predefinedTurbines[turbine.value]);
  }

  addCtPair() {
    if (this.ctContainer) {
      const componentRef: ComponentRef<TurbineTableRowComponent> =
        this.ctContainer.createComponent(this.componentFactory);
      const newRow = componentRef.instance;
      const turbine = this._turbine.getValue();
      if (turbine) {
        newRow.value = turbine.ct_table[this.ctIndex];
        newRow.selfRef = newRow;
        newRow.index = ++this.ctIndex;
        newRow.interaction = this;
        newRow.parent = 'ct';
      }
      this.ctRows.push(componentRef);
    }
  }

  addPwrPair() {
    if (this.pwrContainer) {
      const componentRef: ComponentRef<TurbineTableRowComponent> =
        this.pwrContainer.createComponent(this.componentFactory);
      const newRow = componentRef.instance;
      const turbine = this._turbine.getValue();
      if (turbine) {
        newRow.value = turbine.power_table[this.pwrIndex];
        newRow.selfRef = newRow;
        newRow.index = ++this.pwrIndex;
        newRow.interaction = this;
        newRow.parent = 'pwr';
      }

      this.pwrRows.push(componentRef);
    }
  }

  removeCtRow(index: number) {
    if (this.ctContainer) {
      if (this.ctContainer.length < 1) return;

      const componentRef = this.ctRows.filter(
        (x) => x.instance.index == index
      )[0];
      const containerIndex: number = this.ctContainer.indexOf(
        componentRef.hostView
      );

      // removing component from container
      this.ctContainer.remove(containerIndex);
      this.ctRows = this.ctRows.filter((x) => x.instance.index !== index);
    }
  }

  removePwrRow(index: number) {
    if (this.pwrContainer) {
      if (this.pwrContainer.length < 1) return;

      const componentRef = this.pwrRows.filter(
        (x) => x.instance.index == index
      )[0];
      const containerIndex: number = this.pwrContainer.indexOf(
        componentRef.hostView
      );

      // removing component from container
      this.pwrContainer.remove(containerIndex);
      this.pwrRows = this.pwrRows.filter((x) => x.instance.index !== index);
    }
  }

  AfterViewInit() {
    setTimeout(() => {
      const turbine = this._turbine.getValue();
      this.logger.info(' turbine parameter in after view init is ', turbine);
      if (turbine) {
        for (let i = 0; i < turbine.ct_table.length; i++) {
          this.addCtPair();
        }
        for (let i = 0; i < turbine.ct_table.length; i++) {
          this.addPwrPair();
        }
      }
      this.cdRef.detectChanges();
    }, 1500);
  }

  onSave() {
    const newCtTableValues: number[] = [];
    const newPwrTableValues: number[] = [];

    for (let i = 0; i < this.ctRows.length; i++) {
      const instance = this.ctRows[i].instance;
      if (instance.value) {
        newCtTableValues.push(instance.value);
      }
    }

    for (let i = 0; i < this.pwrRows.length; i++) {
      const instance = this.pwrRows[i].instance;
      if (instance.value) {
        newPwrTableValues.push(instance.value);
      }
    }
    const turbine = this._turbine.getValue();
    if (turbine) {
      turbine.ct_table = newCtTableValues;
      turbine.power_table = newPwrTableValues;
    }
    this._turbine.next(turbine);

    if (this.projectId) {
      this.projectService.setProjectParameters(this.projectId, {
        turbine: this._turbine.getValue(),
      });
    }
  }
}
