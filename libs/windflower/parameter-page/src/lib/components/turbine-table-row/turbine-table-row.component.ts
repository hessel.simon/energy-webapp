import { Component } from '@angular/core';

@Component({
  selector: 'energy-platform-turbine-table-row',
  templateUrl: './turbine-table-row.component.html',
  styleUrls: ['./turbine-table-row.component.scss'],
})
export class TurbineTableRowComponent {
  public index: number | undefined;
  public parent: string | undefined;
  public selfRef: TurbineTableRowComponent | undefined;
  public interaction: rowIndex | undefined;
  public value: number | undefined;
  public isCtCable: boolean | undefined;
  public disabled = false;

  constructor() {}

  remove(index: number | undefined, parent: string | undefined) {
    if (index && parent) {
      if (parent === 'ct') this.removeCt(index);
      else if (parent === 'pwr') this.removePwr(index);
      else console.error('Something went wrong.');
    } else console.error('Something went wrong.');
  }

  removeCt(index: number) {
    if (this.interaction) {
      this.interaction.removeCtRow(index);
    }
  }

  removePwr(index: number) {
    if (this.interaction) {
      this.interaction.removePwrRow(index);
    }
  }
}

export interface rowIndex {
  removeCtRow(index: number): any;
  removePwrRow(index: number): any;
}
