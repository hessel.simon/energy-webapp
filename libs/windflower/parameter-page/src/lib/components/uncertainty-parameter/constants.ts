export const uncertainties_params = [
  {
    name: 'parameter.uncertainty.uncertainties.rows.windspeed',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
  {
    name: 'parameter.uncertainty.uncertainties.rows.wakeEffect',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
  {
    name: 'parameter.uncertainty.uncertainties.rows.ctCurve',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
  {
    name: 'parameter.uncertainty.uncertainties.rows.surfRoughness',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
  {
    name: 'parameter.uncertainty.uncertainties.rows.powerCurve',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
  {
    name: 'parameter.uncertainty.uncertainties.rows.plantPerf',
    value: {
      consider: false,
      pdf_type: 'normal',
      p90_percentage: 2,
    },
  },
];
