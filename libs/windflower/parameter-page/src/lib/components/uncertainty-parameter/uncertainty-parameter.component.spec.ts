import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UncertaintyParameterComponent } from './uncertainty-parameter.component';

describe('UncertaintyParameterComponent', () => {
  let component: UncertaintyParameterComponent;
  let fixture: ComponentFixture<UncertaintyParameterComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [UncertaintyParameterComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UncertaintyParameterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
