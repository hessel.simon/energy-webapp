import { Component, Renderer2, Input, OnInit } from '@angular/core';
import { ParameterPageComponent } from '../../parameter-page/parameter-page.component';
import { Router, ActivatedRoute } from '@angular/router';
import {
  ProjectsService,
  ParameterService,
} from '@energy-platform/shared/services';
import { Config, Uq } from '@energy-platform/shared/interfaces';
// import { Project } from 'src/app/interfaces/project';
import { BehaviorSubject, Observable } from 'rxjs';
import { uncertainties_params } from './constants';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'energy-platform-uncertainty-parameter',
  templateUrl: './uncertainty-parameter.component.html',
  styleUrls: [
    '../../parameter-page/parameter-page.component.scss',
    './uncertainty-parameter.component.scss',
  ],
})
export class UncertaintyParameterComponent
  extends ParameterPageComponent
  implements OnInit
{
  public multiDim: any;
  public sparseGridLevel: any;
  public uncertainties_params = uncertainties_params;

  public schema:
    | {
        uq: {
          properties: {
            sample_size: Config;
          };
        };
      }
    | undefined;

  @Input() projectId = '';

  private _uq: BehaviorSubject<Uq | undefined>;
  uq: Observable<Uq | undefined>;

  constructor(
    renderer: Renderer2,
    router: Router,
    activatedRoute: ActivatedRoute,
    projectService: ProjectsService,
    parameterService: ParameterService,
    snackBar: MatSnackBar
  ) {
    super(
      renderer,
      router,
      activatedRoute,
      projectService,
      parameterService,
      snackBar
    );
    this._uq = new BehaviorSubject<Uq | undefined>(undefined);
    this.uq = this._uq.asObservable();
  }

  ngOnInit() {
    this.parameterService
      .getSchemas()
      .subscribe((data) => (this.schema = data));

    this.parameterService
      .getParameter(this.projectId, 'uq')
      .subscribe((response) => {
        this._uq.next(response.uq);

        // Assign values to uncertanties_param
        for (const param of this.uncertainties_params) {
          switch (param.name) {
            case 'parameter.uncertainty.uncertainties.rows.windspeed':
              param['value'] = response.uq.uq_windspeed;
              break;
            case 'parameter.uncertainty.uncertainties.rows.wakeEffect':
              param['value'] = response.uq.uq_wakeeffect;
              break;
            case 'parameter.uncertainty.uncertainties.rows.ctCurve':
              param['value'] = response.uq.uq_ctcurve;
              break;
            case 'parameter.uncertainty.uncertainties.rows.surfRoughness':
              param['value'] = response.uq.uq_surfaceroughness;
              break;
            case 'parameter.uncertainty.uncertainties.rows.powerCurve':
              param['value'] = response.uq.uq_powercurve;
              break;
            case 'parameter.uncertainty.uncertainties.rows.plantPerf':
              param['value'] = response.uq.uq_plantperformance;
              break;
          }
        }
      });
  }

  onSave() {
    // Assign values to uncertanties_param
    const uq_values = this._uq.getValue();
    if (uq_values) {
      for (const param of this.uncertainties_params) {
        switch (param.name) {
          case 'parameter.uncertainty.uncertainties.rows.windspeed':
            uq_values.uq_windspeed = param['value'];
            break;
          case 'parameter.uncertainty.uncertainties.rows.wakeEffect':
            uq_values.uq_wakeeffect = param['value'];
            break;
          case 'parameter.uncertainty.uncertainties.rows.ctCurve':
            uq_values.uq_ctcurve = param['value'];
            break;
          case 'parameter.uncertainty.uncertainties.rows.surfRoughness':
            uq_values.uq_surfaceroughness = param['value'];
            break;
          case 'parameter.uncertainty.uncertainties.rows.powerCurve':
            uq_values.uq_powercurve = param['value'];
            break;
          case 'parameter.uncertainty.uncertainties.rows.plantPerf':
            uq_values.uq_plantperformance = param['value'];
            break;
        }
      }
    }
    this.projectService.setProjectParameters(this.projectId, {
      uq: this._uq.getValue(),
    });
  }
}
