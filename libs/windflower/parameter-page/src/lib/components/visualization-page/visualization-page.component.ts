import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Project } from '@energy-platform/shared/interfaces';
import { ProjectsService } from '@energy-platform/shared/services';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'energy-platform-visualization-page',
  templateUrl: './visualization-page.component.html',
  styleUrls: ['./visualization-page.component.scss'],
})
export class VisualizationPageComponent implements OnInit {
  project: Project | undefined;
  fieldBoundaries: number[][] = [];
  geoPositions: number[][] = [];
  restrictedAreas: number[][][] = [];
  location: number[] = [];
  hubHeight: number | undefined;
  rotorDiameter: number | undefined;

  constructor(
    private activatedRoute: ActivatedRoute,
    private snackBar: MatSnackBar,
    private projectService: ProjectsService
  ) {}

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(
      (params) => {
        combineLatest([
          // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
          this.projectService.getProjectParameters(params.get('id') as string),
          // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
          this.projectService.getProject(params.get('id') as string),
        ]).subscribe(
          ([parameters, project]) => {
            this.project = project;
            if (!this.project) return;
            this.project.parameters = parameters;

            // Ensure that coordinates fetched from backend form a polygon
            // Therefore the first and last coordinate must be the same
            if (
              this.project.parameters.site.field_boundaries_long_lat[0] !==
              this.project.parameters.site.field_boundaries_long_lat[
                this.project.parameters.site.field_boundaries_long_lat.length -
                  1
              ]
            ) {
              this.project.parameters.site.field_boundaries_long_lat.push(
                this.project.parameters.site.field_boundaries_long_lat[0]
              );
            }

            // Ensure that coordinates fetched from backend form a polygon
            // Therefore the first and last coordinate must be the same
            for (
              let i = 0;
              i < this.project.parameters.site.restricted_areas_long_lat.length;
              i++
            ) {
              if (
                this.project.parameters.site.restricted_areas_long_lat[i][0] !==
                this.project.parameters.site.restricted_areas_long_lat[i][
                  this.project.parameters.site.field_boundaries_long_lat
                    .length - 1
                ]
              ) {
                this.project.parameters.site.restricted_areas_long_lat[i].push(
                  this.project.parameters.site.restricted_areas_long_lat[i][0]
                );
              }
            }
            this.fieldBoundaries =
              this.project.parameters.site.field_boundaries_long_lat;
            this.restrictedAreas =
              this.project.parameters.site.restricted_areas_long_lat;
            this.geoPositions = this.project.parameters.positions
              .split('\n')
              .map((triple) => triple.split(';').map(parseFloat))
              .map((val) => {
                // Attention!! Somehow positions data is stored as Lat, Lng but they are needed as Lng, Lat
                return [val[1], val[0]];
              });
            this.location = [
              this.project.location['lng'],
              this.project.location['lat'],
            ];
            this.rotorDiameter = this.project.parameters.turbine.rotor_diameter;
            this.hubHeight = this.project.parameters.turbine.hub_height;
          },
          (error) => {
            this.snackBar.open(
              `Error ${error.status}: ${error.statusText}`,
              'Ok',
              {
                duration: 1500,
              }
            );
          }
        );
      },
      (error) => {
        this.snackBar.open(`Error ${error.status}: ${error.statusText}`, 'Ok', {
          duration: 1500,
        });
      }
    );
  }
}
