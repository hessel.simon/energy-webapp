import { NgxSliderModule } from '@angular-slider/ngx-slider';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';
import { MatSliderModule } from '@angular/material/slider';
import { MatTabsModule } from '@angular/material/tabs';
import { RouterModule, Routes } from '@angular/router';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ThreeVisualizationModule } from '@energy-platform/shared/three-visualizations';
import { VisualizationModule } from '@energy-platform/shared/visualization';
import { TranslateModule } from '@ngx-translate/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CableTypeRowFieldComponent } from './components/cable-type-row-field/cable-type-row-field.component';
import { CableTypeRowComponent } from './components/cable-type-row/cable-type-row.component';
//parameter Components
import { CablingParameterComponent } from './components/cabling-parameter/cabling-parameter.component';
import { EconomicsParameterComponent } from './components/economics-parameter/economics-parameter.component';
import { MeteoParameterComponent } from './components/meteo-parameter/meteo-parameter.component';
import { OptimizationParameterComponent } from './components/optimization-parameter/optimization-parameter.component';
import { ParameterSliderComponent } from './components/parameter-slider/parameter-slider.component';
import { RangedSliderComponent } from './components/ranged-slider/ranged-slider.component';
import { SimulationParameterComponent } from './components/simulation-parameter/simulation-parameter.component';
import { SiteParameterComponent } from './components/site-parameter/site-parameter.component';
import { TestParameterComponent } from './components/test-parameter/test-parameter.component';
import { TurbineParameterComponent } from './components/turbine-parameter/turbine-parameter.component';
import { TurbineTableRowComponent } from './components/turbine-table-row/turbine-table-row.component';
import { UncertaintyParameterComponent } from './components/uncertainty-parameter/uncertainty-parameter.component';
import { ParameterPageComponent } from './parameter-page/parameter-page.component';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule } from '@angular/material/input';
import { VisualizationPageComponent } from './components/visualization-page/visualization-page.component';
import { ResultPageComponent } from './components/result-page/result-page.component';
import { PublicResultPageComponent } from './components/public-result-page/public-result-page.component';
import { SimulationResultsTableComponent } from './components/simulation-results-table/simulation-results-table.component';
import { MatCardModule } from '@angular/material/card';
import { PlotlyModule } from 'angular-plotly.js';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';

const routes: Routes = [
  { path: 'result/:id', component: ResultPageComponent },
  { path: 'result/:id/:realeaseId', component: PublicResultPageComponent },
  { path: 'visualization/:id', component: VisualizationPageComponent },
  {
    path: 'visualization/:id/:realeaseId',
    component: VisualizationPageComponent,
  },
  { path: ':id', component: ParameterPageComponent },
  { path: ':id/:realeaseId', component: ParameterPageComponent },
];
@NgModule({
  imports: [
    CommonModule,
    TranslateModule.forChild(),
    RouterModule.forChild(routes),
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    LeafletModule,
    MatCheckboxModule,
    MatSliderModule,
    MatIconModule,
    NgxSliderModule,
    VisualizationModule,
    NgxChartsModule,
    MatTabsModule,
    MatSlideToggleModule,
    ThreeVisualizationModule,
    MatCardModule,
    PlotlyModule,
    MatTableModule,
    MatTooltipModule,
  ],
  declarations: [
    ParameterPageComponent,
    CablingParameterComponent,
    EconomicsParameterComponent,
    MeteoParameterComponent,
    OptimizationParameterComponent,
    ParameterSliderComponent,
    RangedSliderComponent,
    SimulationParameterComponent,
    SiteParameterComponent,
    TestParameterComponent,
    TurbineParameterComponent,
    UncertaintyParameterComponent,
    TurbineTableRowComponent,
    CableTypeRowComponent,
    CableTypeRowFieldComponent,
    VisualizationPageComponent,
    ResultPageComponent,
    PublicResultPageComponent,
    SimulationResultsTableComponent,
  ],
})
export class ParameterPageModule {}
