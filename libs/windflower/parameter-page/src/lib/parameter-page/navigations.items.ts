export default [
  {
    name: 'parameter.site.title',
    mdi: 0,
    icon: 'parameterSiteIcon',
    comp: 'site',
  },
  {
    name: 'parameter.turbine.title',
    mdi: 1,
    icon: 'parameterWindTurbineIcon',
    comp: 'turbine',
  },
  {
    name: 'parameter.meteo.title',
    mdi: 0,
    icon: 'parameterMeteoIcon',
    comp: 'meteo',
  },
  {
    name: 'parameter.economics.title',
    mdi: 0,
    icon: 'parameterEconomicsIcon',
    comp: 'economics',
  },
  {
    name: 'parameter.cabling.title',
    mdi: 0,
    icon: 'parameterCablingIcon',
    comp: 'cabling',
  },
  {
    name: 'parameter.simulation.title',
    mdi: 0,
    icon: 'resultSimulationIcon',
    comp: 'simulation',
  },
  {
    name: 'parameter.uncertainty.title',
    mdi: 0,
    icon: 'parameterUncertaintyIcon',
    comp: 'uncertainty',
  },
  {
    name: 'parameter.optimization.title',
    mdi: 0,
    icon: 'resultOptimizationsSimulationIcon',
    comp: 'optimization',
  },
  //{name: "TODO", mdi:0, icon: "settings_overscan", comp: "test"}
];
