import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  Renderer2,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ProjectsService,
  ParameterService,
} from '@energy-platform/shared/services';
import { Project } from '@energy-platform/shared/interfaces';
import { MatSnackBar } from '@angular/material/snack-bar';
import NavigationItems from './navigations.items';

@Component({
  selector: 'energy-platform-parameter-page',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './parameter-page.component.html',
  styleUrls: ['./parameter-page.component.scss'],
})
export class ParameterPageComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit, AfterViewInit
{
  paramGroups = NavigationItems;

  public isCollapsed = false;
  @ViewChild('parameterMatTabGroup', { static: false }) navEl: any;

  public parameterProject: Project | undefined;

  constructor(
    private renderer: Renderer2,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public projectService: ProjectsService,
    public parameterService: ParameterService,
    private snackBar: MatSnackBar
  ) {
    super();
  }

  numberWithCommas(x: number, sep = ',') {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, sep);
  }

  isValidSliderInput(slider: any) {
    if (!slider) return false;
    if (slider.value <= slider.max && slider.value >= slider.min) {
      return true;
    }
    return false;
  }

  // validator function needed in parameter-slider.component.html
  validator(slider: any) {
    if (!slider) return false;
    if (slider.value <= slider.max && slider.value >= slider.min) return false;
    return true;
  }
  // end of replacement

  buttonValidator(sliders: any[]) {
    for (const slider of sliders) {
      if (!this.isValidSliderInput(slider)) {
        return false;
      }
    }
    return true;
  }

  ngOnInit() {
    const projectId = this.activatedRoute.snapshot.params.id;
    if (projectId) {
      this.subs.sink = this.projectService.getProject(projectId).subscribe(
        (project) => (this.parameterProject = project),
        (error) =>
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          )
      );
    }
  }

  ngAfterViewInit() {
    const groupFooter = this.renderer.createElement('div');
    this.renderer.addClass(groupFooter, 'parameterTabGroupFooter');

    const footerIcon = this.renderer.createElement('mat-icon');
    footerIcon.id = 'collapseBtn';
    footerIcon.addEventListener('click', this.onCollapse.bind(this));
    this.renderer.appendChild(
      footerIcon,
      this.renderer.createText(
        this.isCollapsed ? 'chevron_right' : 'chevron_left'
      )
    );
    this.renderer.addClass(footerIcon, 'mat-icon');
    this.renderer.addClass(footerIcon, 'material-icons');

    this.renderer.appendChild(groupFooter, footerIcon);

    if (this.navEl) {
      const parentEl =
        this.navEl._elementRef.nativeElement.querySelector('.mat-tab-labels');
      this.renderer.appendChild(parentEl, groupFooter);
    }
  }

  onCollapse() {
    this.isCollapsed = !this.isCollapsed;
    const button = document.getElementById('collapseBtn');
    if (button) {
      button.innerText = this.isCollapsed ? 'chevron_right' : 'chevron_left';
    }
  }
}
