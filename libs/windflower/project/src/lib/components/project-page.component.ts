import {
  Component,
  ElementRef,
  OnChanges,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { UnsubscribeOnDestroyAdapter } from '@energy-platform/shared/abstract';
import {
  AddUserDialogComponent,
  AddUserMailDialogComponent,
  DeleteDialogComponent,
  NewProjectModalComponent,
  PublishWarningComponent,
} from '@energy-platform/shared/dialog';
import {
  Contributor,
  Project,
  ProjectStatus,
  Right,
  User,
} from '@energy-platform/shared/interfaces';
import { ProjectsService, UserService } from '@energy-platform/shared/services';
import {
  compareRights,
  getLowerRight,
  isTruthy,
} from '@energy-platform/shared/utils';
import { environment } from 'environments/environment';
import {
  FeatureGroup,
  FitBoundsOptions,
  icon,
  LatLng,
  latLng,
  LatLngBounds,
  marker,
  Marker,
  popup,
  TileLayer,
  tileLayer,
} from 'leaflet';
import { concat, merge, of, Subject, throwError } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  take,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'energy-platform-project-page',
  templateUrl: './project-page.component.html',
  styleUrls: ['./project-page.component.scss'],
})
export class ProjectPageComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit, OnChanges
{
  Right = Right;
  title = 'Infozentrum';
  options: {
    layers: TileLayer[];
    center: LatLng;
    zoom: number;
  };
  mainLayer: TileLayer;
  markerLayer: Marker[];
  mapFitToBounds: LatLngBounds | undefined;
  mapFitToBoundsOptions: FitBoundsOptions;
  project: Project | undefined;
  dataSource: MatTableDataSource<Contributor> = new MatTableDataSource();
  displayedColumns: string[];
  user: User | undefined;

  contributors: Contributor[] = [];

  inputs = new Subject<{ name: string; description: string }>();

  @ViewChild(MatSort, { static: true })
  sort: MatSort = new MatSort();
  projectId: string | undefined;

  @ViewChild('projectName', { static: false })
  projectName!: ElementRef<HTMLInputElement>;

  @ViewChild('projectDescription', { static: false })
  projectDescription!: ElementRef<HTMLInputElement>;

  /* getProjectId() {
    this.subs.sink = this.activatedRoute.params.subscribe((params) => {
      if (params.id) {
        this.projectId = params.id;
      }
    });
  } */

  ngOnInit() {
    this.subs.sink = this.activatedRoute.params
      .pipe(
        take(1),
        switchMap((params) => {
          if (params.id) {
            this.projectId = params.id;
            return this.projectService.getProject(params.id);
          } else return throwError('ID is undefined');
        }),
        tap((project) => (this.project = project)),
        switchMap((project) => this.refreshContributors(project.id)),
        take(1),
        tap(() => this.updateMap()),
        switchMap(() => this.updateUser())
      )
      .subscribe(
        () => {},
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: environment.MAT_ERROR_DEFAULT,
            }
          );
        }
      );
    this.subs.sink = this.inputs
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap(({ name, description }) => this.onSave(name, description))
      )
      .subscribe();
  }

  ngOnChanges() {
    this.subs.sink = merge(this.getProject(), this.updateUser()).subscribe(() =>
      this.updateMap()
    );
  }

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private dialog: MatDialog,
    private projectService: ProjectsService,
    private userService: UserService,
    private snackBar: MatSnackBar
  ) {
    super();
    this.markerLayer = [];
    this.mapFitToBoundsOptions = { maxZoom: 15, animate: true };
    this.mainLayer = tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    );
    this.options = {
      layers: [this.mainLayer],
      center: latLng([49.77882445, 6.05932344]),
      zoom: 10,
    };
    this.displayedColumns = ['firstName', 'lastName', 'permission', 'action'];
  }

  refreshContributors(projectID: Project['id']) {
    return this.projectService.fetchProjectContributorId(projectID).pipe(
      tap((contributors) => {
        this.contributors = contributors;
        if (this.project) this.project.contributors = this.contributors;
      })
    );
  }

  onInputsChange() {
    this.inputs.next({
      name: this.project?.name || '',
      description: this.project?.description || '',
    });
  }

  getProject() {
    if (!this.project)
      return throwError({ statusText: 'Project not found', status: 403 });
    return this.projectService
      .getProject(this.project.id)
      .pipe(tap((project) => (this.project = project)));
  }

  updateUser() {
    // Check if projects available. If not return
    if (!this.project) {
      return of(undefined);
    }
    return this.activatedRoute.paramMap.pipe(
      take(1),
      switchMap(() => this.userService.getOwnUserProfile()),
      take(1),
      tap((user) => (this.user = user)),
      switchMap(() =>
        merge(
          this.userService.fetchAllUsers(),
          this.projectService.fetchAllAccessibleProjects()
        )
      )
    );
  }

  updateMap() {
    this.markerLayer = [];

    // Check if projects available. If not return
    if (!this.project) {
      return;
    }
    if (!this.project.contributors) {
      return;
    }
    const htmlPopupContent =
      this.project.projectStatus === ProjectStatus.PUBLIC &&
      this.project.isPublic
        ? `
    <p><b>Project Name: </b>${this.project.name}</p>
    <p><b>Creator: </b>${this.project.creator.firstName} ${this.project.creator.lastName}</p>`
        : this.project.creator.organization
        ? `<p><b>Project Name: </b>${this.project.name}</p>
    <p><b>Participants: </b>${this.project.contributors.length}</p>
    <p><b>Creator: </b>${this.project.creator.firstName} ${this.project.creator.lastName}</p>
    <p><b>Agency: </b>${this.project.creator.organization.name}</p>`
        : `<p><b>Project Name: </b>${this.project.name}</p>
    <p><b>Participants: </b>${this.project.contributors.length}</p>
    <p><b>Creator: </b>${this.project.creator.firstName} ${this.project.creator.lastName}</p>`;

    const popupContent = popup()
      .setLatLng([this.project.location.lat, this.project.location.lng])
      .setContent(htmlPopupContent)
      // TODO find correct type
      // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
      .openOn(this.mainLayer as any);

    const newMarker = this.createMarker(this.project).bindPopup(popupContent);
    newMarker.on('click', () => newMarker.openPopup());

    this.markerLayer.push(newMarker);

    if (this.markerLayer && this.markerLayer.length > 0) {
      const featureGroup = new FeatureGroup(this.markerLayer);
      this.mapFitToBounds = featureGroup.getBounds().pad(0.5);
    }
  }

  createMarker(projectProperties: Project): Marker {
    const markerIcon: string = projectProperties.isPublic
      ? 'assets/images/map/marker-icon.png'
      : 'assets/images/map/marker-icon_own.png';
    return marker(
      [projectProperties.location.lat, projectProperties.location.lng],
      {
        icon: icon({
          iconSize: [43, 50],
          iconAnchor: [13, 41],
          iconUrl: markerIcon,
          shadowUrl: 'assets/images/map/marker-shadow.png',
        }),
        title: projectProperties.name,
      }
    );
  }

  changePermission(elementId: string, permission: Right): void {
    const userElement = this.dataSource.filteredData.find(
      (x) => x.id === elementId
    );
    if (this.project?.id && userElement) {
      userElement.rights =
        compareRights(userElement.rights, permission) < 0
          ? permission
          : getLowerRight(userElement.rights);

      this.subs.sink = this.projectService
        .deleteContributors(this.project.id, userElement.id)
        .pipe(
          switchMap(() =>
            this.project
              ? this.projectService.addContributor(
                  this.project.id,
                  userElement.id,
                  userElement.rights
                )
              : of(undefined)
          ),
          switchMap(() => this.updateUser())
        )
        .subscribe();
    } else {
      this.logger.error('Project is undefined');
    }
  }

  cloneProject() {
    this.subs.sink = this.activatedRoute.params.subscribe((params) => {
      if (params.id) {
        this.dialog.open(NewProjectModalComponent, {
          data: {
            projectID: params.id,
            projectDescription: params.description,
            userId: this.user?.id || '',
          },
        });
      }
    });
  }

  publishToGit() {
    if (this.project) {
      this.subs.sink = this.dialog
        .open(PublishWarningComponent, { data: { projectId: this.project.id } })
        .afterClosed()
        .subscribe((publish: boolean) => {
          if (publish && this.project) {
            this.logger.info('Published project to review process.');
            this.project.projectStatus = ProjectStatus.UNDER_REVIEW;
            this.projectService.pushProjectToPublishReview(this.project);
            setTimeout(() => {
              this.subs.sink = this.snackBar
                .open(`Project published successfully`, 'Ok', {
                  duration: 1500,
                })
                .afterDismissed()
                .subscribe(() => {
                  this.router.navigate([`app/projects`]);
                });
            }, 1500);
          }
        });
    } else {
      this.logger.error('Project is undefined');
    }
  }

  onSave(name: string, description: string) {
    if (this.project && name && description) {
      return this.projectService.editProject(this.project);
    } else this.logger.error('Project is undefined');
    return of(undefined);
  }

  addUser() {
    this.subs.sink = this.getProject().subscribe();

    this.subs.sink = this.dialog
      .open(AddUserDialogComponent, {
        width: '400px',
        data: this.project,
      })
      .afterClosed()
      .pipe(
        switchMap((newContributorIDs: Contributor['id'][] | undefined) => {
          if (newContributorIDs && this.project) {
            return merge(
              ...newContributorIDs
                .filter(isTruthy)
                .map((newContributorID) =>
                  this.project
                    ? this.projectService.addContributor(
                        this.project.id,
                        newContributorID
                      )
                    : of(undefined)
                )
            );
          } else return of(undefined);
        }),
        switchMap(() =>
          merge(
            this.updateUser(),
            this.project ? this.refreshContributors(this.project.id) : of([])
          )
        )
      )
      .subscribe(
        () => {},
        (error) => {
          this.snackBar.open(
            `Error ${error.status}: ${error.statusText}`,
            'Ok',
            {
              duration: 1500,
            }
          );
        }
      );
  }

  inviteUserMail() {
    this.subs.sink = this.getProject().subscribe();

    this.subs.sink = this.dialog
      .open(AddUserMailDialogComponent, {
        width: '50em',
        height: '40em',
        data: { project: this.project, userId: this.user?.id || '' },
      })
      .afterClosed()
      .pipe(
        switchMap((data) =>
          data && data.updateUser ? this.updateUser() : of(undefined)
        )
      )
      .subscribe();
  }

  deleteContributor(contributor: Contributor) {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      data: `the contributor "${contributor.user.firstName} ${contributor.user.lastName}"`,
    });

    this.subs.sink = dialogRef
      .afterClosed()
      .pipe(
        switchMap((result) =>
          result && this.project
            ? concat(
                this.projectService.deleteContributors(
                  this.project.id,
                  contributor.id
                ),
                this.updateUser(),
                this.project
                  ? this.refreshContributors(this.project.id)
                  : of([])
              )
            : of(undefined)
        )
      )
      .subscribe();
  }

  redirectTo(uri: string) {
    this.router
      .navigateByUrl('/', { skipLocationChange: true })
      .then(() => this.router.navigate([uri]));
  }
  deleteProject() {
    if (this.project) {
      const dialogRef = this.dialog.open(DeleteDialogComponent, {
        data: `the project "${this.project.name}"`,
      });

      this.subs.sink = dialogRef.afterClosed().subscribe((result) => {
        if (result && this.project) {
          this.projectService
            .deleteProject(this.project.id)
            .subscribe((data) => console.log('HALLOOOOO: ', data));
          this.redirectTo('app/dashboard');
        }
      });
    } else this.logger.error('Project is undefinded');
  }
}
