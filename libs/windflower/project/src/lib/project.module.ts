import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { ProjectPageComponent } from './components/project-page.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { LayoutModule } from '@energy-platform/shared/layout';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from '@energy-platform/shared/pipes';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatTooltipModule } from '@angular/material/tooltip';

const materialModules = [
  MatIconModule,
  MatTableModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatSnackBarModule,
  MatInputModule,
  MatDialogModule,
  MatButtonModule,
  MatTooltipModule,
];

const routes: Routes = [
  { path: ':id', component: ProjectPageComponent },
  { path: ':id/:realeaseId', component: ProjectPageComponent },
];

@NgModule({
  declarations: [ProjectPageComponent],
  imports: [
    TranslateModule.forChild(),
    materialModules,
    PipesModule,
    FormsModule,
    LayoutModule,
    LeafletModule,
    CommonModule,
    RouterModule.forChild(routes),
  ],
})
export class ProjectModule {}
